import axios from 'axios'
import { SERVICE_ELEND_DOMAIN } from 'src/constants/constants'

const createBorrower = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + '/api/borrowers/create', data)
}

const getBorrowerById = (borrowerId) => {
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/borrowers/getInfo/${borrowerId}`)
}

const updateBorrowerById = (data) => {
  return axios.put(SERVICE_ELEND_DOMAIN + `/api/borrowers/${data.id}`, data)
}

const getAllBorrowers = (data) => {
  let url
  if (data && data.all) {
    url = SERVICE_ELEND_DOMAIN + `/api/borrowers?sort=createdDate,desc`
    if (data.status) {
      url = `${url}&status.equals=${data.status}`
    }
    if (data.loanStatus) {
      url = `${url}&loanStatus.equals=${data.loanStatus}`
    }
    return axios.get(url)
  }
  url =
    SERVICE_ELEND_DOMAIN +
    `/api/borrowers?page=${data.page}&size=${data.size}&sort=createdDate,desc`
  if (data.status) {
    url = `${url}&status.equals=${data.status}`
  }
  if (data.loanStatus) {
    url = `${url}&loanStatus.equals=${data.loanStatus}`
  }
  return axios.get(url)
}

const deleteBorrowerById = (borrowerId) => {
  return axios.delete(SERVICE_ELEND_DOMAIN + `/api/borrowers/${borrowerId}`)
}

const moveBorrowerToBlacklist = (data) => {
  return axios.put(SERVICE_ELEND_DOMAIN + `/api/borrowers/${data.id}/blacklist`)
}

const moveBorrowerOutBlacklist = (data) => {
  return axios.put(SERVICE_ELEND_DOMAIN + `/api/borrowers/${data.id}/blacklist/undo`)
}

export const borrowerService = {
  createBorrower,
  updateBorrowerById,
  getAllBorrowers,
  getBorrowerById,
  deleteBorrowerById,
  moveBorrowerToBlacklist,
  moveBorrowerOutBlacklist,
}

import axios from 'axios'
import { SERVICE_DOMAIN } from 'src/constants/constants'

const createOrganization = (data) => {
  return axios.post(SERVICE_DOMAIN + '/api/cms/organization/create', data)
}

const updateOrganization = (data) => {
  return axios.put(SERVICE_DOMAIN + '/api/cms/organization/update', data)
}

const getOrganization = (organizatonId) => {
  return axios.get(SERVICE_DOMAIN + `/api/cms/organization/${organizatonId}`)
}

const getOrganizations = (data) => {
  if (data && data.all) {
    return axios.get(SERVICE_DOMAIN + `/api/cms/organization?sort=createdDate,desc`)
  }
  return axios.get(
    SERVICE_DOMAIN +
      `/api/cms/organization?page=${data.page}&size=${data.size}&sort=createdDate,desc`,
  )
}

const deleteOrganization = (organizatonId) => {
  return axios.delete(SERVICE_DOMAIN + `/api/cms/organization/delete/${organizatonId}`)
}

const searchUserInOrganization = (organizatonId, isBelonged, keyword) => {
  let fullURL = isBelonged
    ? `get?organizationId.equals=${organizatonId}`
    : `get?organizationId.notEquals=${organizatonId}`
  return axios.get(SERVICE_DOMAIN + `/api/cms/account/${fullURL}`)
}

const updateMembersOrganization = (data) => {
  return axios.post(SERVICE_DOMAIN + '/api/cms/organization/assignUser', data)
}

export const organizationService = {
  createOrganization,
  updateOrganization,
  getOrganizations,
  getOrganization,
  deleteOrganization,
  searchUserInOrganization,
  updateMembersOrganization,
}

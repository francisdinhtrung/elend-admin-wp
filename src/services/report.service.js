import axios from 'axios'
import { SERVICE_ELEND_DOMAIN } from 'src/constants/constants'

const getReportBorrowers = (organizationId) => {
  if (organizationId && organizationId.length > 0) {
    return axios.get(SERVICE_ELEND_DOMAIN + `/api/reports/organization/${organizationId}/borrowers`)
  }
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/reports/borrowers`)
}

const getReportLoans = (organizationId) => {
  if (organizationId && organizationId.length > 0) {
    return axios.get(SERVICE_ELEND_DOMAIN + `/api/reports/organization/${organizationId}/loans`)
  }
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/reports/loans`)
}

const getReportLoanAnalysis = (organizationId) => {
  if (organizationId && organizationId.length > 0) {
    return axios.get(
      SERVICE_ELEND_DOMAIN + `/api/reports/organization/${organizationId}/loans/analysis`,
    )
  }
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/reports/loans/analysis`)
}

export const reportService = {
  getReportBorrowers,
  getReportLoans,
  getReportLoanAnalysis,
}

import axios from 'axios'
import { SERVICE_ELEND_DOMAIN } from 'src/constants/constants'

const createLoan = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + '/api/loans/create', data)
}

const updateLoan = (data) => {
  return axios.put(SERVICE_ELEND_DOMAIN + `/api/loans/${data.id}/update`, data)
}

const getAllLoans = (data) => {
  let url = `/api/loans?sort=createdDate,desc`
  if (data.status) {
    url += `&status.equals=${data.status}`
  }
  if (data && data.all) {
    return axios.get(SERVICE_ELEND_DOMAIN + url)
  }
  url = `/api/loans?page=${data.page}&size=${data.size}&sort=createdDate,desc`
  if (data.borrowerId) {
    url += `&borrowerId.equals=${data.borrowerId}`
  }
  if (data.status) {
    url += `&status.equals=${data.status}`
  }
  return axios.get(SERVICE_ELEND_DOMAIN + url)
}

const getLoanById = (loanId) => {
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/loans/${loanId}`)
}

const deleteLoanById = (loanId) => {
  return axios.delete(SERVICE_ELEND_DOMAIN + `/api/loans/${loanId}`)
}

const approveLoanById = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/approve`, data)
}

const undoApproval = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/undo-approval`, data)
}

const rejectLoanById = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/reject`, data)
}

const blacklistLoanById = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/blacklist`, data)
}

const undoRejection = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/undo-rejection`, data)
}

const disburseLoan = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/disburse-loan`, data)
}

const undoDisbursement = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/undo-disbursement`, data)
}

const createRepayment = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/repayment/create`, data)
}

const getRepaymentDetail = (data) => {
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/repayment/${data.repaymentId}`)
}

const reverseRepayment = (data) => {
  return axios.post(
    SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/repayment/${data.repaymentId}/reverse`,
  )
}

const updateRepayment = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/repayment/update`, data)
}

const processLoanCalculator = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/calculator/process-loan-calculator`, data)
}

const getPaymentTypes = () => {
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/loans/payment-types`)
}

const getMethodOfRepayments = () => {
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/loans/method-repayments`)
}

const confirmRepayment = (data) => {
  return axios.post(
    SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/repayment/${data.id}/complete`,
    data,
  )
}

const payoffDebtRepayment = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/repayment/payoff`, data)
}

const closeLoan = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/close`, data)
}

const getLoanNotes = (data) => {
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/notes`)
}

const getLoanNoteDetail = (data) => {
  return axios.get(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/notes/${data.loanNoteId}`)
}

const createLoanNote = (data) => {
  return axios.post(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/notes/create`, data)
}

const updateLoanNote = (data) => {
  return axios.put(SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/notes/update`, data)
}

const deleteLoanNote = (data) => {
  return axios.delete(
    SERVICE_ELEND_DOMAIN + `/api/loans/${data.loanId}/notes/${data.loanNoteId}/delete`,
  )
}

export const loanService = {
  createLoan,
  updateLoan,
  deleteLoanById,
  getAllLoans,
  getLoanById,
  approveLoanById,
  undoApproval,
  rejectLoanById,
  blacklistLoanById,
  undoRejection,
  disburseLoan,
  undoDisbursement,
  createRepayment,
  getRepaymentDetail,
  reverseRepayment,
  updateRepayment,
  processLoanCalculator,
  getPaymentTypes,
  getMethodOfRepayments,
  confirmRepayment,
  payoffDebtRepayment,
  closeLoan,
  getLoanNotes,
  getLoanNoteDetail,
  createLoanNote,
  updateLoanNote,
  deleteLoanNote,
}

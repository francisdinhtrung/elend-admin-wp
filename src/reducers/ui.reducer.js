const initialState = {
  sidebarShow: true,
  asideShow: false,
  theme: 'dark',
}

export const changeStateReducer = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'set':
      return { ...state, ...rest }
    default:
      return state
  }
}

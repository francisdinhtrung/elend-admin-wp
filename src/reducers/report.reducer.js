import { reportConstants } from '../constants'

const initialState = {
  isFetched: false,
  isFetching: false,
  reportBorrowers: [],
  reportLoans: [],
  reportLoanAnalysis: [],
}

const reportReducer = (state = initialState, action) => {
  switch (action.type) {
    case reportConstants.GET_REPORT_BORROWERS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
      })
    case reportConstants.GET_REPORT_BORROWERS_SUCCESS:
      const borrowers = []
      const data = action.response.data
      for (let i in data) {
        if (
          i === 'organizationId' ||
          i === 'totalBorrowersInPendingLoan' ||
          i === 'totalBorrowersInRejectedLoan'
        )
          continue
        const item = {}
        item.value = data[i]
        item.type = i
        item.bgColor = 'bg-primary-gradient'
        item.title = 'view.Report.TotalBorrowers'
        item.status = ''
        item.order = 0
        if (i === 'totalBorrowersInActiveLoan') {
          item.bgColor = 'bg-success-gradient'
          item.title = 'view.Report.TotalBorrowersInActiveLoan'
          item.order = 2
          item.status = 'active'
        } else if (i === 'totalBorrowersInCompletedLoan') {
          item.bgColor = 'bg-dark-gradient'
          item.title = 'view.Report.TotalBorrowersInCompletedLoan'
          item.order = 3
          item.status = 'completed'
        } else if (i === 'totalBlackList') {
          item.bgColor = 'bg-danger-gradient'
          item.title = 'view.Report.TotalBlackList'
          item.order = 1
          item.status = 'blacklist'
        }

        borrowers.push(item)
      }

      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
        reportBorrowers: borrowers.sort((a, b) =>
          a.order < b.order ? -1 : a.order > b.order ? 1 : 0,
        ),
      })
    case reportConstants.GET_REPORT_BORROWERS_FAILURE:
      return Object.assign({}, state, {
        errorFetch: action.error,
      })
    case reportConstants.GET_REPORT_LOANS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
      })
    case reportConstants.GET_REPORT_LOANS_SUCCESS:
      const loans = []
      const loanData = action.response.data
      for (let i in loanData) {
        if (
          i === 'organizationId' ||
          i === 'totalDurationLate' ||
          i === 'totalDurationToday' ||
          i === 'totalRejected' ||
          i === 'totalBlacklist'
        )
          continue
        const item = {}
        item.value = loanData[i]
        item.type = i
        item.bgColor = 'bg-info-gradient'
        item.title = 'view.Report.TotalLoans'
        item.order = 0
        item.status = ''
        if (i === 'totalActive') {
          item.bgColor = 'bg-success-gradient'
          item.title = 'view.Report.TotalActive'
          item.order = 2
          item.status = 'active'
        } else if (i === 'totalCompleted') {
          item.bgColor = 'bg-dark-gradient'
          item.title = 'view.Report.TotalCompleted'
          item.order = 3
          item.status = 'completed'
        } else if (i === 'totalDurationOverDue') {
          item.bgColor = 'bg-danger-gradient'
          item.title = 'view.Report.TotalDurationOverDue'
          item.order = 1
          item.status = 'overdue'
        }

        loans.push(item)
      }

      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
        reportLoans: loans.sort((a, b) => (a.order < b.order ? -1 : a.order > b.order ? 1 : 0)),
      })
    case reportConstants.GET_REPORT_LOANS_FAILURE:
      return Object.assign({}, state, {
        errorFetch: action.error,
      })
    case reportConstants.GET_REPORT_LOANS_ANALYSIS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
      })
    case reportConstants.GET_REPORT_LOANS_ANALYSIS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
        reportLoanAnalysis: action.response.data.reportLoanAnalysisList,
      })
    case reportConstants.GET_REPORT_LOANS_ANALYSIS_FAILURE:
      return Object.assign({}, state, {
        errorFetch: action.error,
      })
    default:
      return state
  }
}

export default reportReducer

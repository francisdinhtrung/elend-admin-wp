import moment from 'moment'

const getColorByStatus = (status, isHex) => {
  switch (status.toLowerCase()) {
    case 'active':
    case 'approved':
      return isHex ? '#2eb85c' : 'success'
    case 'inactive':
      return isHex ? '#636f83' : 'secondary'
    case 'pending':
      return isHex ? '#f9b115' : 'warning'
    case 'banned':
    case 'rejected':
      return isHex ? '#e55353' : 'danger'
    default:
      return isHex ? '#ced2d8' : 'primary'
  }
}

const getDayInArrearsColor = (item) => {
  const today = moment()
  const dueDate = moment(item.repaymentDueDate)
  const duration = dueDate.diff(today, 'days')
  if (duration < -7) return '#e55353'
  else if (duration <= -7) return '#ff9f43'
  return '#1e9ff2'
}

export const colorHelpers = {
  getColorByStatus,
  getDayInArrearsColor,
}

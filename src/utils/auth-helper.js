import { APP_TOKEN, APP_REFRESH_TOKEN } from 'src/constants/constants'

export const authHelpers = {
  setAccessToken,
  setFreshToken,
  getAccessToken,
  clearToken,
  isAuthenticated,
  findMatchRolePermissions,
}

function findMatchRolePermissions(array_1_small, array2_large) {
  let positions = Array.from(array2_large.entries()).reduce((acc, t) => {
    let index = t[0]
    let element = t[1]
    if (!acc.hasOwnProperty(element)) {
      acc[element] = []
    }
    acc[element].push(index)
    return acc
  }, {})
  let result = new Set()
  array_1_small.forEach((x) => {
    if (positions[x] === undefined) {
      return
    }
    positions[x].forEach((index) => result.add(index))
  })
  return Array.from(result)
}

function isAuthenticated() {
  const token = localStorage.getItem(APP_TOKEN)
  if (token === null) return false
  return token && token.length > 0
}

function clearToken() {
  localStorage.removeItem(APP_TOKEN)
  localStorage.removeItem(APP_REFRESH_TOKEN)
}

function setAccessToken(token) {
  localStorage.setItem(APP_TOKEN, token)
}

function setFreshToken(refreshToken) {
  localStorage.setItem(APP_REFRESH_TOKEN, refreshToken)
}

function getAccessToken() {
  localStorage.getItem(APP_TOKEN)
}

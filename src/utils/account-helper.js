export const accountHelpers = {
  getAccount,
  getMyOrganization,
}

function getAccount() {
  let userAccount
  try {
    userAccount = JSON.parse(localStorage.getItem('account'))
  } catch (error) {
    console.error('missing account!')
  }
  return userAccount
}

function getMyOrganization() {
  const account = getAccount()
  return account ? account.organizations[0] : null
}

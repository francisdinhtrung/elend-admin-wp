import moment from 'moment'

const formatter = new Intl.NumberFormat('vi-VN', {
  minimumFractionDigits: 0,
})

const currencyFormatter = (number) => {
  return formatter.format(Math.round(number))
}

const dateFormatter = (date) => {
  const hourValue = '00:00:00'
  return moment(date).utc(hourValue, 'HH:mm:ss').toISOString()
}

export const loanHelpers = {
  currencyFormatter,
  dateFormatter,
}

import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilBell,
  cilCalculator,
  cilChartPie,
  cilCursor,
  cilDrop,
  cilNotes,
  cilPencil,
  cilPeople,
  cilPuzzle,
  cilSpeedometer,
  cilStar,
  cilUser,
  cilMoney,
  cilMediaRecord,
} from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react-pro'
import { FaRegBuilding } from 'react-icons/fa'

const roleAdminPermissions = ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN']
const roleOrgPermissions = ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_ORGANIZAITION_ADMIN']

const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: 'info',
      text: 'NEW',
    },
    translatename: 'theSidebar.Dashboard',
  },
  {
    component: CNavItem,
    name: 'Organizations',
    translatename: 'theSidebar.Organizations',
    translatemyorgname: 'theSidebar.MyOrganization',
    to: '/organizations',
    icon: <FaRegBuilding className="nav-icon" />,
    permissions: roleOrgPermissions,
  },
  {
    component: CNavGroup,
    name: 'Users',
    icon: <CIcon icon={cilUser} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Employee',
        translatename: 'theSidebar.Accounts',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/users',
      },
      // {
      //   component: CNavItem,
      //   name: 'Customer',
      //   translatename: 'theSidebar.Customer',
      //   to: '/customers',
      // },
      {
        component: CNavItem,
        name: 'User Role',
        translatename: 'theSidebar.UserRole',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/users/role',
        permissions: roleAdminPermissions,
      },
    ],
  },
  {
    component: CNavGroup,
    name: 'Borrowers',
    icon: <CIcon icon={cilPeople} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'View Borrowers',
        translatename: 'theSidebar.ViewBorrowers',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/borrowers',
      },
      // {
      //   component: CNavItem,
      //   name: 'Pending Borrowers',
      //   translatename: 'theSidebar.PendingBorrowers',
      //   to: '/borrowers/pending',
      // },
      {
        component: CNavItem,
        name: 'Add Borrowers',
        translatename: 'theSidebar.AddBorrowers',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/borrowers/create',
      },
      {
        component: CNavItem,
        name: 'Active Loan',
        translatename: 'theSidebar.BorrowerActiveLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/borrowers/active-loans',
      },
      {
        component: CNavItem,
        name: 'Completed Loan',
        translatename: 'theSidebar.BorrowerCompletedLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/borrowers/completed-loans',
      },
      {
        component: CNavItem,
        name: 'Blacklist Borrowers',
        translatename: 'theSidebar.BlacklistBorrowers',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/borrowers/blacklist',
      },
    ],
  },
  {
    component: CNavGroup,
    name: 'Loan Management',
    icon: <CIcon icon={cilMoney} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Create Loan',
        translatename: 'theSidebar.CreateLoan',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans/create',
      },
      {
        component: CNavItem,
        name: 'View All Loans',
        translatename: 'theSidebar.ViewAllLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans',
      },
      {
        component: CNavItem,
        name: 'View Pending Loans',
        translatename: 'theSidebar.ViewPendingLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans/pending',
      },
      // {
      //   component: CNavItem,
      //   name: 'View Submitted Loans',
      //   translatename: 'theSidebar.ViewSubmittedLoans',
      //   icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
      //   to: '/loans/submitted',
      // },
      {
        component: CNavItem,
        name: 'View Approved Loans',
        translatename: 'theSidebar.ViewApprovedLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans/approved',
      },
      {
        component: CNavItem,
        name: 'View Active Loans',
        translatename: 'theSidebar.ViewActiveLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans/active',
      },
      {
        component: CNavItem,
        name: 'View Due Loans',
        translatename: 'theSidebar.ViewDueLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans/overdue',
      },
      {
        component: CNavItem,
        name: 'View Completed Loans',
        translatename: 'theSidebar.ViewCompletedLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans/completed',
      },
      {
        component: CNavItem,
        name: 'View Reject Loans',
        translatename: 'theSidebar.ViewRejectedLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans/rejected',
      },
      {
        component: CNavItem,
        name: 'View Blacklist Loans',
        translatename: 'theSidebar.ViewBlacklistLoans',
        icon: <CIcon icon={cilMediaRecord} height={8} customClassName="sub-nav-icon" />,
        to: '/loans/banned',
      },
      // {
      //   component: CNavItem,
      //   name: 'Calculator',
      //   translatename: 'theSidebar.LoanCalculator',
      //   to: '/users',
      // },
    ],
  },
  // {
  //   component: CNavTitle,
  //   name: 'Theme',
  // },
  // {
  //   component: CNavItem,
  //   name: 'Colors',
  //   to: '/theme/colors',
  //   icon: <CIcon icon={cilDrop} customClassName="nav-icon" />,
  // },
  // {
  //   component: CNavItem,
  //   name: 'Typography',
  //   to: '/theme/typography',
  //   icon: <CIcon icon={cilPencil} customClassName="nav-icon" />,
  // },
  // {
  //   component: CNavTitle,
  //   name: 'Components',
  // },
  // {
  //   component: CNavGroup,
  //   name: 'Base',
  //   to: '/base',
  //   icon: <CIcon icon={cilPuzzle} customClassName="nav-icon" />,
  //   items: [
  //     {
  //       component: CNavItem,
  //       name: 'Accordion',
  //       to: '/base/accordion',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Breadcrumb',
  //       to: '/base/breadcrumbs',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Cards',
  //       to: '/base/cards',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Carousel',
  //       to: '/base/carousels',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Collapse',
  //       to: '/base/collapses',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'List group',
  //       to: '/base/list-groups',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Navs & Tabs',
  //       to: '/base/navs',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Pagination',
  //       to: '/base/paginations',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Popovers',
  //       to: '/base/popovers',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Progress',
  //       to: '/base/progress',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Spinners',
  //       to: '/base/spinners',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Tables',
  //       to: '/base/tables',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Tooltips',
  //       to: '/base/tooltips',
  //     },
  //   ],
  // },
  // {
  //   component: CNavGroup,
  //   name: 'Buttons',
  //   to: '/buttons',
  //   icon: <CIcon icon={cilCursor} customClassName="nav-icon" />,
  //   items: [
  //     {
  //       component: CNavItem,
  //       name: 'Buttons',
  //       to: '/buttons/buttons',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Buttons groups',
  //       to: '/buttons/button-groups',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Dropdowns',
  //       to: '/buttons/dropdowns',
  //     },
  //   ],
  // },
  // {
  //   component: CNavGroup,
  //   name: 'Forms',
  //   icon: <CIcon icon={cilNotes} customClassName="nav-icon" />,
  //   items: [
  //     {
  //       component: CNavItem,
  //       name: 'Form Control',
  //       to: '/forms/form-control',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Select',
  //       to: '/forms/select',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Checks & Radios',
  //       to: '/forms/checks-radios',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Range',
  //       to: '/forms/range',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Input Group',
  //       to: '/forms/input-group',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Floating Labels',
  //       to: '/forms/floating-labels',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Layout',
  //       to: '/forms/layout',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Validation',
  //       to: '/forms/validation',
  //     },
  //   ],
  // },
  // {
  //   component: CNavItem,
  //   name: 'Charts',
  //   to: '/charts',
  //   icon: <CIcon icon={cilChartPie} customClassName="nav-icon" />,
  // },
  // {
  //   component: CNavGroup,
  //   name: 'Icons',
  //   icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
  //   items: [
  //     {
  //       component: CNavItem,
  //       name: 'CoreUI Free',
  //       to: '/icons/coreui-icons',
  //       badge: {
  //         color: 'success',
  //         text: 'NEW',
  //       },
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'CoreUI Flags',
  //       to: '/icons/flags',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'CoreUI Brands',
  //       to: '/icons/brands',
  //     },
  //   ],
  // },
  // {
  //   component: CNavGroup,
  //   name: 'Notifications',
  //   icon: <CIcon icon={cilBell} customClassName="nav-icon" />,
  //   items: [
  //     {
  //       component: CNavItem,
  //       name: 'Alerts',
  //       to: '/notifications/alerts',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Badges',
  //       to: '/notifications/badges',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Modal',
  //       to: '/notifications/modals',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Toasts',
  //       to: '/notifications/toasts',
  //     },
  //   ],
  // },
  // {
  //   component: CNavItem,
  //   name: 'Widgets',
  //   to: '/widgets',
  //   icon: <CIcon icon={cilCalculator} customClassName="nav-icon" />,
  //   badge: {
  //     color: 'info',
  //     text: 'NEW',
  //   },
  // },
  // {
  //   component: CNavTitle,
  //   name: 'Extras',
  // },
  // {
  //   component: CNavGroup,
  //   name: 'Pages',
  //   icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
  //   items: [
  //     {
  //       component: CNavItem,
  //       name: 'Login',
  //       to: '/login',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Register',
  //       to: '/register',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Error 404',
  //       to: '/404',
  //     },
  //     {
  //       component: CNavItem,
  //       name: 'Error 500',
  //       to: '/500',
  //     },
  //   ],
  // },
]

export default _nav

import React from 'react'

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))
const Colors = React.lazy(() => import('./views/theme/colors/Colors'))
const Typography = React.lazy(() => import('./views/theme/typography/Typography'))

// Base
const Accordion = React.lazy(() => import('./views/base/accordion/Accordion'))
const Breadcrumbs = React.lazy(() => import('./views/base/breadcrumbs/Breadcrumbs'))
const Cards = React.lazy(() => import('./views/base/cards/Cards'))
const Carousels = React.lazy(() => import('./views/base/carousels/Carousels'))
const Collapses = React.lazy(() => import('./views/base/collapses/Collapses'))
const ListGroups = React.lazy(() => import('./views/base/list-groups/ListGroups'))
const Navs = React.lazy(() => import('./views/base/navs/Navs'))
const Paginations = React.lazy(() => import('./views/base/paginations/Paginations'))
const Popovers = React.lazy(() => import('./views/base/popovers/Popovers'))
const Progress = React.lazy(() => import('./views/base/progress/Progress'))
const Spinners = React.lazy(() => import('./views/base/spinners/Spinners'))
const Tables = React.lazy(() => import('./views/base/tables/Tables'))
const Tooltips = React.lazy(() => import('./views/base/tooltips/Tooltips'))

// Buttons
const Buttons = React.lazy(() => import('./views/buttons/buttons/Buttons'))
const ButtonGroups = React.lazy(() => import('./views/buttons/button-groups/ButtonGroups'))
const LoadingButtons = React.lazy(() => import('./views/buttons/loading-buttons/LoadingButtons'))
const Dropdowns = React.lazy(() => import('./views/buttons/dropdowns/Dropdowns'))

//Forms
const ChecksRadios = React.lazy(() => import('./views/forms/checks-radios/ChecksRadios'))
const FloatingLabels = React.lazy(() => import('./views/forms/floating-labels/FloatingLabels'))
const FormControl = React.lazy(() => import('./views/forms/form-control/FormControl'))
const InputGroup = React.lazy(() => import('./views/forms/input-group/InputGroup'))
const Layout = React.lazy(() => import('./views/forms/layout/Layout'))
const MultiSelect = React.lazy(() => import('./views/forms/multi-select/MultiSelect'))
const Range = React.lazy(() => import('./views/forms/range/Range'))
const Select = React.lazy(() => import('./views/forms/select/Select'))
const Validation = React.lazy(() => import('./views/forms/validation/Validation'))

// Icons
const CoreUIIcons = React.lazy(() => import('./views/icons/coreui-icons/CoreUIIcons'))
const Flags = React.lazy(() => import('./views/icons/flags/Flags'))
const Brands = React.lazy(() => import('./views/icons/brands/Brands'))

// Notifications
const Alerts = React.lazy(() => import('./views/notifications/alerts/Alerts'))
const Badges = React.lazy(() => import('./views/notifications/badges/Badges'))
const Modals = React.lazy(() => import('./views/notifications/modals/Modals'))
const Toasts = React.lazy(() => import('./views/notifications/toasts/Toasts'))
const SmartTable = React.lazy(() => import('./views/smart-table/SmartTable'))
const RewardEligibilities = React.lazy(() => import('./views/pages/register/Eligibilities'))
// const Login = React.lazy(() => import('./views/examples/pages/login/Login'))
// const Register = React.lazy(() => import('./views/pages/register/Register'))
// const RewardConfirm = React.lazy(() => import('./views/pages/register/RewardConfirm'))
// const Thanks = React.lazy(() => import('./views/pages/register/Thanks'))
// const Page404 = React.lazy(() => import('./views/examples/pages/page404/Page404'))
// const Page500 = React.lazy(() => import('./views/examples/pages/page500/Page500'))

const Widgets = React.lazy(() => import('./views/widgets/Widgets'))

// Plugins
const Calendar = React.lazy(() => import('./views/plugins/calendar/Calendar'))
const Charts = React.lazy(() => import('./views/plugins/charts/Charts'))
const GoogleMaps = React.lazy(() => import('./views/plugins/google-maps/GoogleMaps'))

const Invoice = React.lazy(() => import('./views/apps/invoicing/Invoice'))

// your components
const Users = React.lazy(() => import('./views/users/components/Users'))
const UserRoles = React.lazy(() => import('./views/users/components/UserRoles'))
const EditUserRole = React.lazy(() => import('./views/users/components/UserRole'))
const Customers = React.lazy(() => import('./views/users/components/Customers'))
const User = React.lazy(() => import('./views/users/components/User'))
const Employers = React.lazy(() => import('./views/employers/employers'))
const EmployerCreation = React.lazy(() => import('./views/employers/employer.creation'))
const Organizations = React.lazy(() => import('./views/organization/components/organizations'))
const Organization = React.lazy(() => import('./views/organization/components/organization'))
const OrganizationCreation = React.lazy(() =>
  import('./views/organization/components/organizations.creation'),
)

const BorrowerCreation = React.lazy(() => import('./views/borrowers/components/borrower.creation'))
const Borrowers = React.lazy(() => import('./views/borrowers/components/borrowers'))
const Borrower = React.lazy(() => import('./views/borrowers/components/borrower'))
const BorrowerLoan = React.lazy(() => import('./views/borrowers/components/borrower-loan'))
const BlacklistBorrowers = React.lazy(() =>
  import('./views/borrowers/components/borrowers.blacklist'),
)

// Loan
const Loans = React.lazy(() => import('./views/loans/components/loans'))
const LoanCreate = React.lazy(() => import('./views/loans/components/loan.create'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme', name: 'Theme', component: Colors, exact: true },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', name: 'Base', component: Cards, exact: true },
  { path: '/base/accordion', name: 'Accordion', component: Accordion },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress', name: 'Progress', component: Progress },
  { path: '/base/spinners', name: 'Spinners', component: Spinners },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/loading-buttons', name: 'Loading Buttons', component: LoadingButtons },
  { path: '/buttons/dropdowns', name: 'Dropdowns', component: Dropdowns },
  { path: '/forms', name: 'Forms', component: FormControl, exact: true },
  { path: '/forms/form-control', name: 'Form Control', component: FormControl },
  { path: '/forms/select', name: 'Select', component: Select },
  { path: '/forms/multi-select', name: 'Multi Select', component: MultiSelect },
  { path: '/forms/checks-radios', name: 'Checks & Radios', component: ChecksRadios },
  { path: '/forms/range', name: 'Range', component: Range },
  { path: '/forms/input-group', name: 'Input Group', component: InputGroup },
  { path: '/forms/floating-labels', name: 'Floating Labels', component: FloatingLabels },
  { path: '/forms/layout', name: 'Layout', component: Layout },
  { path: '/forms/validation', name: 'Validation', component: Validation },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/brands', name: 'Brands', component: Brands },
  { path: '/notifications', name: 'Notifications', component: Alerts, exact: true },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/notifications/toasts', name: 'Toasts', component: Toasts },
  { path: '/plugins', name: 'Plugins', component: Calendar, exact: true },
  { path: '/plugins/calendar', name: 'Calendar', component: Calendar },
  { path: '/plugins/charts', name: 'Charts', component: Charts },
  { path: '/plugins/google-maps', name: 'GoogleMaps', component: GoogleMaps },
  { path: '/smart-table', name: 'Smart Table', component: SmartTable },
  { path: '/apps', name: 'Apps', component: Invoice, exact: true },
  { path: '/apps/invoicing', name: 'Invoice', component: Invoice, exact: true },
  { path: '/apps/invoicing/invoice', name: 'Invoice', component: Invoice },
  { path: '/apps/email', name: 'Email', exact: true },
  { path: '/apps/email/inbox', name: 'Inbox', exact: true },
  { path: '/apps/email/compose', name: 'Compose', exact: true },
  { path: '/apps/email/message', name: 'Message', exact: true },

  { path: '/danhsach', name: 'Eligibilities', component: RewardEligibilities },
  // { path: '/login', name: 'Login', component: Login },
  // { path: '/register', name: 'Register', component: Register },
  // { path: '/confirm-reward', name: 'Register', component: RewardConfirm },
  // { path: '/thanks/', exact: true, name: 'Thanks', component: Thanks },
  // { path: '/thanks/:id', exact: true, name: 'Thanks', component: Thanks },
  // { path: '/404', name: '404', component: Page404 },
  // { path: '/500', name: '500', component: Page500 },
  { path: '/widgets', name: 'Widgets', component: Widgets },

  { path: '/users', exact: true, name: 'Users', component: Users },
  { path: '/users/create', exact: true, name: 'New User', component: User },
  { path: '/users/edit/:id', exact: true, name: 'Edit User', component: User },
  { path: '/customers', exact: true, name: 'Customers', component: Customers },
  { path: '/customers/edit/:id', exact: true, name: 'Customers Details', component: User },
  { path: '/users/role', exact: true, name: 'User Roles', component: UserRoles },
  { path: '/users/role/create', exact: true, name: 'New User Roles', component: EditUserRole },
  { path: '/users/role/edit/:id', exact: true, name: 'Edit User Roles', component: EditUserRole },
  { path: '/employers', name: 'Employers', component: Employers },
  { path: '/organizations', exact: true, name: 'Organizations', component: Organizations },
  { path: '/organizations/create', exact: true, name: 'Create', component: OrganizationCreation },
  { path: '/organizations/:id', exact: true, name: 'Edit', component: Organization },

  // borrowers
  { path: '/borrowers', exact: true, name: 'Borrowers', component: Borrowers },
  { path: '/borrowers/active-loans', exact: true, name: 'Active Borrowers', component: Borrowers },
  {
    path: '/borrowers/completed-loans',
    exact: true,
    name: 'Completed Borrowers',
    component: Borrowers,
  },
  { path: '/borrowers/blacklist', exact: true, name: 'Borrowers', component: BlacklistBorrowers },
  { path: '/borrowers/create', exact: true, name: 'Create', component: BorrowerCreation },
  { path: '/borrowers/:id', exact: true, name: 'Borrower Profile', component: Borrower },
  { path: '/borrowers/:id/edit', exact: true, name: 'Edit Profile', component: BorrowerCreation },
  { path: '/borrowers/:id/loan', exact: true, name: 'Borrower Profile', component: BorrowerLoan },
  // loans
  { path: '/loans', exact: true, name: 'View All Loans', component: Loans },
  { path: '/loans/pending', exact: true, name: 'View Pending Loans', component: Loans },
  { path: '/loans/submitted', exact: true, name: 'View Submitted Loans', component: Loans },
  { path: '/loans/approved', exact: true, name: 'View Approved Loans', component: Loans },
  { path: '/loans/active', exact: true, name: 'View Active Loans', component: Loans },
  { path: '/loans/completed', exact: true, name: 'View Completed Loans', component: Loans },
  { path: '/loans/rejected', exact: true, name: 'View Rejected Loans', component: Loans },
  { path: '/loans/overdue', exact: true, name: 'View Due Loans', component: Loans },
  { path: '/loans/banned', exact: true, name: 'View Blacklist Loans', component: Loans },
  { path: '/loans/create', exact: true, name: 'Create Loan', component: LoanCreate },
  { path: '/loans/:id', exact: true, name: 'Detail Loan', component: BorrowerLoan },
  { path: '/loans/:id/edit', exact: true, name: 'Edit Loan', component: LoanCreate },
  {
    path: '/employers/create',
    name: 'Creation',
    component: EmployerCreation,
  },
]

export default routes

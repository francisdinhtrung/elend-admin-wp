import api from '../../utils/api'
import * as t from './actionTypes'
import { toast } from 'react-toastify'

function createAccount(data) {
  return (dispatch) => {
    dispatch(request(data))
    return api.registerService
      .createAccount(data)
      .then((response) => {
        dispatch(success(response))
        toast.success('Create Account Successfully')
        setTimeout(() => {
          window.location.href = `${window.location.origin}/#/`
        }, 5000)
      })
      .catch((error) => {
        dispatch(failure(error))
      })
  }

  function request() {
    return { type: t.CREATE_ACCOUNT_REQUEST }
  }

  function success(data) {
    return { type: t.CREATE_ACCOUNT_SUCCESS, data }
  }

  function failure(error) {
    return { type: t.CREATE_ACCOUNT_FAILURE, error }
  }
}

function activateAccount(data) {
  return (dispatch) => {
    dispatch(request(data))
    return api.registerService
      .activateAccount(data)
      .then((response) => {
        dispatch(success(response))
      })
      .catch((error) => {
        dispatch(failure(error))
      })
  }

  function request() {
    return { type: t.ACTIVATE_ACCOUNT_REQUEST }
  }

  function success(data) {
    return { type: t.ACTIVATE_ACCOUNT_SUCCESS, data }
  }

  function failure(error) {
    return { type: t.ACTIVATE_ACCOUNT_FAILURE, error }
  }
}

export const registerActions = {
  createAccount,
  activateAccount,
}

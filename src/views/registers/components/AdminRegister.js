import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
  CFormSelect,
  CFormFeedback,
  CSpinner,
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser, cilSortNumericUp, cilApps } from '@coreui/icons'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { registerActions } from '../actions'

const AdminRegister = () => {
  const dispatch = useDispatch()
  const { t } = useTranslation()
  const [errors, setErrors] = useState({})
  const [isLoading, setIsLoading] = useState(false)
  const schema = Yup.object({
    username: Yup.string().required(t('messages.validations.requiredField')),
    password: Yup.string().required(t('messages.validations.requiredField')),
    clientName: Yup.string().required(t('messages.validations.requiredField')),
  })
  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
      repeatPassword: '',
      licence: '',
      clientName: '',
    },
    enableReinitialize: true,
    validationSchema: schema,
  })

  const isValidation = async () => {
    const validateForms = await formik.validateForm(formik.values)
    setErrors(validateForms)
    return Object.keys(validateForms).length === 0
  }
  const handleCreateAccount = async () => {
    const isValid = await isValidation()
    if (!isValid) {
      return
    }
    const data = formik.values
    if (data.password !== data.repeatPassword) {
      return
    }
    const payLoad = {
      username: data.username,
      password: data.password,
      licence: data.licence || 'ELEND-2@22',
      clientName: data.clientName,
      clientDesc: 'Manage clients and participants',
      rootUrl: 'https://cc.fappar.com',
      birthDate: '',
    }
    if (payLoad) {
      setIsLoading(true)
      await dispatch(registerActions.createAccount(payLoad))
      setIsLoading(false)
    }
  }

  const handleComparePassword = (e) => {
    const data = formik.values
    if (data.repeatPassword.length > 0 && data.password !== data.repeatPassword) {
      setErrors({ repeatPassword: true })
    } else {
      setErrors({ repeatPassword: false })
    }
  }

  const renderSubmitButton = () => {
    return isLoading ? (
      <div className="d-grid">
        <CButton type="button" color="primary" onClick={handleCreateAccount} disabled={isLoading}>
          <CSpinner color="light" component="span" size="sm" aria-hidden="true" />
          Creating Account ...
        </CButton>
      </div>
    ) : (
      <div className="d-grid">
        <CButton type="button" color="primary" onClick={handleCreateAccount}>
          Creating Account
        </CButton>
      </div>
    )
  }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm>
                  <h1>Register</h1>
                  <p className="text-medium-emphasis">Create your account</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      id="username"
                      placeholder="Email/Username"
                      autoComplete="none"
                      type="text"
                      name="username"
                      invalid={errors.username && errors.username.length > 0}
                      {...formik.getFieldProps('username')}
                    />
                    <CFormFeedback invalid> {errors.username} </CFormFeedback>
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      id="passwordId"
                      type="password"
                      placeholder="Password"
                      autoComplete="new-password"
                      name="password"
                      invalid={errors.password && errors.password.length > 0}
                      onKeyUp={handleComparePassword}
                      {...formik.getFieldProps('password')}
                    />
                    <CFormFeedback invalid> {errors.password} </CFormFeedback>
                  </CInputGroup>
                  <CInputGroup className="mb-4">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Repeat password"
                      autoComplete="new-password"
                      id="repeatPasswordId"
                      name="repeatPassword"
                      onKeyUp={handleComparePassword}
                      invalid={formik.values.password !== formik.values.repeatPassword}
                      {...formik.getFieldProps('repeatPassword')}
                    />
                    <CFormFeedback invalid> Not Matching </CFormFeedback>
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilSortNumericUp} />
                    </CInputGroupText>
                    <CFormInput
                      type="text"
                      placeholder="Licence"
                      autoComplete="licence"
                      name="licence"
                      disabled={true}
                      {...formik.getFieldProps('licence')}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilApps} />
                    </CInputGroupText>
                    <CFormSelect
                      aria-label="Client Name"
                      name="clientName"
                      id="clientNameId"
                      invalid={errors.clientName && errors.clientName.length > 0}
                      {...formik.getFieldProps('clientName')}
                    >
                      <option>Select Client Name</option>
                      <option value="Digital-Eastern-App">Digital-Eastern-App</option>
                      <option value="account">account</option>
                      <option value="web_app">web_app</option>
                    </CFormSelect>
                    <CFormFeedback invalid> {errors.clientName} </CFormFeedback>
                  </CInputGroup>
                  {renderSubmitButton()}
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default AdminRegister

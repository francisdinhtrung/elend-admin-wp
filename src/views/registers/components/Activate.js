import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { CButton, CContainer, CSpinner } from '@coreui/react-pro'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router'
import queryString from 'query-string'
import { registerActions } from '../actions'
import { useDispatch, useSelector } from 'react-redux'

const Activate = ({ match }) => {
  const dispatch = useDispatch()
  const { i18n, t } = useTranslation()
  let navigate = useHistory()
  const location = useLocation()
  const params = queryString.parse(location.search)
  const hasError = useSelector((state) => state.register.hasError)
  console.log('hasError', hasError)

  const getLangKey = () => {
    return params && params.langkey ? params.langkey : 'en'
  }

  const getActivateKey = () => {
    return params && params.key ? params.key : null
  }

  const handleChangeLanguage = (lng) => {
    i18n.changeLanguage(lng)
  }

  const handleLogin = () => {
    navigate.push('/#/login')
  }

  const activateAccount = async () => {
    const payLoad = {
      key: getActivateKey(),
    }
    if (payLoad && payLoad.key) {
      await dispatch(registerActions.activateAccount(payLoad))
    }
  }

  const [langKey, setLangKey] = useState(getLangKey())
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    handleChangeLanguage(langKey)
    const processActivation = async () => {
      await activateAccount()
    }
    setTimeout(() => {
      processActivation()
        .then((res) => {
          console.log('Activate successfully!')
        })
        .finally(() => {
          setIsLoading(false)
        })
    }, 3000)
  }, [])
  return (
    <CContainer
      fluid
      className="d-flex flex-row justify-content-center min-vh-100 activate-container"
    >
      <div className="activate-welcome-group">
        {!hasError && (
          <>
            <span className="activate-welcome">{t('view.activate.welcome')}</span>
          </>
        )}
        {!isLoading && !hasError ? (
          <>
            <span className="activate-message">{t('view.activate.welcomeMsg')}</span>
            <CButton onClick={handleLogin} className="activate-button" color="info">
              {t('view.activate.loginBtnText')}
            </CButton>
          </>
        ) : !isLoading && hasError ? (
          <>
            <div className="activate-message">
              <span>{t('view.activate.errorMsg')}</span>
            </div>
          </>
        ) : (
          <>
            <div className="activate-message">
              <span>{t('view.activate.processMsg')}</span>
              <CSpinner color="light" component="span" aria-hidden="true" />
            </div>
          </>
        )}
      </div>
    </CContainer>
  )
}

Activate.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
}

export default Activate

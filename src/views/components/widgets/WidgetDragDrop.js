import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { CCol, CRow, CButton } from '@coreui/react-pro'
import { FaExchangeAlt } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'

const WidgetDragDrop = (props) => {
  const { t } = useTranslation()
  const { dataSource, onFinish } = props
  const [defaultRoles, setDefaultRoles] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  const categoryEnum = {
    availableRoles: 'availableRoles',
    effectiveRoles: 'effectiveRoles',
  }

  useEffect(() => {
    if (!defaultRoles || defaultRoles.length === 0) {
      setDefaultRoles(dataSource)
    }
  }, [dataSource])

  // init drag roles
  const onDragStart = (ev, id) => {
    ev.dataTransfer.setData('id', id)
  }

  const onDragOver = (ev) => {
    ev.preventDefault()
  }

  const onDrop = (ev, cat) => {
    let id = ev.dataTransfer.getData('id')
    const newRoles = [...defaultRoles]
    const selectedRoleIndex = newRoles.findIndex((r) => r.name === id)
    if (selectedRoleIndex > -1) {
      newRoles[selectedRoleIndex].category = cat
      newRoles[selectedRoleIndex].selected = false
      const effectiveRoles = newRoles[selectedRoleIndex].effectiveRoles
      if (effectiveRoles && effectiveRoles.length > 0) {
        effectiveRoles.forEach((eRole) => {
          const existingSubRoleIndex = newRoles.findIndex((r) => r.name === eRole)
          if (existingSubRoleIndex > -1) {
            newRoles[existingSubRoleIndex].category = cat
            newRoles[existingSubRoleIndex].selected = false
          }
        })
      }
    }
    setDefaultRoles(newRoles)
    handleOnDropCompleted(newRoles.filter((role) => role.category === categoryEnum.effectiveRoles))
  }

  const handleOnDropCompleted = (selectedRoles) => {
    onFinish(
      selectedRoles.map((r) => {
        return { name: r.name }
      }),
    )
  }

  const onFocusRole = (ev, item) => {
    const newRoles = [...defaultRoles]
    newRoles.forEach((r) => {
      if (r.name === item.name) {
        r.selected = true
        ev.currentTarget.classList.add('selected')
      } else {
        r.selected = false
      }
    })
    setDefaultRoles(newRoles)
  }

  const onBlur = (ev) => {
    ev.currentTarget.classList.remove('selected')
  }

  const handleToSwitchRole = (ev, cat) => {
    const newRoles = [...defaultRoles]
    newRoles.forEach((role) => {
      if (role.selected) {
        role.category = cat
        role.selected = false

        const effectiveRoles = role.effectiveRoles
        if (effectiveRoles && effectiveRoles.length > 0) {
          effectiveRoles.forEach((eRole) => {
            const existingSubRoleIndex = newRoles.findIndex((r) => r.name === eRole)
            if (existingSubRoleIndex > -1) {
              newRoles[existingSubRoleIndex].category = cat
              newRoles[existingSubRoleIndex].selected = false
            }
          })
        }
      }
    })
    setDefaultRoles(newRoles)
  }

  return (
    <CRow className="mt-4 role-container-drag">
      <CCol
        className="available-role"
        onDragOver={(e) => onDragOver(e)}
        onDrop={(e) => {
          onDrop(e, categoryEnum.availableRoles)
        }}
      >
        <span className="drag-role-header font-weight-bold">
          {t('view.UserRole.AvailableRoles')}
        </span>
        <div className="drag-role-list">
          {!isLoading &&
            defaultRoles.map((t) => {
              return t.category === categoryEnum.availableRoles ? (
                <div
                  key={t.name}
                  onDragStart={(e) => onDragStart(e, t.name)}
                  draggable
                  onFocus={(e) => onFocusRole(e, t)}
                  onBlur={(e) => onBlur(e)}
                  className={t.selected ? 'draggable selected' : 'draggable'}
                  tabIndex="0"
                >
                  {t.name}
                </div>
              ) : null
            })}
        </div>
        <CButton
          color="primary mt-3"
          onClick={(e) => handleToSwitchRole(e, categoryEnum.effectiveRoles)}
        >
          {t('common.AddSelected')}
        </CButton>
      </CCol>
      <FaExchangeAlt className="drag-icon" />
      <CCol
        className="droppable"
        onDragOver={(e) => onDragOver(e)}
        onDrop={(e) => onDrop(e, categoryEnum.effectiveRoles)}
      >
        <span className="drag-role-header font-weight-bold">
          {t('view.UserRole.EffectiveRoles')}
        </span>
        <div className="drag-role-list">
          {!isLoading &&
            defaultRoles.map((t) => {
              return t.category === categoryEnum.effectiveRoles ? (
                <div
                  key={t.name}
                  onDragStart={(e) => onDragStart(e, t.name)}
                  draggable
                  onFocus={(e) => onFocusRole(e, t)}
                  onBlur={(e) => onBlur(e)}
                  className={t.selected ? 'draggable selected' : 'draggable'}
                  tabIndex="0"
                >
                  {t.name}
                </div>
              ) : null
            })}
        </div>
        <CButton
          color="secondary mt-3"
          onClick={(e) => handleToSwitchRole(e, categoryEnum.availableRoles)}
        >
          {t('common.RemoveSelected')}
        </CButton>
      </CCol>
    </CRow>
  )
}

WidgetDragDrop.propTypes = {
  dataSource: PropTypes.array.isRequired,
  onFinish: PropTypes.func,
}

export default WidgetDragDrop

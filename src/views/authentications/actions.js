import api from '../../utils/api'
import * as t from './actionTypes'
import { toast } from 'react-toastify'

function resetPassword(data) {
  return (dispatch) => {
    dispatch(request(data))
    return api.authService
      .resetPassword(data)
      .then((response) => {
        dispatch(success(response))
      })
      .catch((error) => {
        // toast.error('Something went wrong! Please try again.')
        dispatch(failure(error))
      })
  }

  function request() {
    return { type: t.RESET_PASSWORD_REQUEST }
  }

  function success(data) {
    return { type: t.RESET_PASSWORD_SUCCESS, data }
  }

  function failure(error) {
    return { type: t.RESET_PASSWORD_FAILURE, error }
  }
}

export const authentications = {
  resetPassword,
}

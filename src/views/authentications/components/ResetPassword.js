import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import {
  CButton,
  CContainer,
  CSpinner,
  CForm,
  CFormLabel,
  CFormInput,
  CRow,
  CCol,
  CFormFeedback,
  CImage,
  CAlert,
} from '@coreui/react-pro'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router'
import queryString from 'query-string'
import { authentications } from '../actions'
import { useDispatch, useSelector } from 'react-redux'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { toast } from 'react-toastify'

const ResetPassword = ({ match }) => {
  const dispatch = useDispatch()
  const { i18n, t } = useTranslation()
  let navigate = useHistory()
  const location = useLocation()
  const params = queryString.parse(location.search)

  const getLangKey = () => {
    return params && params.langkey ? params.langkey : 'en'
  }
  const getResetKey = () => {
    return params && params.key ? params.key : ''
  }

  const handleChangeLanguage = (lng) => {
    i18n.changeLanguage(lng)
  }
  const resetPasswordSuccess = useSelector((state) => state.authentication.resetPasswordSuccess)
  const hasError = useSelector((state) => state.authentication.hasError)
  const isLoading = useSelector((state) => state.authentication.isLoading)
  const [langKey, setLangKey] = useState(getLangKey())
  const [errors, setErrors] = useState({})
  const strongPasswordReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/
  const schema = Yup.object({
    newPassword: Yup.string()
      .matches(strongPasswordReg, t('messages.validations.passwordStrongRequired'))
      .required(t('messages.validations.requiredField')),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref('newPassword'), null], t('messages.validations.passwordNotMatch'))
      .required(t('messages.validations.confirmPasswordRequired')),
  })
  const formik = useFormik({
    initialValues: {
      newPassword: '',
      confirmPassword: '',
    },
    enableReinitialize: true,
    validationSchema: schema,
  })
  const isValidation = async () => {
    const validateForms = await formik.validateForm(formik.values)
    setErrors(validateForms)
    return Object.keys(validateForms).length === 0
  }
  const handleLogin = () => {
    navigate.push('/#/login')
  }
  const handleResetPassword = async () => {
    const data = formik.values
    const isValid = await isValidation()
    if (!isValid) {
      return 0
    }
    if (getResetKey().length === 0) {
      toast.error(t('view.resetPassword.errorMsg'))
    } else {
      const payLoad = {
        key: getResetKey(),
        newPassword: data.newPassword,
      }
      if (payLoad) {
        await dispatch(authentications.resetPassword(payLoad))
      }
    }
  }

  const renderResetPasswordButton = () => {
    return isLoading ? (
      <CButton type="button" color="primary" onClick={handleResetPassword} disabled={isLoading}>
        {t('view.resetPassword.buttonText')}
        <CSpinner color="light" component="span" size="sm" aria-hidden="true" />
      </CButton>
    ) : (
      <CButton type="button" color="primary" onClick={handleResetPassword}>
        {t('view.resetPassword.buttonText')}
      </CButton>
    )
  }
  useEffect(() => {
    handleChangeLanguage(langKey)
  }, [])

  return (
    <CContainer fluid md>
      {resetPasswordSuccess ? (
        <div className="authen-page row justify-content-md-center">
          <CCol />
          <CCol sm={8} md={6} className="px-4 py-4">
            <div className="reset-password-success">
              <div className="clearfix">
                <CImage
                  align="center"
                  rounded
                  src="/images/24px-approve-export-fill-2.svg"
                  width={100}
                  height={100}
                />
              </div>
              <h2>{t('view.resetPassword.successMsg')}</h2>
              <p>{t('view.resetPassword.successActionMsg')}</p>
              <CButton type="button" color="primary" onClick={handleLogin}>
                {t('view.resetPassword.buttonLoginText')}
              </CButton>
            </div>
          </CCol>
          <CCol />
        </div>
      ) : (
        <CRow>
          <CCol />
          <CCol sm={8} md={6}>
            <CForm className="px-4 py-4">
              <h1>{t('view.resetPassword.title')}</h1>
              <div className="mb-3">
                <CFormLabel htmlFor="newPassword">{t('view.resetPassword.newPassword')}</CFormLabel>
                <CFormInput
                  type="password"
                  id="newPassword"
                  placeholder={t('view.User.Password')}
                  name="newPassword"
                  invalid={errors.newPassword && errors.newPassword.length > 0}
                  {...formik.getFieldProps('newPassword')}
                />
                <CFormFeedback invalid> {errors.newPassword} </CFormFeedback>
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="confirmPassword">
                  {t('view.resetPassword.confirmPassword')}
                </CFormLabel>
                <CFormInput
                  type="password"
                  id="confirmPassword"
                  name="confirmPassword"
                  placeholder={t('view.User.Password')}
                  invalid={errors.confirmPassword && errors.confirmPassword.length > 0}
                  {...formik.getFieldProps('confirmPassword')}
                />
                <CFormFeedback invalid>{errors.confirmPassword}</CFormFeedback>
              </div>
              {renderResetPasswordButton()}
              {hasError && (
                <CAlert color="danger" className="mt-3">
                  {t('view.resetPassword.requestErrorMsg')}
                </CAlert>
              )}
            </CForm>
          </CCol>
          <CCol />
        </CRow>
      )}
    </CContainer>
  )
}

ResetPassword.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
}

export default ResetPassword

import React, { useEffect } from 'react'
import { CRow, CCol, CButton } from '@coreui/react-pro'
import { FaWallet } from 'react-icons/all'
import { useDispatch, useSelector } from 'react-redux'
import { reportActions } from '../../../actions/report.actions'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

const ReportLoans = () => {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const history = useHistory()
  const reportLoans = useSelector((state) => state.report.reportLoans)

  useEffect(() => {
    dispatch(reportActions.getReportLoans(''))
  }, [dispatch])

  const navigateToPage = (item) => {
    switch (item.status) {
      case 'active':
        history.push('/loans/active')
        break
      case 'completed':
        history.push('/loans/completed')
        break
      case 'overdue':
        history.push('/loans/overdue')
        break
      default:
        history.push('/loans')
        break
    }
  }

  const renderStateItem = (item) => {
    return (
      <CCol key={item.order} sm={6} lg={3} className="mb-3">
        <div
          className={`custom-stat-item ${item.bgColor} text-white box-radius-10`}
          onClick={() => navigateToPage(item)}
        >
          <div className="custom-stat-icon">
            <FaWallet className="fa-icon" />
          </div>
          <div className="custom-stat-detail">
            <div className="number">
              <span className="amount">{item.value}</span>
            </div>
            <div className="description">
              <span>{t(item.title)}</span>
            </div>
            {/*<CButton className="text--small mt-3" color="light">*/}
            {/*  {t('common.ViewAll')}*/}
            {/*</CButton>*/}
          </div>
        </div>
      </CCol>
    )
  }

  return (
    <CRow className="mt-lg-4 mb-4">
      <CCol>
        <h5 className="mb-4">{t('view.Report.LoanStat')}</h5>
        <CRow>
          {reportLoans &&
            reportLoans.length > 0 &&
            reportLoans.map((item) => {
              return renderStateItem(item)
            })}
        </CRow>
      </CCol>
    </CRow>
  )
}
export default ReportLoans

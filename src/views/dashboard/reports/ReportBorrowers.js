import React, { useEffect } from 'react'
import { CRow, CCol, CButton } from '@coreui/react-pro'
import { FaUsers } from 'react-icons/all'
import { useDispatch, useSelector } from 'react-redux'
import { reportActions } from '../../../actions/report.actions'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

const ReportBorrowers = () => {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const history = useHistory()
  const reportBorrowers = useSelector((state) => state.report.reportBorrowers)

  useEffect(() => {
    dispatch(reportActions.getReportBorrowers(''))
  }, [dispatch])

  const navigateToPage = (item) => {
    switch (item.status) {
      case 'active':
        history.push('/borrowers/active-loans')
        break
      case 'completed':
        history.push('/borrowers/completed-loans')
        break
      case 'blacklist':
        history.push('/borrowers/blacklist')
        break
      default:
        history.push('/borrowers')
        break
    }
  }

  const renderStateItem = (item) => {
    return (
      <CCol key={item.order} sm={6} lg={3} className="mb-3">
        <div
          className={`custom-stat-item ${item.bgColor} text-white box-radius-10`}
          onClick={() => navigateToPage(item)}
        >
          <div className="custom-stat-icon">
            <FaUsers className="fa-icon" />
          </div>
          <div className="custom-stat-detail">
            <div className="number">
              <span className="amount">{item.value}</span>
            </div>
            <div className="description">
              <span>{t(item.title)}</span>
            </div>
            {/*<CButton className="text--small mt-3" color="light">*/}
            {/*  {t('common.ViewAll')}*/}
            {/*</CButton>*/}
          </div>
        </div>
      </CCol>
    )
  }

  return (
    <CRow className="mt-lg-4 mb-4">
      <CCol>
        <h5 className="mb-4">{t('view.Report.BorrowerStat')}</h5>
        <CRow>
          {reportBorrowers &&
            reportBorrowers.length > 0 &&
            reportBorrowers.map((item) => {
              return renderStateItem(item)
            })}
        </CRow>
      </CCol>
    </CRow>
  )
}
export default ReportBorrowers

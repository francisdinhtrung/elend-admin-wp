import React, { useEffect } from 'react'
import { CRow, CCol } from '@coreui/react-pro'
import { useDispatch, useSelector } from 'react-redux'
import { reportActions } from '../../../actions/report.actions'
import { useTranslation } from 'react-i18next'
import { loanHelpers } from '../../../utils/loan-helper'

const ReportLoanAnalysis = () => {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const reportLoanAnalysis = useSelector((state) => state.report.reportLoanAnalysis)
  const currencyFormatter = (item) => {
    return loanHelpers.currencyFormatter(item)
  }

  const renderStatisticItem = (item, index) => {
    const title =
      item.loanStatus === 'ACTIVE'
        ? t('view.Report.TotalActiveLoans')
        : t('view.Report.TotalCompletedLoans')

    const bgColor = item.loanStatus === 'ACTIVE' ? 'bg-success-gradient' : 'bg-info-gradient'
    const totalAmountRepaidDerived =
      item.totalPrincipalRepaidDerived +
      item.totalInterestRepaidDerived +
      item.totalPenaltyDerived +
      item.totalFeeDerived
    const totalPrincipalRunningAmount = item.totalPrincipal - item.totalPrincipalRepaidDerived
    return (
      <CCol key={index} xl={12}>
        <div className={`widget-three ${bgColor} text-white box-radius-10`}>
          <h4>{title}</h4>
          <div className="widget-three__content">
            <CRow>
              <CCol xl={4} className="text-amount">
                {t('view.Report.TotalRunningAmount')}
              </CCol>
              <CCol className="text-amount numbers">{currencyFormatter(item.totalPrincipal)}</CCol>
            </CRow>
            <CRow>
              <CCol xl={4} className="text-amount">
                {t('view.Report.TotalPrincipalRunningAmount')}
              </CCol>
              <CCol className="text-amount numbers">
                {currencyFormatter(totalPrincipalRunningAmount)}
              </CCol>
            </CRow>
            <hr />
            <CRow>
              <CCol xl={4} className="text-amount">
                {t('view.Report.TotalPrincipalRepaidDerived')}
              </CCol>
              <CCol className="text-amount numbers">
                {currencyFormatter(item.totalPrincipalRepaidDerived)}
              </CCol>
            </CRow>
            <CRow>
              <CCol xl={4} className="text-amount">
                {t('view.Report.TotalInterestRepaidDerived')}
              </CCol>
              <CCol className="text-amount numbers">
                {currencyFormatter(item.totalInterestRepaidDerived)}
              </CCol>
            </CRow>
            <CRow>
              <CCol xl={4} className="text-amount">
                {t('view.Report.TotalPenaltyRepaidDerived')}
              </CCol>
              <CCol className="text-amount numbers">
                {currencyFormatter(item.totalPenaltyDerived)}
              </CCol>
            </CRow>
            <CRow>
              <CCol xl={4} className="text-amount">
                {t('view.Report.TotalFeeRepaidDerived')}
              </CCol>
              <CCol className="text-amount numbers">{currencyFormatter(item.totalFeeDerived)}</CCol>
            </CRow>
            <hr />
            <CRow>
              <CCol xl={4} className="text-amount">
                {t('view.Report.TotalAmountRepaidDerived')}
              </CCol>
              <CCol className="text-amount numbers">
                {currencyFormatter(totalAmountRepaidDerived)}
              </CCol>
            </CRow>
          </div>
        </div>
      </CCol>
    )
  }

  useEffect(() => {
    dispatch(reportActions.getReportLoanAnalysis(''))
  }, [dispatch])
  return (
    <CRow className="mt-lg-5 mb-4">
      {reportLoanAnalysis &&
        reportLoanAnalysis.length > 0 &&
        reportLoanAnalysis.map((item, index) => {
          return (
            <CCol key={index}>
              <CRow key={index}>{renderStatisticItem(item, index)}</CRow>
            </CCol>
          )
        })}
    </CRow>
  )
}

export default ReportLoanAnalysis

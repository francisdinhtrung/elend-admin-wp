import React, { lazy } from 'react'

const ReportBorrowers = lazy(() => import('./reports/ReportBorrowers'))
const ReportLoans = lazy(() => import('./reports/ReportLoans'))
const ReportLoanAnalysis = lazy(() => import('./reports/ReportLoanAnalysis'))

const Dashboard = () => {
  return (
    <>
      <ReportBorrowers />
      <ReportLoans />
      <ReportLoanAnalysis />
    </>
  )
}

export default Dashboard

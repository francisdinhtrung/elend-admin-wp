import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CCollapse,
  CSmartTable,
  CSmartPagination,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CModal,
  CLink,
  CFormTextarea,
} from '@coreui/react-pro'
import { loanActions } from '../actions'
import { colorHelpers } from '../../../utils/color-helper'
import moment from 'moment'
import { FaMotorcycle, FaTimes, FaCheck, FaPhoneAlt } from 'react-icons/fa'
import { accountHelpers } from '../../../utils/account-helper'
import { loanHelpers } from '../../../utils/loan-helper'

const Loans = ({ match }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const account = accountHelpers.getAccount()
  const loans = useSelector((state) => state.loans.loans)
  const isFetching = useSelector((state) => state.loans.isFetching)
  const itemsPerPage = useSelector((state) => state.loans.itemsPerPage)
  const totalPages = useSelector((state) => state.loans.totalPages)
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)
  const [details, setDetails] = useState([])
  const [danger, setDanger] = useState(false)
  const [selectedLoan, setSelectedLoan] = useState(null)
  const [isAddNote, setIsAddNote] = useState(false)
  const [selectedNote, setSelectedNote] = useState({
    description: '',
  })

  let status = ''
  const lastIndexOf = match.url.lastIndexOf('/')
  if (lastIndexOf > 0) {
    status = match.url.substring(lastIndexOf + 1, match.url.length).toUpperCase()
  }

  const pageChange = (newPage) => {
    let url = `/loans?page=${newPage === 0 ? 1 : newPage}`
    if (status && status.length > 0) {
      url = `/loans/${status.toLowerCase()}?page=${newPage === 0 ? 1 : newPage}`
    }
    currentPage !== newPage && history.push(url)
  }

  const getAllLoans = async () => {
    const payload = {
      page: currentPage > 1 ? currentPage - 1 : 0,
      size: itemsPerPage,
    }
    if (status && status.length > 0) {
      payload.status = status
    }
    dispatch(loanActions.getAllLoans(payload))
    setPage(currentPage)
  }

  const getBadge = (status) => {
    if (!status) return
    return colorHelpers.getColorByStatus(status.toLowerCase(), false)
  }

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  const handleToView = (item, index, e) => {
    if (e === 'action') return
    history.push(`/loans/${item.id}`)
  }

  const handleToEdit = (item, index, e) => {
    if (e === 'action') return
    history.push(`/loans/${item.id}/edit`)
  }

  const handleDeleteLoan = async () => {
    setDanger(!danger)
    dispatch(loanActions.deleteLoan(selectedLoan.id))
  }

  const getShortLoanId = (id) => {
    return id.substring(0, 8).toUpperCase()
  }

  const getInstallment = (item) => {
    if (item.status !== 'ACTIVE') return null
    const text = 'common.' + item.repaymentFrequencyType
    return item.totalInstallment ? `${item.nextInstallment}/${item.totalInstallment} ${t(text)}` : 0
  }

  const getNextInstallment = (item) => {
    if (item.status !== 'ACTIVE') return null
    return moment(item.repaymentDueDate).format('DD-MM-yyyy')
  }

  const getDayInArrearsColor = (item) => {
    if (item.status !== 'ACTIVE') return ''
    return colorHelpers.getDayInArrearsColor(item)
  }

  const getDayInArrears = (item) => {
    if (item.status !== 'ACTIVE') return ''
    const today = moment()
    const dueDate = moment(item.repaymentDueDate)
    const duration = dueDate.diff(today, 'days')
    let dayInArrearText = t('common.Today')
    if (duration > 0) {
      dayInArrearText = `${t('common.Still')} ${Math.abs(duration)} ${t('common.Days')}`
    } else if (duration < 0) {
      dayInArrearText = `${t('common.Late')} ${Math.abs(duration)} ${t('common.Days')}`
    }
    return dayInArrearText
  }

  const getPledgeIcons = (item) => {
    return item.pledge ? (
      <FaCheck size="1.2em" color="#1e9ff2" />
    ) : (
      <FaTimes size="1.2em" color="#e55353" />
    )
  }

  const handleLoanNoteChange = (e) => {
    const { name, value } = e.target
    setSelectedNote((selectedNote) => ({ ...selectedNote, [name]: value }))
  }

  const handleToAddNote = () => {
    const payload = {
      createdById: account.id,
      loanId: selectedLoan.id,
      description: selectedNote.description,
    }
    dispatch(loanActions.createLoanNote(payload))
    setIsAddNote(false)
  }

  const renderAddNoteModal = () => {
    return (
      <CModal
        visible={isAddNote}
        onClose={() => {
          setIsAddNote(false)
        }}
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.Notes')}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xl={12} className="mb-2">
              <b>{t('common.Description')}</b>
            </CCol>
            <CCol xl={12}>
              <CFormTextarea
                id="description"
                name="description"
                rows="3"
                value={selectedNote.description}
                onChange={handleLoanNoteChange}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleToAddNote}>
            {t('common.Save')}
          </CButton>
          <CButton
            color="secondary"
            onClick={() => {
              setIsAddNote(false)
            }}
          >
            {t('common.Cancel')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const renderDeleteModal = () => {
    return (
      <CModal
        visible={danger}
        onClose={() => {
          setDanger(false)
          setSelectedLoan(null)
        }}
        color="danger"
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.ConfirmDelete')}</CModalTitle>
        </CModalHeader>
        <CModalBody>{t('messages.messageConfirmDelete')}</CModalBody>
        <CModalFooter>
          <CButton color="danger" onClick={handleDeleteLoan}>
            {t('common.Delete')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setDanger(false)
              setSelectedLoan(null)
            }}
          >
            {t('common.Cancel')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  useEffect(() => {
    const loadData = async () => {
      await getAllLoans()
    }
    loadData()
  }, [dispatch, currentPage, page])

  const getLoanTitle = () => {
    switch (status.toLowerCase()) {
      case 'active':
        return t('theHeader.ActiveLoans')
      case 'approved':
        return t('theHeader.ApprovedLoans')
      case 'pending':
        return t('theHeader.PendingLoans')
      case 'submitted':
        return t('theHeader.SubmittedLoans')
      case 'completed':
        return t('theHeader.CompletedLoans')
      case 'overdue':
        return t('theHeader.OverDueLoans')
      case 'rejected':
        return t('theHeader.RejectedLoans')
      default:
        return t('theHeader.AllLoans')
    }
  }
  return (
    <CRow>
      <CCol>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm="5">
                <h4 id="traffic" className="card-title mb-0">
                  {getLoanTitle()}
                </h4>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody className="table-responsive">
            <CSmartTable
              loading={isFetching}
              items={loans}
              columns={[
                {
                  key: 'id',
                  _classes: 'font-weight-bold',
                  label: t('view.Loan.LoanId'),
                },
                {
                  key: 'borrowerName',
                  _classes: 'font-weight-bold',
                  label: t('view.Loan.CustomerName'),
                },
                { key: 'amountPrincipal', label: t('view.Loan.Amount') },
                { key: 'repaymentDueAmount', label: t('view.Loan.InstallmentAmount') },
                { key: 'installment', label: t('view.Loan.Installment') },
                { key: 'repaymentFromDate', label: t('view.Loan.NextInstallment') },
                { key: 'pledge', label: t('common.Pledge') },
                { key: 'notes', label: t('common.Notes') },
                { key: 'status', label: t('view.Loan.Status') },
                { key: 'action', label: t('common.Action') },
              ]}
              tableFilter
              itemsPerPageSelect
              itemsPerPage={itemsPerPage}
              activePage={currentPage - 1}
              pagination
              tableProps={{
                striped: true,
                hover: true,
              }}
              scopedColumns={{
                id: (item) => (
                  <td>
                    <CLink
                      className="text-decoration-none font-weight-bold"
                      href={`#/loans/${item.id}`}
                    >
                      {getShortLoanId(item.id)}
                    </CLink>
                  </td>
                ),
                borrowerName: (item) => (
                  <td>
                    <CRow className="mb-2">
                      <CLink
                        className="text-decoration-none"
                        href={`#/borrowers/${item.borrowerId}`}
                      >
                        {item.borrowerName}
                      </CLink>
                    </CRow>
                    <CRow>
                      <span>
                        <FaPhoneAlt color="#1e9ff2" />{' '}
                        <CLink
                          style={{ color: '#1e9ff2' }}
                          className="text-decoration-none font-weight-bold"
                          href={`tel:${item.borrowerMobilePhone}`}
                        >
                          {item.borrowerMobilePhone}
                        </CLink>
                      </span>
                    </CRow>
                  </td>
                ),
                amountPrincipal: (item) => (
                  <td className="text-right">
                    {loanHelpers.currencyFormatter(item.amountPrincipal)}
                  </td>
                ),
                repaymentDueAmount: (item) => (
                  <td className="text-right">
                    {item.status === 'ACTIVE'
                      ? loanHelpers.currencyFormatter(item.repaymentDueAmount)
                      : ''}
                  </td>
                ),
                installment: (item) => (
                  <td>
                    <CRow>
                      <span>{getInstallment(item)}</span>
                    </CRow>
                  </td>
                ),
                repaymentFromDate: (item) => (
                  <td>
                    <CRow className="mb-2">
                      <span>{getNextInstallment(item)}</span>
                    </CRow>
                    <CRow>
                      <span style={{ color: getDayInArrearsColor(item) }}>
                        <b>{getDayInArrears(item)}</b>
                      </span>
                    </CRow>
                  </td>
                ),
                pledge: (item) => (
                  <td>
                    <FaMotorcycle size="1.2em" className="me-2" />
                    {getPledgeIcons(item)}
                  </td>
                ),
                status: (item) => (
                  <td>
                    <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                  </td>
                ),
                action: (item) => {
                  return (
                    <td>
                      <CButton
                        color="primary"
                        variant="outline"
                        shape="square"
                        size="sm"
                        onClick={() => {
                          toggleDetails(item.id)
                        }}
                      >
                        {details.includes(item.id) ? 'Hide' : 'Show'}
                      </CButton>
                    </td>
                  )
                },
                details: (item) => {
                  return (
                    <CCollapse visible={details.includes(item.id)}>
                      <CCardBody>
                        <CButton
                          size="sm"
                          color="info"
                          className="me-4"
                          onClick={() => handleToView(item)}
                        >
                          {t('common.View')}
                        </CButton>
                        {item.status.toLowerCase() === 'pending' ||
                        item.status.toLowerCase() === 'approved' ? (
                          <CButton
                            size="sm"
                            color="info"
                            className="me-4"
                            onClick={() => handleToEdit(item)}
                          >
                            {t('common.Edit')}
                          </CButton>
                        ) : null}
                        {item.status.toLowerCase() === 'pending' ||
                        item.status.toLowerCase() === 'approved' ? (
                          <CButton
                            size="sm"
                            color="danger"
                            className="me-4"
                            onClick={() => {
                              setDanger(true)
                              setSelectedLoan(item)
                            }}
                          >
                            {t('common.Delete')}
                          </CButton>
                        ) : null}
                        <CButton
                          size="sm"
                          color="info"
                          className="me-4"
                          onClick={() => {
                            setSelectedLoan(item)
                            setIsAddNote(true)
                          }}
                        >
                          {t('common.AddNote')}
                        </CButton>
                      </CCardBody>
                    </CCollapse>
                  )
                },
              }}
            />
            <CSmartPagination
              activePage={page}
              onActivePageChange={pageChange}
              pages={totalPages}
              doubleArrows={false}
              align="center"
            />
          </CCardBody>
        </CCard>
      </CCol>
      {renderDeleteModal()}
      {renderAddNoteModal()}
    </CRow>
  )
}

export default Loans

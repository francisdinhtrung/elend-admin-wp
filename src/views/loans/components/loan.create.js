import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CFormInput,
  CFormFeedback,
  CFormSelect,
  CBadge,
  CFormLabel,
  CCardTitle,
  CInputGroup,
  CButtonGroup,
  CModalHeader,
  CModalBody,
  CModal,
  CForm,
  CMultiSelect,
  CDatePicker,
} from '@coreui/react-pro'
import CurrencyInput from 'react-currency-input-field'
import moment from 'moment'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import CIcon from '@coreui/icons-react'
import { cilPlus, cilMinus } from '@coreui/icons'
import { accountHelpers } from '../../../utils/account-helper'
import { organizationActions } from '../../organization/actions'
import { loanActions } from '../actions'
import { borrowerActions } from '../../borrowers/actions'
import { toast } from 'react-toastify'
import { colorHelpers } from '../../../utils/color-helper'
import api from '../../../utils/api'
import { loanHelpers } from '../../../utils/loan-helper'

const LoanCreate = ({ match }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const borrowers = useSelector((state) => state.borrowers.borrowers)
  const organizations = useSelector((state) => state.organizations.organizations)
  const loanData = useSelector((state) => state.loans.loan)
  const loanStatus = useSelector((state) => state.loans.loanStatus)
  const [errors, setErrors] = useState({})
  const [pledgeErrors, setPledgeErrors] = useState([])
  // const [fileUpload, setFileUpload] = useState([])
  const [pledges, setPledges] = useState([])
  const [isPreviewImage, setIsPreviewImage] = useState(false)
  const [previewImage, setPreviewImage] = useState(null)
  const myOrg = accountHelpers.getMyOrganization()
  const account = accountHelpers.getAccount()
  const placeholderImage = `${process.env.PUBLIC_URL}/images/placeholder-image.png`
  const uploadFiles = {}
  const optionRepaymentSchedules = ['days', 'weeks', 'months']
  let paramId = match.params.id
  const idEditMode = paramId && loanData && loanData.loansDTO
  const initialPledge = {
    id: 0,
    createdBy: null,
    loanId: 0,
    pledgeNumber: '',
    pledgeType: '',
    price: 0,
    name: '',
    description: '',
    firstPledgeImage: '',
    secondPledgeImage: '',
    thirdPledgeImage: '',
    frontIdentityPledgeImage: '',
    backIdentityPledgeImage: '',
    firstPledgeImageFile: '',
    secondPledgeImageFile: '',
    thirdPledgeImageFile: '',
    frontIdentityPledgeImageFile: '',
    backIdentityPledgeImageFile: '',
  }

  const loanStatusEnum = {
    LOAN_CREATED: 'LOAN_CREATED',
    LOAN_UPDATED: 'LOAN_UPDATED',
  }

  const uploadFileEnums = {
    ATTACHED_CONTRACT_FILE: 'ATTACHED_CONTRACT_FILE',
    FIRST_PLEDGE_IMAGE: 'FIRST_PLEDGE_IMAGE',
    SECOND_PLEDGE_IMAGE: 'SECOND_PLEDGE_IMAGE',
    THIRD_PLEDGE_IMAGE: 'THIRD_PLEDGE_IMAGE',
    FRONT_IDENTITY_PLEDGE_IMAGE: 'FRONT_IDENTITY_PLEDGE_IMAGE',
    BACK_IDENTITY_PLEDGE_IMAGE: 'BACK_IDENTITY_PLEDGE_IMAGE',
  }

  const getBadge = (status) => {
    if (!status) return
    return colorHelpers.getColorByStatus(status.toLowerCase(), false)
  }

  const schema = Yup.object({
    repaymentCycle: Yup.string()
      .required(t('messages.validations.requiredField'))
      .oneOf(optionRepaymentSchedules),
    selectedOrganizationId: Yup.string().required(t('messages.validations.requiredField')),
    selectedBorrowerId: Yup.string().required(t('messages.validations.requiredField')),
    amountOfPrincipal: Yup.string().required(t('messages.validations.requiredField')),
    durationOfLoan: Yup.number().required(t('messages.validations.requiredField')),
    repaymentFrequency: Yup.number().required(t('messages.validations.requiredField')),
    expectedDisbursementDate: Yup.string().required(t('messages.validations.requiredField')),
    interestOfLoan: Yup.number().required(t('messages.validations.requiredField')),
  })
  const formik = useFormik({
    initialValues: {
      selectedOrganizationId: idEditMode ? loanData.loansDTO.organizationId : '',
      selectedBorrowerId: idEditMode ? loanData.loansDTO.borrowerId : '',
      amountOfPrincipal: idEditMode ? loanData.loansDTO.amountPrincipal : '',
      durationOfLoan: idEditMode ? loanData.loansDTO.loanTerm : '',
      durationLoanType: idEditMode ? loanData.loansDTO.loanTermType : 'months',
      repaymentFrequency: idEditMode ? loanData.loansDTO.repaymentFrequency : '',
      repaymentCycle: idEditMode ? loanData.loansDTO.repaymentFrequencyType : '',
      repaymentFrequencyType: idEditMode ? loanData.loansDTO.repaymentFrequencyType : '',
      expectedDisbursementDate: idEditMode
        ? moment(loanData.loansDTO.expectedDisbursementDate).format('yyyy-MM-DD')
        : '',
      expectedFirstPaymentDate: idEditMode
        ? moment(loanData.loansDTO.expectedFirstPaymentDate).format('yyyy-MM-DD')
        : '',
      interestOfLoan: idEditMode ? loanData.loansDTO.interestRate : 4,
      interestOfLoanType: idEditMode ? loanData.loansDTO.interestRateType : 'monthly',
      attachContractOfLoanUrl: idEditMode ? loanData.loansDTO.attachContractLoan : '',
    },
    enableReinitialize: true,
    validationSchema: schema,
  })

  const isValidation = async () => {
    const validateForms = await formik.validateForm(formik.values)
    console.log('isValidation validateForms=', validateForms)
    setErrors(validateForms)
    formik.setErrors(validateForms)
    formik.setTouched(validateForms)
    return Object.keys(validateForms).length === 0
  }

  const validatePlegdes = () => {
    if (!pledges || pledges.length === 0) {
      return true
    }
    const errorMessages = []
    pledges.forEach((p, index) => {
      errorMessages[index] = {}
      if (!p.name || p.name.length === 0) {
        errorMessages[index].name = t('messages.validations.requiredField')
      }
      if (!p.pledgeNumber) {
        errorMessages[index].pledgeNumber = t('messages.validations.requiredField')
      }
      if (!p.pledgeType) {
        errorMessages[index].pledgeType = t('messages.validations.requiredField')
      }
      if (!p.price) {
        errorMessages[index].price = t('messages.validations.requiredField')
      }
      if (!p.frontIdentityPledgeImage) {
        errorMessages[index].frontIdentityPledgeImage = t('messages.validations.requiredField')
      }
      if (!p.backIdentityPledgeImage) {
        errorMessages[index].backIdentityPledgeImage = t('messages.validations.requiredField')
      }
      setPledgeErrors(errorMessages)
    })
    return errorMessages.length === 0
  }

  const handleToSubmitLoan = async () => {
    const isValid = await isValidation()
    console.log('handleToSubmitLoan isValid 1=', isValid)
    if (!isValid) {
      console.log('handleToSubmitLoan isValid 2=', isValid)
      return
    }
    if (!validatePlegdes()) {
      console.log('handleToSubmitLoan validatePlegdes false=')
      return
    }
    if (loanData && loanData.loansDTO.status.toLowerCase() === 'completed') {
      toast.warn('Sorry, loan cannot update because it already completed!')
      return
    }
    if (
      loanData &&
      loanData.loansDTO &&
      (loanData.loansDTO.status.toLowerCase() === 'banned' ||
        loanData.loansDTO.status.toLowerCase() === 'rejected')
    ) {
      toast.warn('Sorry, loan cannot update because it already banned!')
      return
    }
    const data = formik.values
    const payload = {
      organizationId: data.selectedOrganizationId,
      borrowerId: data.selectedBorrowerId,
      loanOfficerId: account.id,
      submittedByUserId: account.id,
      expectedDisbursementDate: loanHelpers.dateFormatter(data.expectedDisbursementDate),
      expectedFirstPaymentDate: loanHelpers.dateFormatter(data.expectedDisbursementDate),
      amountPrincipal: data.amountOfPrincipal,
      interestRate: data.interestOfLoan,
      loanTerm: data.durationOfLoan,
      loanTermType: data.durationLoanType,
      repaymentFrequency: data.repaymentFrequency,
      repaymentFrequencyType: data.repaymentCycle,
      repaymentCycle: data.repaymentCycle,
      interestRateType: data.interestOfLoanType,
      interestMethodology: 'flat',
      attachContractLoan: data.attachContractOfLoanUrl,
      lastModifiedBy: account.id,
    }
    if (paramId && data) {
      payload.id = loanData.loansDTO.id
      if (pledges && pledges.length > 0) {
        pledges.forEach((p) => {
          p.loanId = loanData.loansDTO.id
          p.lastModifiedBy = account.id
        })
        payload.loanPledges = pledges
      }

      dispatch(loanActions.updateLoan(payload))
    } else {
      payload.createdBy = account.id
      if (pledges && pledges.length > 0) {
        pledges.forEach((p) => {
          p.createdBy = account.id
          p.lastModifiedBy = account.id
        })
        payload.loanPledges = pledges
      }
      dispatch(loanActions.createLoan(payload))
    }
  }

  const getAllUsers = async () => {
    dispatch(
      borrowerActions.getAllBorrowers({
        all: true,
        status: 'ACTIVE',
      }),
    )
  }

  const getAllOrganizations = async () => {
    dispatch(
      organizationActions.getOrganizationList({
        all: true,
      }),
    )
  }

  // const onUploadFileChange = (e, type, index = 0) => {
  //   console.log('onUploadFileChange=', e)
  // }

  useEffect(() => {
    const loadData = async () => {
      dispatch(loanActions.resetLoan())
      if (!myOrg || !myOrg.organizationId) {
        await getAllOrganizations()
      }
      await getAllUsers()
      if (paramId) {
        dispatch(loanActions.getLoanById(paramId))
      }
    }

    if (loanStatus && loanStatus.length > 0 && loanData) {
      const loanId = loanData.loansDTO ? loanData.loansDTO.id : loanData.id
      dispatch(loanActions.resetLoan())
      let url =
        loanStatus === loanStatusEnum.LOAN_CREATED ? `/loans/${loanId}` : `/loans/${loanId}/edit`
      if (loanData.loansDTO && loanData.loansDTO.status.toUpperCase() === 'ACTIVE') {
        url = `/loans/${loanId}`
      }
      history.push(url)
      return
    }

    loadData()
  }, [dispatch, loanStatus])

  const renderOrganizations = () => {
    return !myOrg || !myOrg.organizationId ? (
      <CRow className="mb-3">
        <CCol xl={3}>
          {t('common.Organization')} <span className="form-required"> *</span>
        </CCol>
        <CCol>
          <CFormSelect
            invalid={
              formik.errors.selectedOrganizationId &&
              formik.errors.selectedOrganizationId.length > 0 &&
              formik.touched.selectedOrganizationId &&
              formik.touched.selectedOrganizationId.length > 0
            }
            {...formik.getFieldProps('selectedOrganizationId')}
          >
            <option>{t('view.Organizations.SelectOrganizations')}</option>
            {organizations.map((item, index) => {
              return (
                <option key={index} value={item.id}>
                  {item.name}
                </option>
              )
            })}
          </CFormSelect>
          <CFormFeedback
            invalid
            style={{
              display:
                formik.errors.selectedOrganizationId && formik.touched.selectedOrganizationId
                  ? 'block'
                  : 'none',
            }}
          >
            {errors.selectedOrganizationId}
          </CFormFeedback>
        </CCol>
      </CRow>
    ) : null
  }

  const getCustomerName = (data) => {
    return data ? data.fullName : ''
  }

  const onMultiSelectChange = (selected) => {
    const item = selected[0]
    if (!idEditMode && item) {
      if (!formik.values.selectedBorrowerId || formik.values.selectedBorrowerId.length === 0) {
        formik.setFieldValue('selectedBorrowerId', item.value)
      } else if (
        formik.values.selectedBorrowerId &&
        formik.values.selectedBorrowerId.length > 0 &&
        formik.values.selectedBorrowerId !== item.value
      ) {
        formik.setFieldValue('selectedBorrowerId', item.value)
      }
    }
  }

  const renderBorrowerOptions = () => {
    if (!borrowers || borrowers.length === 0) return null
    const options = borrowers.map((person) => ({
      value: person.id,
      text: getCustomerName(person),
      selected: person.id === formik.values.selectedBorrowerId,
      disabled: idEditMode || false,
    }))
    return <CMultiSelect options={options} multiple={false} onChange={onMultiSelectChange} />
  }

  const renderBorrowers = () => {
    return (
      <CRow className="mb-3">
        <CCol xl={3}>
          {t('common.CustomerName')} <span className="form-required"> *</span>
        </CCol>
        <CCol>
          {renderBorrowerOptions()}
          <CFormFeedback
            invalid
            style={{
              display: errors.selectedBorrowerId ? 'block' : 'none',
            }}
          >
            {errors.selectedBorrowerId}
          </CFormFeedback>
        </CCol>
      </CRow>
    )
  }

  const getHeader = () => {
    return paramId ? t('theHeader.EditNewLoan') : t('theHeader.CreateNewLoan')
  }

  const renderStatus = () => {
    return paramId && loanData ? (
      <CRow className="mb-3">
        <CCol xl={3}>{t('common.Status')}</CCol>
        <CCol>
          <CBadge color={getBadge(loanData.loansDTO.status)}>{loanData.loansDTO.status}</CBadge>
        </CCol>
      </CRow>
    ) : null
  }

  const addPledge = () => {
    setPledges([...pledges, initialPledge])
  }

  const removePledge = (index) => {
    let newArray = [...pledges]
    if (newArray[index].id) {
      // TODO api to remove plegde
    }
    newArray.splice(index, 1)
    setPledges(newArray)

    const newPledgeErrors = [...pledgeErrors]
    newPledgeErrors.splice(index, 1)
    setPledgeErrors(newPledgeErrors)
  }

  const inputPledgeChangedHandler = (event, index, inputType) => {
    let newArray = [...pledges]
    switch (inputType) {
      case 'name':
        newArray[index].name = event.target.value
        break
      case 'pledgeNumber':
        newArray[index].pledgeNumber = event.target.value
        break
      case 'pledgeType':
        newArray[index].pledgeType = event.target.value
        break
      case 'price':
        newArray[index].price = event.target.value
        break
      case 'description':
        newArray[index].description = event.target.value
        break
    }
    setPledges(newArray)
    validatePlegdes()
  }

  const inputPledgeCostChanged = (index, value) => {
    let newArray = [...pledges]
    newArray[index].price = value
    setPledges(newArray)
    if (newArray && newArray.length > 0) {
      validatePlegdes()
    }
  }

  const onloadedFileReader = (e, type, index, reader) => {
    const newArrayLazy = [...pledges]
    switch (type) {
      case uploadFileEnums.FIRST_PLEDGE_IMAGE:
        newArrayLazy[index].firstPledgeImage = reader.result
        break
      case uploadFileEnums.SECOND_PLEDGE_IMAGE:
        newArrayLazy[index].secondPledgeImage = reader.result
        break
      case uploadFileEnums.THIRD_PLEDGE_IMAGE:
        newArrayLazy[index].thirdPledgeImage = reader.result
        break
      case uploadFileEnums.FRONT_IDENTITY_PLEDGE_IMAGE:
        newArrayLazy[index].frontIdentityPledgeImage = reader.result
        break
      case uploadFileEnums.BACK_IDENTITY_PLEDGE_IMAGE:
        newArrayLazy[index].backIdentityPledgeImage = reader.result
        break
    }
    // setPledges(newArrayLazy)
    handleToUploadAttachedFiles(type, index)
  }

  const onUploadPledgeFilesChange = (e, type, index = 0) => {
    let newArray = [...pledges]
    const file = e.target.files[0]
    switch (type) {
      case uploadFileEnums.FIRST_PLEDGE_IMAGE:
        newArray[index].firstPledgeImageFile = file
        break
      case uploadFileEnums.SECOND_PLEDGE_IMAGE:
        newArray[index].secondPledgeImageFile = file
        break
      case uploadFileEnums.THIRD_PLEDGE_IMAGE:
        newArray[index].thirdPledgeImageFile = file
        break
      case uploadFileEnums.FRONT_IDENTITY_PLEDGE_IMAGE:
        newArray[index].frontIdentityPledgeImageFile = file
        break
      case uploadFileEnums.BACK_IDENTITY_PLEDGE_IMAGE:
        newArray[index].backIdentityPledgeImageFile = file
        break
    }
    setPledges(newArray)
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = (e) => onloadedFileReader(e, type, index, reader)
  }

  const handleToUploadAttachedFiles = (type, index) => {
    let file
    switch (type) {
      case uploadFileEnums.FIRST_PLEDGE_IMAGE:
        file = pledges[index].firstPledgeImageFile
        break
      case uploadFileEnums.SECOND_PLEDGE_IMAGE:
        file = pledges[index].secondPledgeImageFile
        break
      case uploadFileEnums.THIRD_PLEDGE_IMAGE:
        file = pledges[index].thirdPledgeImageFile
        break
      case uploadFileEnums.FRONT_IDENTITY_PLEDGE_IMAGE:
        file = pledges[index].frontIdentityPledgeImageFile
        break
      case uploadFileEnums.BACK_IDENTITY_PLEDGE_IMAGE:
        file = pledges[index].backIdentityPledgeImageFile
        break
    }
    if (!file) {
      toast.warn('File cannot be empty!')
      return
    }
    api.cheveretoService
      .uploadImage(file)
      .then((response) => {
        return response.json()
      })
      .then((result) => {
        if (result.status_code === 200) {
          toast.success(`Upload the image successfully.`)
          let newArray = [...pledges]
          switch (type) {
            case uploadFileEnums.FIRST_PLEDGE_IMAGE:
              newArray[index].firstPledgeImage = result.image.url
              break
            case uploadFileEnums.SECOND_PLEDGE_IMAGE:
              newArray[index].secondPledgeImage = result.image.url
              break
            case uploadFileEnums.THIRD_PLEDGE_IMAGE:
              newArray[index].thirdPledgeImage = result.image.url
              break
            case uploadFileEnums.FRONT_IDENTITY_PLEDGE_IMAGE:
              newArray[index].frontIdentityPledgeImage = result.image.url
              break
            case uploadFileEnums.BACK_IDENTITY_PLEDGE_IMAGE:
              newArray[index].backIdentityPledgeImage = result.image.url
              break
          }
          setPledges(newArray)
        } else {
          toast.error(result.error.message)
        }
      })
      .catch((error) => {
        console.error('error=', error)
      })
  }

  const renderPledgeFeedback = (index, name) => {
    return (
      <CFormFeedback
        invalid
        style={{
          display:
            pledgeErrors && pledgeErrors.length > 0 && pledgeErrors[index][name] ? 'block' : 'none',
        }}
      >
        {pledgeErrors && pledgeErrors.length > 0 && pledgeErrors[index][name]
          ? pledgeErrors[index][name]
          : ''}
      </CFormFeedback>
    )
  }

  const renderPledges = (item, index) => {
    return (
      <CCard className="mb-3" key={index}>
        <CCardBody>
          <CCardTitle className="card-pledge-title">
            <span>
              {t('common.Pledge')} {index + 1}
            </span>
            <div className="pledge-add-circle minus ms-3" onClick={() => removePledge(index)}>
              <CIcon icon={cilMinus} size="lg" />
            </div>
          </CCardTitle>
          <CRow className="mb-3">
            <CCol xl={3}>
              {t('view.Pledges.Name')} <span className="form-required"> *</span>
            </CCol>
            <CCol>
              <CFormInput
                id="pledgeName"
                name="pledgeName"
                defaultValue={item.name}
                onKeyUp={(event) => inputPledgeChangedHandler(event, index, 'name')}
              />
              {renderPledgeFeedback(index, 'name')}
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xl={3}>
              {t('view.Pledges.Number')} <span className="form-required"> *</span>
            </CCol>
            <CCol>
              <CFormInput
                id="pledgeNumber"
                name="pledgeNumber"
                defaultValue={item.pledgeNumber}
                onKeyUp={(event) => inputPledgeChangedHandler(event, index, 'pledgeNumber')}
              />
              {renderPledgeFeedback(index, 'pledgeNumber')}
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xl={3}>
              {t('view.Pledges.Type')} <span className="form-required"> *</span>
            </CCol>
            <CCol>
              <CFormInput
                id="pledgeType"
                name="pledgeType"
                defaultValue={item.pledgeType}
                onKeyUp={(event) => inputPledgeChangedHandler(event, index, 'pledgeType')}
              />
              {renderPledgeFeedback(index, 'pledgeType')}
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xl={3}>
              {t('view.Pledges.Cost')} <span className="form-required"> *</span>
            </CCol>
            <CCol>
              <CurrencyInput
                intlConfig={{ locale: 'vi-VN', currency: 'VND' }}
                id="pledgePrice"
                name="pledgePrice"
                className="currency-input"
                defaultValue={item.price}
                decimalsLimit={0}
                onValueChange={(value, name) => inputPledgeCostChanged(index, value)}
              />
              {renderPledgeFeedback(index, 'price')}
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xl={3}>{t('view.Pledges.Description')}</CCol>
            <CCol>
              <CFormInput
                id="pledgeDescription"
                name="pledgeDescription"
                defaultValue={item.description}
                onKeyUp={(event) => inputPledgeChangedHandler(event, index, 'description')}
              />
            </CCol>
          </CRow>
          <CRow className="mb-4">
            <CCol xl={3}>
              {t('view.Pledges.FrontIdentityImage')} <span className="form-required"> *</span>
            </CCol>
            <CCol>
              <div
                className="box-identification zoom-in mb-4"
                onClick={() => {
                  setPreviewImage(item.frontIdentityPledgeImage || placeholderImage)
                  setIsPreviewImage(true)
                }}
              >
                <img src={item.frontIdentityPledgeImage || placeholderImage} height="80px" />
              </div>
              <CInputGroup>
                <CButtonGroup role="group" className="group-button-identification">
                  <CFormInput
                    type="file"
                    onChange={(e) =>
                      onUploadPledgeFilesChange(
                        e,
                        uploadFileEnums.FRONT_IDENTITY_PLEDGE_IMAGE,
                        index,
                      )
                    }
                  ></CFormInput>
                </CButtonGroup>
              </CInputGroup>
              {renderPledgeFeedback(index, 'frontIdentityPledgeImage')}
            </CCol>
          </CRow>
          <CRow className="mb-4">
            <CCol xl={3}>
              {t('view.Pledges.BackIdentityImage')} <span className="form-required"> *</span>
            </CCol>
            <CCol>
              <div
                className="box-identification zoom-in mb-4"
                onClick={() => {
                  setPreviewImage(item.backIdentityPledgeImage || placeholderImage)
                  setIsPreviewImage(true)
                }}
              >
                <img src={item.backIdentityPledgeImage || placeholderImage} height="80px" />
              </div>
              <CInputGroup>
                <CButtonGroup role="group" className="group-button-identification">
                  <CFormInput
                    type="file"
                    onChange={(e) =>
                      onUploadPledgeFilesChange(
                        e,
                        uploadFileEnums.BACK_IDENTITY_PLEDGE_IMAGE,
                        index,
                      )
                    }
                  ></CFormInput>
                </CButtonGroup>
              </CInputGroup>
              {renderPledgeFeedback(index, 'backIdentityPledgeImage')}
            </CCol>
          </CRow>
          <CRow className="mb-4">
            <CCol xl={3}>{t('view.Pledges.AttachedImage1')}</CCol>
            <CCol>
              <div
                className="box-identification zoom-in mb-4"
                onClick={() => {
                  setPreviewImage(item.firstPledgeImage || placeholderImage)
                  setIsPreviewImage(true)
                }}
              >
                <img src={item.firstPledgeImage || placeholderImage} height="80px" />
              </div>
              <CInputGroup>
                <CButtonGroup role="group" className="group-button-identification">
                  <CFormInput
                    type="file"
                    onChange={(e) =>
                      onUploadPledgeFilesChange(e, uploadFileEnums.FIRST_PLEDGE_IMAGE, index)
                    }
                  ></CFormInput>
                </CButtonGroup>
              </CInputGroup>
            </CCol>
          </CRow>
          <CRow className="mb-4">
            <CCol xl={3}>{t('view.Pledges.AttachedImage2')}</CCol>
            <CCol>
              <div
                className="box-identification zoom-in  mb-4"
                onClick={() => {
                  setPreviewImage(item.secondPledgeImage || placeholderImage)
                  setIsPreviewImage(true)
                }}
              >
                <img src={item.secondPledgeImage || placeholderImage} height="80px" />
              </div>
              <CInputGroup>
                <CButtonGroup role="group" className="group-button-identification">
                  <CFormInput
                    type="file"
                    onChange={(e) =>
                      onUploadPledgeFilesChange(e, uploadFileEnums.SECOND_PLEDGE_IMAGE, index)
                    }
                  ></CFormInput>
                </CButtonGroup>
              </CInputGroup>
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol xl={3}>{t('view.Pledges.AttachedImage3')}</CCol>
            <CCol>
              <div
                className="box-identification zoom-in mb-4"
                onClick={() => {
                  setPreviewImage(item.thirdPledgeImage || placeholderImage)
                  setIsPreviewImage(true)
                }}
              >
                <img src={item.thirdPledgeImage || placeholderImage} height="80px" />
              </div>
              <CInputGroup>
                <CButtonGroup role="group" className="group-button-identification">
                  <CFormInput
                    type="file"
                    onChange={(e) =>
                      onUploadPledgeFilesChange(e, uploadFileEnums.THIRD_PLEDGE_IMAGE, index)
                    }
                  ></CFormInput>
                </CButtonGroup>
              </CInputGroup>
            </CCol>
          </CRow>
        </CCardBody>
      </CCard>
    )
  }

  const previewImageModal = () => {
    return (
      <CModal
        alignment="center"
        visible={isPreviewImage}
        onClose={() => {
          setIsPreviewImage(false)
          setPreviewImage(null)
        }}
        size="lg"
      >
        <CModalHeader>Preview</CModalHeader>
        <CModalBody>
          <img src={previewImage} />
        </CModalBody>
      </CModal>
    )
  }

  const onFormValidation = async () => {
    await isValidation()
  }

  return (
    <CRow>
      <CCol>
        <CForm autoComplete="off">
          <CCard>
            <CCardHeader>
              <CRow>
                <CCol sm="5">
                  <h4 id="traffic" className="card-title mb-0">
                    {getHeader()}
                  </h4>
                </CCol>
              </CRow>
            </CCardHeader>
            <CCardBody>
              {renderOrganizations()}
              {renderBorrowers()}
              {renderStatus()}
              <CRow className="mb-3">
                <CCol xl={3}>
                  {t('view.Loan.AmountOfPrincipal')} <span className="form-required"> *</span>
                </CCol>
                <CCol>
                  <CurrencyInput
                    intlConfig={{ locale: 'vi-VN', currency: 'VND' }}
                    id="amountOfPrincipal"
                    name="amountOfPrincipal"
                    className="currency-input"
                    value={formik.values.amountOfPrincipal}
                    decimalsLimit={0}
                    onValueChange={async (value, name) => {
                      await formik.validateForm()
                      formik.setFieldValue('amountOfPrincipal', value)
                    }}
                    {...formik.getFieldProps('amountOfPrincipal')}
                  />
                  <CFormFeedback
                    invalid
                    style={{
                      display: errors.amountOfPrincipal ? 'block' : 'none',
                    }}
                  >
                    {errors.amountOfPrincipal}
                  </CFormFeedback>
                </CCol>
              </CRow>
              <CRow className="mb-1">
                <CCol xl={3}>
                  {t('view.Loan.DurationOfLoan')} <span className="form-required"> *</span>
                </CCol>
                <CCol>
                  <CFormInput
                    id="durationOfLoan"
                    name="durationOfLoan"
                    invalid={
                      formik.errors.durationOfLoan &&
                      formik.errors.durationOfLoan.length > 0 &&
                      formik.touched.durationOfLoan &&
                      formik.touched.durationOfLoan.length > 0
                    }
                    value={formik.values.durationOfLoan}
                    autoComplete="none"
                    {...formik.getFieldProps('durationOfLoan')}
                  />
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.durationOfLoan && formik.touched.durationOfLoan
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {errors.durationOfLoan}
                  </CFormFeedback>
                </CCol>
                <CCol className="mb-3">
                  <CFormSelect
                    custom="true"
                    name="durationLoanType"
                    id="durationLoanType"
                    {...formik.getFieldProps('durationLoanType')}
                  >
                    <option value="months">{t('common.Month(s)')}</option>
                  </CFormSelect>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol xl={3}>
                  {t('view.Loan.RepaymentCycle')} <span className="form-required"> *</span>
                </CCol>
                <CCol>
                  <CFormInput
                    id="repaymentFrequency"
                    name="repaymentFrequency"
                    invalid={
                      formik.errors.repaymentFrequency &&
                      formik.errors.repaymentFrequency.length > 0 &&
                      formik.touched.repaymentFrequency &&
                      formik.touched.repaymentFrequency.length > 0
                    }
                    value={formik.values.repaymentFrequency}
                    autoComplete="none"
                    {...formik.getFieldProps('repaymentFrequency')}
                  />
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.repaymentCycle && formik.touched.repaymentCycle
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {errors.repaymentCycle}
                  </CFormFeedback>
                </CCol>
                <CCol>
                  <CFormSelect
                    custom="true"
                    name="repaymentCycle"
                    id="repaymentCycle"
                    invalid={
                      formik.errors.repaymentCycle &&
                      formik.errors.repaymentCycle.length > 0 &&
                      formik.touched.repaymentCycle &&
                      formik.touched.repaymentCycle.length > 0
                    }
                    {...formik.getFieldProps('repaymentCycle')}
                  >
                    <option value="">{t('common.SelectRepaymentCycle')}</option>
                    <option value="days">{t('common.Daily')}</option>
                    <option value="weeks">{t('common.Weekly')}</option>
                    <option value="months">{t('common.Monthly')}</option>
                  </CFormSelect>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol xl={3}>
                  {t('view.Loan.DateOfDisbursement')} <span className="form-required"> *</span>
                </CCol>
                <CCol>
                  <CDatePicker
                    id="expectedDisbursementDate"
                    name="expectedDisbursementDate"
                    invalid={
                      formik.errors.expectedDisbursementDate &&
                      formik.errors.expectedDisbursementDate.length > 0 &&
                      formik.touched.expectedDisbursementDate &&
                      formik.touched.expectedDisbursementDate.length > 0
                    }
                    date={formik.values.expectedDisbursementDate}
                    onDateChange={(date) => {
                      if (date) {
                        formik.setFieldValue('expectedDisbursementDate', date)
                      }
                    }}
                  />
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.expectedDisbursementDate &&
                        formik.touched.expectedDisbursementDate
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {errors.expectedDisbursementDate}
                  </CFormFeedback>
                </CCol>
              </CRow>
              <CRow className="mb-1">
                <CCol xl={3}>
                  {t('view.Loan.InterestOfLoan')} <span className="form-required"> *</span>
                </CCol>
                <CCol>
                  <CFormInput
                    id="interestOfLoan"
                    name="interestOfLoan"
                    invalid={
                      formik.errors.interestOfLoan &&
                      formik.errors.interestOfLoan.length > 0 &&
                      formik.touched.interestOfLoan &&
                      formik.touched.interestOfLoan.length > 0
                    }
                    value={formik.values.interestOfLoan}
                    autoComplete="none"
                    {...formik.getFieldProps('interestOfLoan')}
                  />
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.interestOfLoan && formik.touched.interestOfLoan
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {errors.interestOfLoan}
                  </CFormFeedback>
                </CCol>
                <CCol className="mb-3">
                  <CFormSelect
                    custom="true"
                    name="interestOfLoanType"
                    id="interestOfLoanType"
                    {...formik.getFieldProps('interestOfLoanType')}
                  >
                    <option value="months">{t('common.Monthly')}</option>
                  </CFormSelect>
                </CCol>
              </CRow>
              <hr />
              <CRow className="mt-4 pe-2 ps-2 pledge">
                <CCol sm={12}>
                  <CRow className="mb-4">
                    <CCol xl={12}>
                      <CFormLabel htmlFor="Pledges" className="col-form-label">
                        {t('common.Pledges')}
                      </CFormLabel>
                      <CButton className="ms-4" onClick={() => addPledge()}>
                        {t('common.Add')}
                      </CButton>
                    </CCol>
                  </CRow>
                  <CRow className="pe-3 ps-3">
                    <CCol sm={11}>
                      {pledges.map((item, index) => {
                        return renderPledges(item, index)
                      })}
                    </CCol>
                    <CCol sm={1} className="pledge-add-icon">
                      {pledges && pledges.length > 0 ? (
                        <div>
                          <div className="pledge-add-circle" onClick={() => addPledge()}>
                            <CIcon icon={cilPlus} size="lg" />
                          </div>
                        </div>
                      ) : null}
                    </CCol>
                  </CRow>
                </CCol>
              </CRow>
              <CRow>
                <CCol xl={12}>
                  <div className="flex-right mt-4">
                    <CButton
                      type="button"
                      color="primary"
                      className="text-center"
                      onClick={handleToSubmitLoan}
                    >
                      {t('common.Save')}
                    </CButton>
                  </div>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CForm>
      </CCol>
      {previewImageModal()}
    </CRow>
  )
}

export default LoanCreate

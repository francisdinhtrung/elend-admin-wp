import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CDropdown,
  CCollapse,
  CSmartTable,
  CSmartPagination,
} from '@coreui/react-pro'
import { userActions } from '../actions'
import { authenticationActions } from '../../../actions'
import { FaUserPlus, FaEllipsisH, FaRegCheckSquare, FaRegSquare } from 'react-icons/fa'
import { colorHelpers } from '../../../utils/color-helper'

const getBadge = (status) => {
  return colorHelpers.getColorByStatus(status, false)
}

const Users = () => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const isFetching = useSelector((state) => state.users.isFetching)
  const usersData = useSelector((state) => state.users.users)
  const itemsPerPage = useSelector((state) => state.users.itemsPerPage)
  const totalPages = useSelector((state) => state.users.totalPages)
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)
  const [details, setDetails] = useState([])

  const pageChange = (newPage) => {
    currentPage !== newPage && history.push(`/users?page=${newPage === 0 ? 1 : newPage}`)
  }

  const onPaginationChange = (numberItemsPerPage) => {
    dispatch(userActions.getAllUsers({ page: 0, size: numberItemsPerPage }))
  }

  const getRoleColor = (status) => {
    switch (status) {
      case 'ROLE_ADMIN':
        return 'success'
      case 'ROLE_USER':
        return 'secondary'
      default:
        return 'primary'
    }
  }

  const handleToEdit = (item, index, e) => {
    if (e === 'action') return
    history.push(`/users/edit/${item.id}`)
  }

  const handleResendVerifyEmail = (userId) => {
    dispatch(
      authenticationActions.resendVerifyEmail(userId, t('messages.resendVerifyEmailSuccess')),
    )
  }

  const handleResetPasswordEmail = (userId) => {
    dispatch(authenticationActions.resetPassword(userId, t('messages.resetPasswordSuccess')))
  }

  useEffect(() => {
    const loadData = async () => {
      await getAllUsers()
    }
    loadData()
  }, [dispatch, currentPage, page])

  const getAllUsers = async () => {
    dispatch(userActions.resetIsCreatedAccount())
    dispatch(
      userActions.getAllUsers({ page: currentPage > 1 ? currentPage - 1 : 0, size: itemsPerPage }),
    )
    setPage(currentPage)
  }

  const navigationToUserCreation = () => {
    history.push(`/users/create`)
  }

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  return (
    <CRow>
      <CCol>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm="5">
                <h4 id="traffic" className="card-title mb-0">
                  {t('theHeader.Employee')}
                </h4>
              </CCol>
              <CCol sm="7" className="d-none d-md-block">
                <CButton color="primary" className="float-end" onClick={navigationToUserCreation}>
                  <FaUserPlus />
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody className="table-responsive">
            <CSmartTable
              loading={isFetching}
              items={usersData}
              columns={[
                {
                  key: 'name',
                  _classes: 'font-weight-bold',
                  label: t('view.Users.fields.PersonInfo'),
                },
                { key: 'email', label: t('view.Users.fields.Username') },
                { key: 'createdDate', label: t('view.Users.fields.Registered') },
                { key: 'authorities', label: t('view.Users.fields.Role') },
                { key: 'emailVerified', label: t('view.Users.fields.EmailVerified') },
                { key: 'status', label: t('view.Users.fields.Status') },
                { key: 'action', label: t('common.Action') },
              ]}
              tableFilter
              itemsPerPageSelect
              itemsPerPage={itemsPerPage}
              activePage={currentPage - 1}
              onRowClick={(item, index, e) => handleToEdit(item, index, e)}
              tableProps={{
                striped: true,
                hover: true,
              }}
              scopedColumns={{
                name: (item) => (
                  <td>
                    <p className="text-primary m-0">{item.name}</p>
                    <p className="m-0">{item.email}</p>
                    <p className="m-0">{item.phone}</p>
                  </td>
                ),
                emailVerified: (item) => (
                  <td style={{ width: '120px', textAlign: 'center' }}>
                    {item.verifiedEmail ? (
                      <FaRegCheckSquare size="1.5em" />
                    ) : (
                      <FaRegSquare size="1.5em" />
                    )}
                  </td>
                ),
                status: (item) => (
                  <td>
                    <CBadge color={getBadge(item.accountStatus)}>{item.accountStatus}</CBadge>
                  </td>
                ),
                authorities: (item) => (
                  <td style={{ width: '130px' }}>
                    {item.authorities.map((role) => {
                      return (
                        <CBadge key={role.name} className="me-1" color={getRoleColor(role.name)}>
                          {role.name}
                        </CBadge>
                      )
                    })}
                  </td>
                ),
                action: (item) => {
                  return (
                    <td>
                      <CButton
                        color="primary"
                        variant="outline"
                        shape="square"
                        size="sm"
                        onClick={() => {
                          toggleDetails(item.id)
                        }}
                      >
                        {details.includes(item.id) ? 'Hide' : 'Show'}
                      </CButton>
                    </td>
                  )
                },
                details: (item) => {
                  return (
                    <CCollapse visible={details.includes(item.id)}>
                      <CCardBody>
                        <CButton
                          size="sm"
                          color="info"
                          className="me-4"
                          onClick={() => handleToEdit(item)}
                        >
                          {t('common.Edit')}
                        </CButton>
                        <CButton
                          size="sm"
                          color="success"
                          className="me-4"
                          onClick={() => handleResendVerifyEmail(item.id)}
                        >
                          {t('common.ResendVerifyEmail')}
                        </CButton>
                        <CButton
                          size="sm"
                          color="warning"
                          className="me-4"
                          onClick={() => handleResetPasswordEmail(item.id)}
                        >
                          {t('common.ResetPassword')}
                        </CButton>
                        <CButton size="sm" color="danger" className="me-4">
                          {t('common.Terminate')}
                        </CButton>
                      </CCardBody>
                    </CCollapse>
                  )
                },
              }}
            />
            <CSmartPagination
              activePage={page}
              onActivePageChange={pageChange}
              pages={totalPages}
              doubleArrows={false}
              align="center"
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Users

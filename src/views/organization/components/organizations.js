import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CCollapse,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CSpinner,
  CSmartTable,
  CSmartPagination,
} from '@coreui/react-pro'
import { FaRegBuilding, FaPlus, FaTrashAlt } from 'react-icons/fa'
import { organizationActions } from '../actions'

const Organizations = () => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const isFetched = useSelector((state) => state.organizations.isFetched)
  const itemsPerPage = useSelector((state) => state.organizations.itemsPerPage)
  const maxPage = useSelector((state) => state.organizations.totalPages)
  const organizations = useSelector((state) => state.organizations.organizations)
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)
  const [details, setDetails] = useState([])
  const [isDeleteOrganization, setDeleteOrganization] = useState(false)

  const organizeText = {
    description:
      'Represent the teams, business customers, and partner companies that access your applications as organizations',
    createOrganizationButton: 'Create Organization',
    show: 'Show',
    hide: 'Hide',
    delete: 'Delete',
    cancel: 'Cancel',
    organizationSettings: 'Organization Settings',
    confirmDeleteOrganization: 'Are you sure you want to delete organization ${name} ?',
  }

  const pageChange = (newPage) => {
    currentPage !== newPage && history.push(`/organizations?page=${newPage === 0 ? 1 : newPage}`)
  }

  const onPaginationChange = (numberItemsPerPage) => {
    dispatch(organizationActions.getOrganizationList({ page: 0, size: numberItemsPerPage }))
  }

  const navigateToCreateOrganization = () => {
    history.push('/organizations/create')
  }

  useEffect(() => {
    const loadData = async () => {
      await getAllOrganizations()
    }
    loadData()
  }, [dispatch, currentPage, page])

  const getAllOrganizations = async () => {
    dispatch(
      organizationActions.getOrganizationList({
        page: currentPage > 1 ? currentPage - 1 : 0,
        size: itemsPerPage,
      }),
    )
    setPage(currentPage)
  }

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  const openOrganizationDetailClick = (item, index, e) => {
    if (e === 'action') return
    history.push(`/organizations/${item.id}`)
  }

  const handleToDeleteOrganization = (item) => {
    dispatch(organizationActions.deleteOrganization(item.id)).then(() => {
      history.push('/organizations')
    })
  }

  const generateConfirmDelete = (item) => {
    return organizeText.confirmDeleteOrganization.replace('${name}', item.name)
  }

  const renderOrganizations = () => {
    return (
      <CRow className="table-responsive">
        <CSmartTable
          columns={[
            {
              key: 'name',
              _classes: 'font-weight-bold',
              label: 'Organization',
            },
            { key: 'displayName', label: t('common.DisplayName') },
            { key: 'action', label: '' },
          ]}
          items={organizations}
          itemsPerPage={itemsPerPage}
          activePage={currentPage - 1}
          onRowClick={(item, index, e) => openOrganizationDetailClick(item, index, e)}
          tableProps={{
            striped: true,
            hover: true,
          }}
          scopedColumns={{
            action: (item) => (
              <td>
                <CButton
                  color="primary"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={() => {
                    toggleDetails(item.id)
                  }}
                >
                  {details.includes(item.id) ? 'Hide' : 'Show'}
                </CButton>
              </td>
            ),
            details: (item) => {
              return (
                <CCollapse visible={details.includes(item.id)}>
                  <CCardBody>
                    <CButton
                      size="sm"
                      color="info"
                      className="me-4"
                      onClick={() => openOrganizationDetailClick(item)}
                    >
                      {organizeText.organizationSettings}
                    </CButton>
                    <CButton
                      size="sm"
                      color="danger"
                      className="ml-1"
                      onClick={() => setDeleteOrganization(true)}
                    >
                      <FaTrashAlt /> {organizeText.delete}
                    </CButton>
                    <CModal
                      visible={isDeleteOrganization}
                      onClose={() => setDeleteOrganization(false)}
                    >
                      <CModalHeader>
                        <CModalTitle>Delete Organization</CModalTitle>
                      </CModalHeader>
                      <CModalBody>{generateConfirmDelete(item)}</CModalBody>
                      <CModalFooter>
                        <CButton color="secondary" onClick={() => setDeleteOrganization(false)}>
                          {organizeText.cancel}
                        </CButton>
                        <CButton color="danger" onClick={() => handleToDeleteOrganization(item)}>
                          {organizeText.delete}
                        </CButton>
                      </CModalFooter>
                    </CModal>
                  </CCardBody>
                </CCollapse>
              )
            },
          }}
        />
        <CSmartPagination
          activePage={page}
          onActivePageChange={pageChange}
          pages={maxPage}
          doubleArrows={false}
          align="center"
        />
      </CRow>
    )
  }

  const renderCreateOrganization = () => {
    return (
      <CRow>
        <CCol className="text-center">
          <h1 className="mb-4">Organizations</h1>
          <FaRegBuilding width="100px" color="#bcbaff" size="10em" className="mb-4" />
          <p className="mb-4">{organizeText.description}</p>
          <CButton color="primary" className="mb-4" onClick={navigateToCreateOrganization}>
            <FaPlus /> {organizeText.createOrganizationButton}
          </CButton>
        </CCol>
      </CRow>
    )
  }

  const renderUI = () => {
    return organizations && organizations.length > 0
      ? renderOrganizations()
      : renderCreateOrganization()
  }

  return (
    <CRow>
      <CCol xl={12}>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm="9">
                <h4 id="traffic" className="card-title mb-0">
                  {t('view.Organizations.title')}
                </h4>
                <p className="text-muted">{organizeText.description}</p>
              </CCol>
              <CCol className="flex-right">
                <CButton color="primary" className="mb-4" onClick={navigateToCreateOrganization}>
                  <FaPlus /> {organizeText.createOrganizationButton}
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            {isFetched ? (
              renderUI()
            ) : (
              <div className="flex-center">
                <CSpinner color="primary" size="sm" />
              </div>
            )}
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Organizations

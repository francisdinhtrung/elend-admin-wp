import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { CCol, CRow, CButton, CSmartTable } from '@coreui/react-pro'
import { useDispatch, useSelector } from 'react-redux'
import { FaUser, FaUserTie } from 'react-icons/fa'
import { organizationActions } from '../../actions'

const TabMembers = (props) => {
  const dispatch = useDispatch()
  const history = useHistory()
  const { match, ...attributes } = props
  const allMembers = useSelector((state) => state.organizations.allMembers)
  const assignedMembers = useSelector((state) => state.organizations.assignedMembers)
  const itemsPerPage = useSelector((state) => state.organizations.itemsPerPage)
  const [addAssignMembers, setAddAssignMembers] = useState([])
  const [removedMembers, setRemovedMembers] = useState([])
  const [isLoadingMembers, setIsLoadingMembers] = useState(false)
  const [isLoadingAssignedMembers, setIsLoadingAssignedMembers] = useState(false)

  const organizeText = {
    title: 'Members',
    description: 'Add users directly from your tenant to become members of your organization',
    organizationTitle: 'Select Organizations',
    organizationOptionDesc: 'You may select an organization.',
    orgPlaceholder: 'Select Organization',
    assignMembersToOrg: 'Add Members to Organization',
    saveText: 'Save',
  }

  useEffect(() => {
    const loadData = async () => {
      await getAllUsers()
      await getAllUsersInOrg()
    }
    loadData()
  }, [dispatch])

  const getAllUsers = async () => {
    dispatch(
      organizationActions.searchMembersToOrganizationList({
        organizatonId: match.params.id,
      }),
    )
  }

  const getAllUsersInOrg = async () => {
    dispatch(
      organizationActions.searchMembersInOrganizationList({
        organizatonId: match.params.id,
      }),
    )
  }

  const getAssignedNumber = () => {
    return assignedMembers ? assignedMembers.length : 0
  }

  const handleSaveClick = () => {
    const members = []
    const addAssignMembers = assignedMembers.filter((member) => member.stateAction === 'ADD')
    const deletedMembers = allMembers.filter((member) => member.stateAction === 'REMOVE')

    if (addAssignMembers && addAssignMembers.length > 0) {
      addAssignMembers.forEach((member) => {
        members.push({
          userId: member.id,
          organizationId: match.params.id,
          isAssigned: true,
        })
      })
    }

    if (deletedMembers && deletedMembers.length > 0) {
      deletedMembers.forEach((member) => {
        members.push({
          userId: member.id,
          organizationId: match.params.id,
          isAssigned: false,
        })
      })
    }
    dispatch(organizationActions.updateMembersOrganization(members))
  }

  const handleToEdit = (item, index, e) => {
    if (e === 'firstName') return
    history.push(`/users/edit/${item.id}`)
  }

  const handleOnSelectedItemsChangeInAllMembers = (items) => {
    setAddAssignMembers(items)
  }

  const handleOnSelectedItemsChangeInAssignedMembers = (items) => {
    setRemovedMembers(items)
  }

  const handleToAddAssignMembers = () => {
    addAssignMembers.forEach((m) => {
      m.stateAction = 'ADD'
    })
    dispatch(organizationActions.updateAddMembersToOrganization(addAssignMembers))
  }

  const handleToRemoveAssignMembers = () => {
    removedMembers.forEach((m) => {
      m.stateAction = 'REMOVE'
    })
    dispatch(organizationActions.updateRemoveMembersFromOrganization(removedMembers))
  }

  return (
    <CRow className="mt-4">
      <CCol sm="12">
        <h4>{organizeText.title}</h4>
        <p className="text-muted">{organizeText.description}</p>
      </CCol>
      <CCol sm="12" className="mt-2">
        <h6>{organizeText.assignMembersToOrg}</h6>
      </CCol>
      <CCol sm="6" className="mt-2">
        <div>
          <FaUser size="1.5em" /> <span>Members</span>
        </div>
        <CSmartTable
          loading={isLoadingMembers}
          cleaner
          items={allMembers}
          columns={[
            { key: 'firstName', label: 'Firstname' },
            { key: 'lastName', label: 'LastName' },
            { key: 'email', label: 'Email' },
          ]}
          tableProps={{
            striped: true,
            hover: true,
          }}
          tableFilter
          itemsPerPageSelect
          itemsPerPage={itemsPerPage}
          clickableRows={true}
          onRowClick={(item, index, e) => handleToEdit(item, index, e)}
          pagination
          selectable
          onSelectedItemsChange={(items) => handleOnSelectedItemsChangeInAllMembers(items)}
        />
      </CCol>
      <CCol sm="6" className="mt-2">
        <div>
          <FaUserTie size="1.5em" /> <span>Assigned Members ({getAssignedNumber()})</span>
        </div>
        <CSmartTable
          loading={isLoadingAssignedMembers}
          cleaner
          items={assignedMembers}
          columns={[
            { key: 'firstName', label: 'Firstname' },
            { key: 'lastName', label: 'LastName' },
            { key: 'email', label: 'Email' },
          ]}
          tableProps={{
            striped: true,
            hover: true,
          }}
          tableFilter
          itemsPerPageSelect
          itemsPerPage={itemsPerPage}
          clickableRows={true}
          onRowClick={(item, index, e) => handleToEdit(item, index, e)}
          pagination
          selectable
          onSelectedItemsChange={(items) => handleOnSelectedItemsChangeInAssignedMembers(items)}
        />
      </CCol>
      <CCol sm="6" className="mt-2">
        <CButton
          color="success"
          onClick={handleToAddAssignMembers}
          disabled={!addAssignMembers || addAssignMembers.length === 0}
        >
          Add
        </CButton>
      </CCol>
      <CCol sm="6" className="mt-2">
        <CButton
          color="danger"
          onClick={handleToRemoveAssignMembers}
          disabled={!removedMembers || removedMembers.length === 0}
        >
          Removed
        </CButton>
      </CCol>
      <CCol sm="12" className="flex-right mt-4">
        <CButton onClick={handleSaveClick}>{organizeText.saveText}</CButton>
      </CCol>
    </CRow>
  )
}
export default TabMembers

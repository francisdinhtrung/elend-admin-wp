import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  CCol,
  CRow,
  CButton,
  CButtonGroup,
  CFormLabel,
  CFormInput,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CModal,
  CFormSwitch,
} from '@coreui/react-pro'
import { useDispatch, useSelector } from 'react-redux'
import InputColor from 'react-input-color'
import { toast } from 'react-toastify'
import { organizationActions } from '../../actions'
import api from '../../../../utils/api'

const TabOrgOverview = (props) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const { match, ...attributes } = props
  const organization = useSelector((state) => state.organizations.organization) || {
    name: '',
    displayName: '',
    currentBrand: {
      primaryColor: '#0059d6',
      backgroundColor: '#FFF',
    },
  }

  const [isDeleteOrganization, setDeleteOrganization] = useState(false)
  const [fileLogo, setLogo] = useState([])
  const [displayLogoUrl, setDisplayLogoUrl] = useState(
    `${process.env.PUBLIC_URL}/images/alogo-1.svg`,
  )

  if (
    organization.currentBrand &&
    organization.currentBrand.logoUrl &&
    organization.currentBrand.logoUrl.indexOf('http') > -1 &&
    displayLogoUrl !== organization.currentBrand.logoUrl
  ) {
    setDisplayLogoUrl(organization.currentBrand.logoUrl)
  }

  const organizeText = {
    EditOrganization: 'Edit Organization',
    BasicInfo: 'Basic Info',
    BasicInfoDesc: 'These are basic details needed to set up your new organization.',
    Name: 'Name',
    NamePlaceholder: 'Enter a human-readable identifier for this organization',
    NameDesc:
      'This is any human-readable identifier for the organization that will be used by end-users to direct them to their organization in your application. This name cannot be changed.',
    DisplayName: 'Display Name',
    DisplayNamePlaceHolder: 'Enter a friendly name for this organization',
    DisplayDesc:
      'If set, this is the name that will be displayed to end-users for this organization in any interaction with them.',
    Overview: 'Overview',
    Active: 'Active',
    Members: 'Members',
    Invitations: 'Invitations',
    Branding: 'Branding',
    BrandingDesc: 'These are branding settings associated with your organization.',
    Logo: 'Organization Logo',
    LogoDesc:
      'If set, this is the logo that will be displayed to end-users for this organization in any interaction with them.',
    PrimaryColor: 'Primary Color',
    PrimaryColorDesc: `If set, this will be the primary color for CTAs that will be displayed to end-users for this organization in your application's authentication flows.`,
    PageBackgroundColor: `Page Background Color`,
    BackgroundColorDesc: `If set, this will be the page background color that will be displayed to end-users for this organization in in your application's authentication flows.`,
    DeleteOrg: 'Delete this organization',
    DeleteOrgDesc: `All user membership to this organization will be removed. Users won't be removed. Once confirmed, this operation can't be undone!`,
    Save: 'Save',
    Delete: 'Delete',
    cancel: 'Cancel',
    confirmDeleteOrganization: 'Are you sure you want to delete organization ${name} ?',
  }

  const generateConfirmDelete = (item) => {
    return organizeText.confirmDeleteOrganization.replace('${name}', item.name)
  }

  const handleToDeleteOrganization = (item) => {
    dispatch(organizationActions.deleteOrganization(item.uuid)).then(() => {
      history.push('/organizations')
    })
  }

  const handleToUpdateOrganization = () => {
    if (!organization.uuid || organization.uuid.length === 0) return

    // save brand section
    organization.brands[0] = organization.currentBrand
    const payload = Object.assign({}, organization)
    delete payload.currentBrand
    dispatch(organizationActions.updateOrganization(payload))
  }

  const handleToUploadLogo = () => {
    if (!fileLogo || fileLogo.length === 0) return
    api.cheveretoService
      .uploadImage(fileLogo[0])
      .then((response) => {
        return response.json()
      })
      .then((result) => {
        if (result.status_code === 200) {
          toast.success(`Upload the image successfully.`)
          organization.currentBrand.logoUrl = result.image.url
        } else {
          toast.error(result.error.message)
        }
      })
      .catch((error) => {
        console.error('error=', error)
      })
  }

  const onUploadFileChange = (e) => {
    const file = e.target.files[0]
    setLogo(e.target.files)
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = (e) => {
      setDisplayLogoUrl(reader.result)
    }
  }

  const onChangePrimaryColor = (color) => {
    if (!organization.currentBrand) {
      organization.currentBrand = {}
    }
    organization.currentBrand.primaryColor = color.hex
  }

  const onChangeBackgroundColor = (color) => {
    if (!organization.currentBrand) {
      organization.currentBrand = {}
    }
    organization.currentBrand.backgroundColor = color.hex
  }

  const onChangeDisplayName = (e) => {
    organization.displayName = e.target.value
  }

  useEffect(() => {
    dispatch(organizationActions.getOrganization(match.params.id))
  }, [dispatch])

  return (
    <CRow className="mt-4">
      <CCol sm="12">
        <h4>{organizeText.BasicInfo}</h4>
        <p className="text-muted">{organizeText.BasicInfoDesc}</p>
      </CCol>
      <CCol sm="12">
        <div>
          <CFormLabel htmlFor="name">{organizeText.Name}</CFormLabel>
          <CFormInput id="name" value={organization.name} disabled />
          <p className="text-muted">{organizeText.NameDesc}</p>
        </div>
      </CCol>
      <CCol sm="12">
        <div>
          <CFormLabel htmlFor="displayName">{organizeText.DisplayName}</CFormLabel>
          <CFormInput
            id="displayName"
            placeholder={organizeText.DisplayNamePlaceHolder}
            value={organization.displayName}
            onChange={onChangeDisplayName}
            required
          />
          <p className="text-muted">{organizeText.DisplayDesc}</p>
        </div>
      </CCol>
      <CCol sm={12} className="mb-4 col-org-active">
        <CFormLabel className="me-2">{organizeText.Active}</CFormLabel>
        <CFormSwitch
          size="xl"
          className={'mx-1'}
          variant={'3d'}
          color={'success'}
          checked={organization.enabled}
        />
      </CCol>
      <CCol sm={12} className="mb-4">
        <hr />
      </CCol>
      <CCol sm={12} className="mb-4">
        <h4>{organizeText.Branding}</h4>
        <p className="text-muted">{organizeText.BrandingDesc}</p>
      </CCol>
      <CCol sm={12}>
        <CFormLabel className="font-weight-bold">{organizeText.Logo}</CFormLabel>
      </CCol>
      <CCol sm={12} className="mb-4">
        <div className="box-logo">
          <img src={displayLogoUrl} height="80px" />
        </div>
      </CCol>
      <CCol sm={12} className="mb-3">
        <CButtonGroup role="group">
          <CFormInput type="file" onChange={(e) => onUploadFileChange(e)}></CFormInput>
          <CButton color="primary" onClick={handleToUploadLogo}>
            Upload
          </CButton>
        </CButtonGroup>
      </CCol>
      <CCol sm={12}>
        <CFormLabel htmlFor="primaryColor" className="font-weight-bold">
          {organizeText.PrimaryColor}
        </CFormLabel>
        <div>
          <InputColor
            initialValue={
              organization && organization.currentBrand
                ? organization.currentBrand.primaryColor
                : '#fff'
            }
            onChange={onChangePrimaryColor}
            placement="right"
          />
          <p className="text-muted">{organizeText.PrimaryColorDesc}</p>
        </div>
      </CCol>
      <CCol sm={12}>
        <CFormLabel htmlFor="primaryColor" className="font-weight-bold">
          {organizeText.PageBackgroundColor}
        </CFormLabel>
        <div>
          <InputColor
            initialValue={
              organization && organization.currentBrand
                ? organization.currentBrand.backgroundColor
                : '#000'
            }
            onChange={onChangeBackgroundColor}
            placement="right"
          />
          <p className="text-muted">{organizeText.BackgroundColorDesc}</p>
        </div>
      </CCol>
      <CCol sm={12} className="text-center">
        <CButton color="primary" onClick={handleToUpdateOrganization}>
          {organizeText.Save}{' '}
        </CButton>
      </CCol>
      <CCol sm={12} className="mt-4 p-4 text-left bg-danger-light">
        <h4>{organizeText.DeleteOrg}</h4>
        <CRow>
          <CCol sm={11}>{organizeText.DeleteOrgDesc}</CCol>
          <CCol>
            <CButton color="danger" onClick={() => setDeleteOrganization(true)}>
              {organizeText.Delete}
            </CButton>
          </CCol>
        </CRow>
        <CModal visible={isDeleteOrganization} onClose={() => setDeleteOrganization(false)}>
          <CModalHeader>
            <CModalTitle>Delete Organization</CModalTitle>
          </CModalHeader>
          <CModalBody>{generateConfirmDelete(organization)}</CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={() => setDeleteOrganization(false)}>
              {organizeText.cancel}
            </CButton>
            <CButton color="danger" onClick={() => handleToDeleteOrganization(organization)}>
              {organizeText.Delete}
            </CButton>
          </CModalFooter>
        </CModal>
      </CCol>
    </CRow>
  )
}

export default TabOrgOverview

import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CNavItem,
  CNavLink,
  CNav,
  CTabContent,
  CTabPane,
} from '@coreui/react-pro'
import { FaRegBuilding } from 'react-icons/fa'
import TabOrgOverview from './tabs/tabOrgOverview'
import TabMembers from './tabs/tabMembers'

const Organization = ({ match }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const [activeKey, setActiveKey] = useState(1)

  const tabs = {
    OVERVIEW: 1,
    MEMBERS: 2,
    INVITATIONS: 3,
  }

  const organizeText = {
    EditOrganization: 'Edit Organization',
    Overview: 'Overview',
    Members: 'Members',
    Invitations: 'Invitations',
    DeleteOrg: 'Delete this organization',
    DeleteOrgDesc: `All user membership to this organization will be removed. Users won't be removed. Once confirmed, this operation can't be undone!`,
  }

  const handleTabSelected = (tabIndex) => {
    setActiveKey(tabIndex)
  }

  return (
    <CRow>
      <CCol xl={12}>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm="8">
                <h4 id="traffic" className="card-title mb-0">
                  <FaRegBuilding size="2em" /> {organizeText.EditOrganization}
                </h4>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            <CNav>
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink
                    data-tab="overview"
                    active={activeKey === tabs.OVERVIEW}
                    onClick={() => handleTabSelected(tabs.OVERVIEW)}
                  >
                    {organizeText.Overview}
                  </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink
                    data-tab="members"
                    active={activeKey === tabs.MEMBERS}
                    onClick={() => handleTabSelected(tabs.MEMBERS)}
                  >
                    {organizeText.Members}
                  </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink
                    data-tab="invitations"
                    active={activeKey === tabs.INVITATIONS}
                    onClick={() => handleTabSelected(tabs.INVITATIONS)}
                  >
                    {organizeText.Invitations}
                  </CNavLink>
                </CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane visible={activeKey === tabs.OVERVIEW}>
                  <TabOrgOverview match={match} />
                </CTabPane>
                <CTabPane visible={activeKey === tabs.MEMBERS}>
                  <TabMembers match={match} />
                </CTabPane>
                <CTabPane visible={activeKey === tabs.INVITATIONS}>
                  <div>Invitation</div>
                </CTabPane>
              </CTabContent>
            </CNav>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Organization

import React from 'react'
import { Redirect, useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CFormLabel,
  CFormInput,
  CForm,
  CFormFeedback,
} from '@coreui/react-pro'
import { FaRegBuilding } from 'react-icons/fa'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { organizationActions } from '../actions'

const OrganizationCreation = () => {
  const { t } = useTranslation()
  const history = useHistory()
  const organization = useSelector((state) => state.organizations.organization)
  const isRedirect = useSelector((state) => state.organizations.isRedirect)
  const dispatch = useDispatch()
  const organizeText = {
    AddOrganization: 'Add Organization',
    BasicInfo: 'Basic Info',
    BasicInfoDesc: 'These are basic details needed to set up your new organization.',
    Name: 'Name',
    NamePlaceholder: 'Enter a human-readable identifier for this organization',
    NameDesc:
      'This is any human-readable identifier for the organization that will be used by end-users to direct them to their organization in your application. This name cannot be changed.',
    DisplayName: 'Display Name',
    DisplayNamePlaceHolder: 'Enter a friendly name for this organization',
    DisplayDesc:
      'If set, this is the name that will be displayed to end-users for this organization in any interaction with them.',
  }

  const nameReg = /^[a-zA-Z0-9_.-]*$/
  const schema = Yup.object({
    name: Yup.string()
      .required('"Name" length must be at least 3 characters long')
      .min(3, '"Name" length must be at least 3 characters long')
      .matches(
        nameReg,
        `"Name" must only contain lowercase characters, '-', and '_', and start with a letter or number`,
      ),
  })

  const formik = useFormik({
    initialValues: {
      name: '',
      displayName: '',
      description: '',
      email: 'demo@example.com',
      phone: '0123456789',
    },
    enableReinitialize: true,
    validationSchema: schema,
    onSubmit: (values) => handleToCreateOrganization(values),
  })

  const handleToCreateOrganization = (data) => {
    if (!data.name || data.name.length === 0) return
    if (!data.displayName || data.displayName.length === 0) {
      data.displayName = data.name
    }
    dispatch(organizationActions.createOrganization(data))
  }

  return isRedirect && organization ? (
    <Redirect to={{ pathname: `/organizations/${organization.id}` }} />
  ) : (
    <CRow>
      <CCol xl={12}>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm="8">
                <h4 id="traffic" className="card-title mb-0">
                  <FaRegBuilding size="2em" /> {organizeText.AddOrganization}
                </h4>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            <CForm onSubmit={formik.handleSubmit}>
              <CRow>
                <CCol sm="12">
                  <div>
                    <CFormLabel htmlFor="name">{organizeText.Name}</CFormLabel>
                    <CFormInput
                      id="name"
                      placeholder={organizeText.NamePlaceholder}
                      value={formik.values.name}
                      invalid={formik.errors.name && formik.touched.name}
                      {...formik.getFieldProps('name')}
                    />
                    <CFormFeedback
                      invalid
                      style={{
                        display: formik.errors.name && formik.touched.name ? 'block' : 'none',
                      }}
                    >
                      {formik.errors.name}
                    </CFormFeedback>
                    <p className="text-muted">{organizeText.NameDesc}</p>
                  </div>
                </CCol>
                <CCol sm="12">
                  <div>
                    <CFormLabel htmlFor="displayName">{organizeText.DisplayName}</CFormLabel>
                    <CFormInput
                      id="displayName"
                      placeholder={organizeText.DisplayNamePlaceHolder}
                      value={formik.values.displayName}
                      {...formik.getFieldProps('displayName')}
                    />
                    <p className="text-muted">{organizeText.DisplayDesc}</p>
                  </div>
                </CCol>
                <CCol sm="12" className="text-center">
                  <CButton color="primary" type="submit">
                    {organizeText.AddOrganization}{' '}
                  </CButton>
                </CCol>
              </CRow>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default OrganizationCreation

import React from 'react'
import { useTranslation } from 'react-i18next'
import { CCol, CRow, CSmartTable } from '@coreui/react-pro'
import { loanHelpers } from '../../../../utils/loan-helper'

const TabSummary = (props) => {
  const { t } = useTranslation()
  const { paramId, loan, transactions, repaymentSchedule, ...attributes } = props
  const typePayOffDebt = loan.typePayOffDebt
  const totalItem = repaymentSchedule[repaymentSchedule.length - 1]
  const totalPrincipal = loan.principalDisbursedDerived || 0
  let totalInterest = loan.interestDisbursedDerived || 0
  let totalPenalties = loan.penaltiesDisbursedDerived || 0
  let totalFees = loan.feesDisbursedDerived || 0
  const totalBalance = loan.principalDisbursedDerived + totalInterest + totalPenalties + totalFees

  let pendingCapital = totalPrincipal - totalItem.principalRepaidDerived
  let pendingInterest = totalInterest - totalItem.interestRepaidDerived
  let pendingPenalties = totalPenalties > 0 ? totalPenalties - totalItem.penaltiesRepaidDerived : 0
  let pendingFee = totalFees > 0 ? totalFees - totalItem.feesRepaidDerived : 0
  let pendingTotal = totalBalance - totalItem.paid
  if (typePayOffDebt && typePayOffDebt.length > 0) {
    pendingCapital = 0
    pendingInterest = 0
    pendingTotal = 0
  }

  const data = [
    {
      balance: t('view.Loan.TotalBalance'),
      capital: loanHelpers.currencyFormatter(totalPrincipal) || 0,
      interest: loanHelpers.currencyFormatter(totalInterest),
      paid: '0.00',
      more: '0.00',
      penalties: loanHelpers.currencyFormatter(totalPenalties),
      fees: loanHelpers.currencyFormatter(totalFees),
      total: loanHelpers.currencyFormatter(totalBalance),
      _cellProps: {
        balance: {
          color: 'primary',
          width: '15%',
        },
      },
    },
    {
      balance: t('view.Loan.PaidBalance'),
      capital: loanHelpers.currencyFormatter(totalItem.principalRepaidDerived),
      interest: loanHelpers.currencyFormatter(totalItem.interestRepaidDerived),
      paid: '0.00',
      more: '0.00',
      penalties: loanHelpers.currencyFormatter(totalItem.penaltiesRepaidDerived),
      fees: loanHelpers.currencyFormatter(totalItem.feesRepaidDerived),
      total: loanHelpers.currencyFormatter(totalItem.paid),
      _cellProps: {
        balance: {
          color: 'primary',
        },
      },
    },
    {
      balance: t('view.Loan.PendingBalance'),
      capital: loanHelpers.currencyFormatter(pendingCapital),
      interest: loanHelpers.currencyFormatter(pendingInterest),
      paid: '0.00',
      more: '0.00',
      penalties: loanHelpers.currencyFormatter(pendingPenalties),
      fees: loanHelpers.currencyFormatter(pendingFee),
      total: loanHelpers.currencyFormatter(pendingTotal),
      _cellProps: {
        balance: {
          color: 'primary',
        },
      },
    },
  ]

  const getSummaryArrearsAmount = () => {
    return loan && loan.summary && loan.summary.arrearsAmount > 0
      ? loanHelpers.currencyFormatter(loan.summary.arrearsAmount)
      : 0
  }

  const getSummaryArrearsDays = () => {
    return loan && loan.summary ? loan.summary.arrearsDays : 0
  }

  const getSummaryLastPayment = () => {
    return loan && loan.summary ? loan.summary.lastPayment : ''
  }

  const getSummaryNextPayment = () => {
    if (typePayOffDebt && typePayOffDebt.length > 0) {
      const payOffText = `common.${typePayOffDebt}`
      return t(payOffText)
    }
    return loan && loan.summary ? loan.summary.nextPayment : ''
  }

  return (
    <CRow className="mt-4">
      <CCol>
        <CRow>
          <CCol xl={6}>
            <CRow className="mb-3">
              <CCol>
                <b>{t('view.Borrowers.tabSummary.AmountInArrears')}</b>
              </CCol>
              <CCol>
                <span
                  style={{
                    color:
                      loan && loan.summary && loan.summary.hasArrearsAmount ? '#e55353' : 'black',
                  }}
                >
                  {getSummaryArrearsAmount()}
                </span>
              </CCol>
            </CRow>
            <CRow className="mb-3">
              <CCol>
                <b>{t('view.Borrowers.tabSummary.DaysInArrears')}</b>
              </CCol>
              <CCol>
                <span
                  style={{
                    color:
                      loan && loan.summary && loan.summary.hasArrearsDays ? '#e55353' : 'black',
                  }}
                >
                  {getSummaryArrearsDays()}
                </span>
              </CCol>
            </CRow>
          </CCol>
          <CCol xl={6}>
            <CRow className="mb-3">
              <CCol>
                <b>{t('view.Borrowers.tabSummary.LastPayment')}</b>
              </CCol>
              <CCol>{getSummaryLastPayment()}</CCol>
            </CRow>
            <CRow className="mb-3">
              <CCol>
                <b>{t('view.Borrowers.tabSummary.NextPayment')}</b>
              </CCol>
              <CCol>{getSummaryNextPayment()}</CCol>
            </CRow>
          </CCol>
        </CRow>
        <CRow>
          <CCol className="table-responsive">
            <CSmartTable
              items={data}
              columns={[
                { key: 'balance', label: t('view.Borrowers.tabSummary.Balance') },
                { key: 'capital', label: t('view.Borrowers.tabSummary.Capital') },
                { key: 'interest', label: t('view.Borrowers.tabSummary.Interest') },
                { key: 'fees', label: t('view.Borrowers.tabSummary.Fee') },
                { key: 'penalties', label: t('view.Borrowers.tabSummary.Penalty') },
                { key: 'total', label: t('view.Borrowers.tabSummary.Total') },
              ]}
              tableHeadProps={{
                color: 'primary',
              }}
              tableProps={{
                hover: true,
              }}
              scopedColumns={{
                capital: (item) => <td className="text-right">{item.capital}</td>,
                interest: (item) => <td className="text-right">{item.interest}</td>,
                fees: (item) => <td className="text-right">{item.fees}</td>,
                penalties: (item) => <td className="text-right">{item.penalties}</td>,
                total: (item) => <td className="text-right">{item.total}</td>,
              }}
            />
          </CCol>
        </CRow>
      </CCol>
    </CRow>
  )
}

export default TabSummary

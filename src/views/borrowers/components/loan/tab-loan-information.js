import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import {
  CCol,
  CRow,
  CButton,
  CTable,
  CTableBody,
  CTableRow,
  CTableDataCell,
  CBadge,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CModal,
  CFormInput,
  CFormLabel,
  CFormTextarea,
  CDatePicker,
} from '@coreui/react-pro'
import { useDispatch } from 'react-redux'
import moment from 'moment'
import { FaRegSquare, FaRegCheckSquare } from 'react-icons/fa'
import { colorHelpers } from '../../../../utils/color-helper'
import { loanActions } from '../../../loans/actions'
import { accountHelpers } from '../../../../utils/account-helper'
import { useHistory } from 'react-router-dom'
import { loanHelpers } from '../../../../utils/loan-helper'

const TabLoanInformation = (props) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const { paramId, loan, ...attributes } = props
  const [isApproved, setApproval] = useState(false)
  const [isDisburse, setDisburse] = useState(false)
  const [danger, setDanger] = useState(false)
  const [isUndoApproval, setUndoApproval] = useState(false)
  const [isDeclined, setIsDeclined] = useState(false)
  const [isBlacklist, setBlacklistModal] = useState(false)
  const [approvalLoan, setApprovalLoan] = useState({
    approvedDate: moment().format('yyyy-MM-DD'),
    approvedAmount: loan.amountPrincipal,
    notes: '',
  })
  const [declineLoan, setDeclineLoan] = useState({
    declinedDate: moment().format('yyyy-MM-DD'),
    declinedAmount: loan.amountPrincipal,
    notes: '',
  })
  const [disburseLoan, setDisburseLoan] = useState({
    disburseDate: moment(loan.expectedDisbursementDate).format('yyyy-MM-DD'),
    dateOfFirstPayment: moment(loan.expectedFirstPaymentDate).format('yyyy-MM-DD'),
    disbursedBy: '',
    notes: '',
  })
  const loanStatus = {
    ACTIVE: 'ACTIVE',
    APPROVED: 'APPROVED',
    PENDING: 'PENDING',
    SUBMITTED: 'SUBMITTED',
    REJECTED: 'REJECTED',
  }
  const account = accountHelpers.getAccount()
  const loanData = [
    {
      title: t('view.Borrowers.tabInformation.LoanStatus'),
      value: loan.status,
      field: 'loanStatus',
    },
    {
      title: t('view.Borrowers.tabInformation.LoanNumber'),
      value: loan.id,
      field: 'loanNumber',
    },
    {
      title: t('view.Borrowers.tabInformation.AutoPenalty'),
      value: loan.enableAutoPenalty,
      field: 'enableAutoPenalty',
    },
    {
      title: t('view.Borrowers.tabInformation.Organization'),
      value: loan.organizationName,
      field: 'organization',
    },
    {
      title: t('view.Borrowers.tabInformation.ApprovedAmount'),
      value: `${loan.amountPrincipal}`,
      field: 'approvedAmount',
    },
    {
      title: t('view.Borrowers.tabInformation.DisbursementDate'),
      value: moment(loan.expectedDisbursementDate).format('DD/MM/yyyy'),
      field: 'disbursementDate',
    },
    {
      title: t('view.Borrowers.tabInformation.MethodOfInterest'),
      value: 'Flat',
      field: 'methodOfInterest',
    },
    {
      title: t('view.Borrowers.tabInformation.InterestRate'),
      value: `${loan.interestRate}% / ${loan.interestRateType}`,
      field: 'interestRate',
    },
    {
      title: t('view.Borrowers.tabInformation.Duration'),
      value: `${loan.loanTerm} ${loan.loanTermType}`,
      field: 'duration',
    },
    {
      title: t('view.Borrowers.tabInformation.RepaymentCycle'),
      value: `Every ${loan.repaymentFrequency} ${loan.repaymentCycle}`,
      field: 'repaymentCycle',
    },
    // {
    //   title: t('view.Borrowers.tabInformation.AmountOfPayments'),
    //   value: '0',
    //   field: 'amountOfPayments',
    // },
    {
      title: t('view.Borrowers.tabInformation.StartPaying'),
      value: moment(loan.expectedFirstPaymentDate).format('DD/MM/yyyy'),
      field: 'startPaying',
    },
  ]

  const getBadge = (status) => {
    if (!status) return
    return colorHelpers.getColorByStatus(status.toLowerCase(), false)
  }

  const handleApproveLoan = () => {
    setApproval(false)
    const payload = {
      loanId: loan.id,
      approvedAmount: approvalLoan.approvedAmount,
      approvedOnDate: loanHelpers.dateFormatter(approvalLoan.approvedDate),
      approvedNotes: approvalLoan.notes,
      approvedByUserId: account.id,
    }
    dispatch(loanActions.approveLoan(payload))
  }

  const handleToShowDeclineLoan = () => {
    setIsDeclined(true)
  }

  const handleDeclineLoan = () => {
    const payload = {
      loanId: loan.id,
      rejectedByUserId: account.id,
      rejectedOnDate: loanHelpers.dateFormatter(declineLoan.declinedDate),
      rejectedAmount: 0,
      notes: declineLoan.notes,
    }
    dispatch(loanActions.rejectLoan(payload))
  }

  const handleEditLoan = () => {
    history.push(`/loans/${loan.id}/edit`)
  }

  const handleToShowDeleteLoan = () => {
    setDanger(true)
  }

  const handleApprovalChange = (e) => {
    const { name, value } = e.target
    setApprovalLoan((approvalLoan) => ({ ...approvalLoan, [name]: value }))
  }

  const handleDeclineChange = (e) => {
    const { name, value } = e.target
    setDeclineLoan((declineLoan) => ({ ...declineLoan, [name]: value }))
  }

  const handleDisburseChange = (e) => {
    const { name, value } = e.target
    setDisburseLoan((disburseLoan) => ({ ...disburseLoan, [name]: value }))
  }

  const handleToSubmitDisburse = () => {
    const payload = {
      loanId: loan.id,
      disbursedOnDate: loanHelpers.dateFormatter(disburseLoan.disburseDate),
      firstPaymentDate: loanHelpers.dateFormatter(disburseLoan.dateOfFirstPayment),
      createdById: account.id,
      notes: disburseLoan.notes,
    }
    dispatch(loanActions.disburseLoan(payload))
  }

  const handleUnApproval = () => {
    setUndoApproval(false)
    const payload = {
      loanId: loan.id,
      createdByUserId: account.id,
    }
    dispatch(loanActions.undoApprovalLoan(payload))
  }

  const handleUndoDisbursement = () => {
    console.log('handleUndoDisbursement')
  }

  const handleDeleteLoan = () => {
    dispatch(loanActions.deleteLoan(loan.id))
  }

  const handleUndoRejection = () => {
    const payload = {
      loanId: loan.id,
      createdByUserId: account.id,
    }
    dispatch(loanActions.undoRejectLoan(payload))
  }

  const renderButtons = () => {
    switch (loan.status) {
      case loanStatus.APPROVED: {
        return (
          <CCol>
            <CButton
              type="button"
              color="success"
              className="text-center me-3"
              onClick={() => setDisburse(true)}
            >
              {t('common.Disburse')}
            </CButton>
            <CButton
              type="button"
              color="danger"
              className="text-center"
              onClick={() => setUndoApproval(true)}
            >
              {t('common.UndoApproval')}
            </CButton>
          </CCol>
        )
      }
      case loanStatus.PENDING:
      case loanStatus.SUBMITTED: {
        return (
          <CCol>
            <CButton
              type="button"
              color="success"
              className="text-center me-3"
              onClick={() => setApproval(true)}
            >
              {t('common.Approve')}
            </CButton>
            <CButton
              type="button"
              color="danger"
              className="text-center me-3"
              onClick={handleToShowDeclineLoan}
            >
              {t('common.Decline')}
            </CButton>
          </CCol>
        )
      }
      case loanStatus.REJECTED: {
        return (
          <CCol>
            <CButton
              type="button"
              color="success"
              className="text-center me-3"
              onClick={handleUndoRejection}
            >
              {t('common.UndoDecline')}
            </CButton>
          </CCol>
        )
      }
      default:
        return null
    }
  }

  const renderEditButtons = () => {
    switch (loan.status) {
      case loanStatus.APPROVED:
      case loanStatus.SUBMITTED:
      case loanStatus.PENDING: {
        return (
          <CCol>
            <CButton
              type="button"
              color="primary"
              className="text-center me-3"
              onClick={handleEditLoan}
            >
              {t('common.Edit')}
            </CButton>
            <CButton
              type="button"
              color="primary"
              className="text-center me-3"
              onClick={handleToShowDeleteLoan}
            >
              {t('common.Delete')}
            </CButton>
            <CButton
              type="button"
              color="danger"
              className="text-center"
              onClick={() => setBlacklistModal(true)}
            >
              {t('common.Blacklist')}
            </CButton>
          </CCol>
        )
      }
      case loanStatus.ACTIVE:
        return (
          <CCol>
            <CButton
              type="button"
              color="primary"
              className="text-center me-3"
              onClick={handleEditLoan}
            >
              {t('common.Edit')}
            </CButton>
            <CButton
              type="button"
              color="danger"
              className="text-center"
              onClick={() => setBlacklistModal(true)}
            >
              {t('common.Blacklist')}
            </CButton>
          </CCol>
        )
      default:
        return null
    }
  }

  const renderPopupDeclineLoan = () => {
    return (
      <CModal
        size="lg"
        visible={isDeclined}
        onClose={() => {
          setIsDeclined(false)
        }}
        color="warning"
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('view.Loan.DeclineLoan')}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xl={12}>
              <CFormLabel htmlFor="declinedDate" className="col-form-label">
                {t('view.Loan.DeclinedDate')}
              </CFormLabel>
              <CDatePicker
                id="declinedDate"
                name="declinedDate"
                date={declineLoan.declinedDate}
                onDateChange={(date) => {
                  setDeclineLoan((declineLoan) => ({ ...declineLoan, declinedDate: date }))
                }}
              />
            </CCol>
            <CCol>
              <CFormLabel htmlFor="notes" className="col-form-label">
                {t('common.Notes')}
              </CFormLabel>
              <CFormTextarea
                id="notes"
                name="notes"
                rows="3"
                value={declineLoan.notes}
                onChange={handleDeclineChange}
              ></CFormTextarea>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleDeclineLoan}>
            {t('common.Save')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setIsDeclined(false)
            }}
          >
            {t('common.Close')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }
  const renderPopupDelete = () => {
    return (
      <CModal
        visible={danger}
        onClose={() => {
          setDanger(false)
        }}
        color="danger"
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.ConfirmDelete')}</CModalTitle>
        </CModalHeader>
        <CModalBody>{t('messages.messageConfirmDelete')}</CModalBody>
        <CModalFooter>
          <CButton color="danger" onClick={handleDeleteLoan}>
            {t('common.Delete')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setDanger(false)
            }}
          >
            {t('common.Cancel')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }
  const renderPopupDisburse = () => {
    return (
      <CModal
        size="lg"
        visible={isDisburse}
        onClose={() => {
          setDisburse(false)
        }}
        color="warning"
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('view.Loan.DisburseLoan')}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xl={12}>
              <CFormLabel htmlFor="disburseDate" className="col-form-label">
                {t('view.Loan.DisbursementDate')}
              </CFormLabel>
              <CDatePicker
                id="disburseDate"
                name="disburseDate"
                date={disburseLoan.disburseDate}
                onDateChange={(date) => {
                  setDisburseLoan((disburseLoan) => ({ ...disburseLoan, disburseDate: date }))
                }}
              />
            </CCol>
            <CCol>
              <CFormLabel htmlFor="notes" className="col-form-label">
                {t('common.Comments')}
              </CFormLabel>
              <CFormTextarea
                id="notes"
                name="notes"
                rows="3"
                value={disburseLoan.notes}
                onChange={handleDisburseChange}
              ></CFormTextarea>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleToSubmitDisburse}>
            {t('common.Save')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setDisburse(false)
            }}
          >
            {t('common.Close')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }
  const renderPopupApproval = () => {
    return (
      <CModal
        size="lg"
        visible={isApproved}
        onClose={() => {
          setApproval(false)
        }}
        color="warning"
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('view.Loan.ApproveLoan')}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xl={12}>
              <CFormLabel htmlFor="approvedDate" className="col-form-label">
                {t('view.Loan.ApprovedDate')}
              </CFormLabel>
              <CDatePicker
                id="approvedDate"
                name="approvedDate"
                date={approvalLoan.approvedDate}
                onDateChange={(date) => {
                  setApprovalLoan((approvalLoan) => ({ ...approvalLoan, approvedDate: date }))
                }}
              />
            </CCol>
            <CCol xl={12}>
              <CFormLabel htmlFor="approvedAmount" className="col-form-label">
                {t('view.Loan.ApprovedAmount')}
              </CFormLabel>
              <CFormInput
                id="approvedAmount"
                name="approvedAmount"
                value={approvalLoan.approvedAmount}
                onChange={handleApprovalChange}
              />
            </CCol>
            <CCol>
              <CFormLabel htmlFor="notes" className="col-form-label">
                {t('common.Notes')}
              </CFormLabel>
              <CFormTextarea
                id="notes"
                name="notes"
                rows="3"
                value={approvalLoan.notes}
                onChange={handleApprovalChange}
              ></CFormTextarea>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleApproveLoan}>
            {t('common.Save')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setApproval(false)
            }}
          >
            {t('common.Close')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const renderPopupUndoApproval = () => {
    return (
      <CModal
        visible={isUndoApproval}
        onClose={() => {
          setUndoApproval(false)
        }}
        color="danger"
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.UndoApproval')}</CModalTitle>
        </CModalHeader>
        <CModalBody>{t('messages.messageConfirmYesNo')}</CModalBody>
        <CModalFooter>
          <CButton color="danger" onClick={handleUnApproval}>
            {t('common.Yes')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setUndoApproval(false)
            }}
          >
            {t('common.No')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const renderCell = (item) => {
    switch (item.field) {
      case 'loanStatus':
        return <CBadge color={getBadge(item.value)}>{item.value}</CBadge>
      case 'enableAutoPenalty':
        return item.value ? <FaRegCheckSquare color="#1e9ff2" /> : <FaRegSquare />
      case 'approvedAmount':
        return loanHelpers.currencyFormatter(item.value)
      default:
        return item.value
    }
  }

  const handleToMoveLoanToBlacklist = () => {
    setBlacklistModal(false)
    const payload = {
      loanId: loan.id,
      blacklistByUserId: account.id,
    }
    dispatch(loanActions.blacklistLoan(payload))
  }

  const renderBlacklistModal = () => {
    return (
      <CModal
        visible={isBlacklist}
        onClose={() => {
          setBlacklistModal(false)
        }}
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.Confirm')}</CModalTitle>
        </CModalHeader>
        <CModalBody>{t('messages.messageConfirmYesNo')}</CModalBody>
        <CModalFooter>
          <CButton color="danger" onClick={handleToMoveLoanToBlacklist}>
            {t('common.Yes')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setBlacklistModal(false)
            }}
          >
            {t('common.No')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  return (
    <CRow className="mt-4">
      <CCol>
        <CRow className="mb-4">
          {renderButtons()}
          {renderEditButtons()}
        </CRow>
        <CRow>
          <CCol>
            <CTable>
              <CTableBody>
                {loanData.map((item, index) => {
                  return (
                    <CTableRow key={index}>
                      <CTableDataCell width="200px">
                        <b>{item.title}</b>
                      </CTableDataCell>
                      <CTableDataCell>{renderCell(item)}</CTableDataCell>
                    </CTableRow>
                  )
                })}
              </CTableBody>
            </CTable>
          </CCol>
        </CRow>
      </CCol>
      {renderPopupDisburse()}
      {renderPopupApproval()}
      {renderPopupDelete()}
      {renderPopupUndoApproval()}
      {renderPopupDeclineLoan()}
      {renderBlacklistModal()}
    </CRow>
  )
}

export default TabLoanInformation

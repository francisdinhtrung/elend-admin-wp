import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  CCol,
  CRow,
  CButton,
  CFormInput,
  CFormSelect,
  CSmartTable,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CModal,
  CFormTextarea,
  CDatePicker,
} from '@coreui/react-pro'
import { useDispatch, useSelector } from 'react-redux'
import CIcon from '@coreui/icons-react'
import { cilCheckCircle } from '@coreui/icons'
import { FaCheckCircle, FaUndo } from 'react-icons/fa'
import moment from 'moment'
import { loanActions } from '../../../loans/actions'
import { accountHelpers } from '../../../../utils/account-helper'
import { loanHelpers } from '../../../../utils/loan-helper'
import CurrencyInput from 'react-currency-input-field'

const TabTransactions = (props) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const { paramId, loan, transactions, repaymentSchedule, ...attributes } = props
  const paymentTypes = useSelector((state) => state.loans.paymentTypes)
  const methodRepayments = useSelector((state) => state.loans.methodRepayments)
  const account = accountHelpers.getAccount()
  if (paymentTypes && paymentTypes.length > 0) {
    paymentTypes.forEach((item) => {
      item.label = t(item.translateName)
      item.value = item.id
    })
  }
  if (methodRepayments && methodRepayments.length > 0) {
    methodRepayments.forEach((item) => {
      item.label = t(item.translateName)
      item.value = item.id
    })
  }
  const [details, setDetails] = useState([])
  const [isEnabledRepayment, setEnabledRepayment] = useState(false)
  const [isConfirmed, setConfirmation] = useState(false)
  const [isReversedPopup, setReversedPopup] = useState(false)
  const [isPayOffDebt, setPayOffDebt] = useState(false)
  const [isPayOffDebtEarly, setPayOffDebtEarly] = useState(false)
  const [selectedItem, setSelectedItem] = useState({})
  const [paidInstallment, setPaidInstallment] = useState(0)
  const [addPayment, setAddPayment] = useState({
    amountOfRepayment: '',
    methodOfRepayment: 'cash_deposit',
    paymentType: 'pay_fee',
    paymentDate: moment().format('yyyy-MM-DD'),
    notes: '',
  })
  const typePayOffDebt = loan.typePayOffDebt || ''
  let balance = 0
  if (transactions && transactions.length > 0) {
    transactions.forEach((item) => {
      if (!item.debit) {
        item.debit = 0
      }
      if (!item.credit) {
        item.credit = 0
      }
      item.effectiveDate = item.createdDate
      item.processingDate = item.submittedOn
      if (item.completedDate) {
        item.completedDate = item.completedDate
      } else {
        item.completedDate = ''
      }
      item.transactionType = item.name
      if (item.name === 'Disbursements') {
        balance = item.amount
      } else if (item.name === 'Interests Applied') {
        balance += item.amount
      } else {
        const totalPaid =
          item.principalRepaidDerived +
          item.interestRepaidDerived +
          item.penaltiesRepaidDerived +
          item.feesRepaidDerived
        if (loan.typePayOffDebt && loan.typePayOffDebt.indexOf('PAY_OFF_DEBT') > -1) {
          item.amount = totalPaid
          balance = 0
        } else {
          balance = balance - totalPaid
        }
      }
      item.balance = balance
    })
  }

  const data = transactions

  const handleToConfirmTransaction = () => {
    const payload = {
      id: selectedItem.id,
      loanId: selectedItem.loanId,
      createdByUserId: account.id,
    }
    dispatch(loanActions.confirmRepayment(payload))
  }

  const handleToPayOffDebt = () => {
    setPayOffDebt(false)
    const payload = {
      loanId: loan.id,
      createdByUserId: account.id,
      isPayOffEarly: false,
    }
    dispatch(loanActions.payoffDebtRepayment(payload))
  }

  const handleToPayOffDebtEarly = () => {
    setPayOffDebtEarly(false)
    const payload = {
      loanId: loan.id,
      createdByUserId: account.id,
      isPayOffEarly: true,
      paidInstallment: paidInstallment,
    }
    dispatch(loanActions.payoffDebtRepayment(payload))
  }

  const handleToAddRepayment = () => {
    if (addPayment.methodOfRepayment === 'cash_deposit') {
      addPayment.methodOfRepayment = methodRepayments.filter(
        (m) => m.description === 'cash_deposit',
      )[0].id
    }
    if (addPayment.paymentType === 'pay_fee') {
      addPayment.paymentType = paymentTypes.filter((m) => m.systemName === 'pay_fee')[0].id
    }
    const payload = {
      loanId: loan.id,
      createdByUserId: account.id,
      methodRepaymentId: addPayment.methodOfRepayment,
      paymentTypeId: addPayment.paymentType,
      amount: addPayment.amountOfRepayment,
      date: loanHelpers.dateFormatter(addPayment.paymentDate),
      notes: addPayment.notes,
      enablePenalty: loan.enableAutoPenalty,
    }
    dispatch(loanActions.createRepayment(payload))
  }

  const showAddRepayment = (status) => {
    setEnabledRepayment(status)
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    setAddPayment((addPayment) => ({ ...addPayment, [name]: value }))
  }

  const handleReverseTransaction = () => {
    dispatch(
      loanActions.reverseRepayment({
        loanId: selectedItem.loanId,
        repaymentId: selectedItem.id,
      }),
    )
  }

  useEffect(() => {
    dispatch(loanActions.getMethodRepayments())
    dispatch(loanActions.getPaymentTypes())
  }, [dispatch])

  const renderAction = (item) => {
    if (!item || item.loanTransactionTypeId === 1 || item.loanTransactionTypeId === 11) {
      return null
    }

    if (item.completedDate) {
      return <FaCheckCircle size="1.2em" color="#2eb85c" />
    }

    return (
      <div>
        <CButton
          color="primary"
          variant="outline"
          shape="square"
          size="sm"
          className="me-3"
          onClick={() => {
            setSelectedItem(item)
            setConfirmation(true)
          }}
        >
          {t('common.Confirm')}
        </CButton>
        <FaUndo
          className="icon-button"
          size="1.2em"
          color="#e55353"
          onClick={() => {
            setSelectedItem(item)
            setReversedPopup(true)
          }}
        />
      </div>
    )
  }

  const renderAddRepaymentModal = () => {
    return (
      <CModal size="lg" visible={isEnabledRepayment} onClose={() => showAddRepayment(false)}>
        <CModalHeader>
          <CModalTitle>{t('view.Loan.NewPayment')}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow className="mb-3">
            <CCol>
              <b>{t('view.Loan.AmountOfRepayment')}</b>
            </CCol>
            <CCol>
              <CurrencyInput
                intlConfig={{ locale: 'vi-VN', currency: 'VND' }}
                id="amountOfRepayment"
                name="amountOfRepayment"
                className="currency-input"
                defaultValue={addPayment.price}
                decimalsLimit={0}
                onValueChange={(value, name) => {
                  addPayment['amountOfRepayment'] = value
                  setAddPayment(addPayment)
                }}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol>
              <b>{t('view.Loan.MethodOfRepayment')}</b>
            </CCol>
            <CCol>
              <CFormSelect
                id="methodOfRepayment"
                name="methodOfRepayment"
                value={addPayment.methodOfRepayment}
                onChange={handleChange}
                options={methodRepayments}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol>
              <b>{t('view.Loan.PaymentType')}</b>
            </CCol>
            <CCol>
              <CFormSelect
                id="paymentType"
                name="paymentType"
                value={addPayment.paymentType}
                onChange={handleChange}
                options={paymentTypes}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol>
              <b>{t('view.Loan.PaymentDate')}</b>
            </CCol>
            <CCol>
              <CDatePicker
                id="paymentDate"
                name="paymentDate"
                date={addPayment.paymentDate}
                onDateChange={(date) => {
                  setAddPayment((addPayment) => ({ ...addPayment, paymentDate: date }))
                }}
              />
            </CCol>
          </CRow>
          <CRow className="mb-3">
            <CCol>
              <b>{t('common.Notes')}</b>
            </CCol>
            <CCol>
              <CFormTextarea
                id="notes"
                name="notes"
                rows="3"
                value={addPayment.notes}
                onChange={handleChange}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleToAddRepayment}>
            {t('common.Save')}
          </CButton>
          <CButton color="secondary" onClick={() => showAddRepayment(false)}>
            {t('common.Cancel')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const renderConfirmationModal = () => {
    return (
      <CModal
        visible={isConfirmed}
        onClose={() => {
          setConfirmation(false)
        }}
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.Confirm')}</CModalTitle>
        </CModalHeader>
        <CModalBody>{t('messages.messageConfirmYesNo')}</CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleToConfirmTransaction}>
            {t('common.Yes')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setConfirmation(false)
            }}
          >
            {t('common.No')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const renderReversedModal = () => {
    return (
      <CModal
        visible={isReversedPopup}
        onClose={() => {
          setReversedPopup(false)
        }}
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.Confirm')}</CModalTitle>
        </CModalHeader>
        <CModalBody>{t('messages.messageConfirmYesNo')}</CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleReverseTransaction}>
            {t('common.Yes')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setReversedPopup(false)
            }}
          >
            {t('common.No')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const getNextInstallment = () => {
    return loan && loan.summary ? loan.summary.payoffDebt.nextInstallment : 0
  }
  const getTotalInstallments = () => {
    return loan && loan.summary ? loan.summary.payoffDebt.totalInstalments : 0
  }

  const getPrincipalAmount = () => {
    return loan && loan.summary
      ? loan && loanHelpers.currencyFormatter(loan.summary.payoffDebt.principal)
      : 0
  }

  const getInterestAmount = () => {
    return loan && loan.summary
      ? loan && loanHelpers.currencyFormatter(loan.summary.payoffDebt.interest)
      : 0
  }

  const getPendingInstallments = () => {
    return loan && loan.summary ? loan && loan.summary.payoffDebt.remainingInstallments : 0
  }

  const getPendingInstallmentPayoffDebtEarly = () => {
    return loan && loan.summary ? loan && loan.summary.payoffDebtEarly.remainingInstallments : 0
  }

  const getTotalPayoffDebt = () => {
    return loan && loan.summary
      ? loan && loanHelpers.currencyFormatter(loan.summary.payoffDebt.totalPayoffDebt)
      : 0
  }

  const getTotalPayoffDebtEarly = () => {
    return loan && loan.summary
      ? loan && loanHelpers.currencyFormatter(loan.summary.payoffDebtEarly.totalPayoffDebt)
      : 0
  }

  const renderPayoffDebtModal = () => {
    return (
      <CModal
        visible={isPayOffDebt}
        onClose={() => {
          setPayOffDebt(false)
        }}
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('view.Borrowers.tabTransaction.PayOffDebt')}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xl={12}>
              <CRow>
                <CCol>
                  <span>{t('common.Installment')}:</span>
                </CCol>
                <CCol>
                  <span>
                    {getNextInstallment()}/{getTotalInstallments()}
                  </span>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <span>{t('common.Capital')}: </span>
                </CCol>
                <CCol>
                  <span>{getPrincipalAmount()}</span>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <span>{t('common.Interest')}: </span>
                </CCol>
                <CCol>
                  <span>{getInterestAmount()}</span>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <span>{t('common.TotalPendingInstallments')}: </span>
                </CCol>
                <CCol>
                  <span>{getPendingInstallments()}</span>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <span>
                    {t('view.Borrowers.tabTransaction.PayOffDebt')}: = {getPendingInstallments()}
                  </span>
                  <span> {t('common.Months')}</span>
                </CCol>
                <CCol>
                  <span>
                    <b>{getTotalPayoffDebt()}</b>
                  </span>
                </CCol>
              </CRow>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleToPayOffDebt}>
            {t('common.Yes')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setPayOffDebt(false)
            }}
          >
            {t('common.No')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const renderPayoffDebtEarlyModal = () => {
    return (
      <CModal
        visible={isPayOffDebtEarly}
        onClose={() => {
          setPayOffDebtEarly(false)
        }}
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('view.Borrowers.tabTransaction.PayOffDebtEarly')}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xl={12}>
              <CRow>
                <CCol>
                  <span>{t('common.Installment')}:</span>
                </CCol>
                <CCol>
                  <span>
                    {getNextInstallment()}/{getTotalInstallments()}
                  </span>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <span>{t('common.Capital')}: </span>
                </CCol>
                <CCol>
                  <span>{getPrincipalAmount()}</span>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <span>{t('common.Interest')}: </span>
                </CCol>
                <CCol>
                  <span>{getInterestAmount()}</span>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <span>{t('common.TotalPendingInstallments')}: </span>
                </CCol>
                <CCol>
                  <span>{getPendingInstallments()}</span>
                </CCol>
              </CRow>
              <CRow>
                <CCol>
                  <span>
                    {t('view.Borrowers.tabTransaction.PayOffDebtEarly')}: = (1{' '}
                    {t('common.FullMonth')} + {getPendingInstallmentPayoffDebtEarly()}{' '}
                    {t('common.Months')} thêm 8%)
                  </span>
                </CCol>
                <CCol>
                  <span>
                    <b>{getTotalPayoffDebtEarly()}</b>
                  </span>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol>
                  <b>{t('view.Borrowers.tabTransaction.PayOffDebtEarlyByPaidInstallment')}</b>
                </CCol>
                <CCol>
                  <CFormInput
                    type="number"
                    id="paidInstallment"
                    name="paidInstallment"
                    value={paidInstallment}
                    onChange={(e) => setPaidInstallment(e.target.value)}
                  />
                </CCol>
              </CRow>
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleToPayOffDebtEarly}>
            {t('common.Yes')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setPayOffDebtEarly(false)
            }}
          >
            {t('common.No')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const renderShowAddRepayment = () => {
    return typePayOffDebt.length === 0 ? (
      <CButton className="me-3" color="primary" onClick={() => showAddRepayment(true)}>
        {t('view.Borrowers.tabTransaction.AddRepayment')}
      </CButton>
    ) : null
  }

  const renderPayOffDebtButton = () => {
    return typePayOffDebt.length === 0 ? (
      <CButton
        className="me-3"
        color="success"
        onClick={() => {
          setPayOffDebt(true)
        }}
      >
        {t('view.Borrowers.tabTransaction.PayOffDebt')}
      </CButton>
    ) : null
  }

  const renderPayOffDebtEarlyButton = () => {
    return typePayOffDebt.length === 0 ? (
      <CButton
        className="me-3"
        color="success"
        onClick={() => {
          setPayOffDebtEarly(true)
        }}
      >
        {t('view.Borrowers.tabTransaction.PayOffDebtEarly')}
      </CButton>
    ) : null
  }

  const getTransactionType = (item) => {
    const value = 'common.' + item.transactionType.replace(/\s/g, '')
    return t(value)
  }

  return (
    <CRow className="mt-4">
      <div>
        {renderShowAddRepayment()}
        {renderPayOffDebtButton()}
        {renderPayOffDebtEarlyButton()}
      </div>
      <CCol className="table-responsive">
        <CSmartTable
          items={data}
          columns={[
            {
              key: 'id',
              _classes: 'font-weight-bold',
              label: t('view.Borrowers.tabTransaction.TransactionId'),
            },
            { key: 'effectiveDate', label: t('view.Borrowers.tabTransaction.EffectiveDate') },
            { key: 'processingDate', label: t('view.Borrowers.tabTransaction.ProcessingDate') },
            { key: 'completedDate', label: t('view.Borrowers.tabTransaction.CompletedDate') },
            { key: 'transactionType', label: t('view.Borrowers.tabTransaction.TransactionType') },
            { key: 'amount', label: t('view.Borrowers.tabTransaction.Amount') },
            { key: 'balance', label: t('view.Borrowers.tabTransaction.Balance') },
            { key: 'action', label: t('common.Action') },
          ]}
          itemsPerPageSelect
          itemsPerPage={20}
          activePage={1}
          pagination
          tableHeadProps={{
            color: 'primary',
          }}
          tableProps={{
            striped: true,
            hover: true,
          }}
          scopedColumns={{
            transactionType: (item) => <td>{getTransactionType(item)}</td>,
            amount: (item) => <td>{loanHelpers.currencyFormatter(item.amount)}</td>,
            balance: (item) => <td>{loanHelpers.currencyFormatter(item.balance)}</td>,
            action: (item) => {
              return <td>{renderAction(item)}</td>
            },
          }}
        />
      </CCol>
      {renderAddRepaymentModal()}
      {renderConfirmationModal()}
      {renderReversedModal()}
      {renderPayoffDebtModal()}
      {renderPayoffDebtEarlyModal()}
    </CRow>
  )
}

export default TabTransactions

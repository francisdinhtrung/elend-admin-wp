import React from 'react'
import { useTranslation } from 'react-i18next'
import { CCol, CRow, CSmartTable } from '@coreui/react-pro'

const TabGuarantors = (props) => {
  const { t } = useTranslation()
  const { paramId, loan, guarantors, ...attributes } = props

  return (
    <CRow className="mt-4">
      <CCol className="table-responsive">
        <CSmartTable
          items={guarantors}
          columns={[
            {
              key: 'fullName',
              _classes: 'font-weight-bold',
            },
            { key: 'numberPhone' },
          ]}
          itemsPerPageSelect
          itemsPerPage={5}
          activePage={1}
          pagination
          tableHeadProps={{
            color: 'primary',
          }}
          tableProps={{
            striped: true,
            hover: true,
          }}
        />
      </CCol>
    </CRow>
  )
}

export default TabGuarantors

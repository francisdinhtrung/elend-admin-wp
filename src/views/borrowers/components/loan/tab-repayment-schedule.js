import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { CCol, CRow, CSmartTable, CButton } from '@coreui/react-pro'
import Toggle from 'react-toggle'
import 'react-toggle/style.css'
import { loanActions } from '../../../loans/actions'
import { loanHelpers } from '../../../../utils/loan-helper'

const TabRepaymentSchedule = (props) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const { paramId, loan, repaymentSchedule, ...attributes } = props
  const [enableAutoPenalty, setEnableAutoPenalty] = useState(loan.enableAutoPenalty)
  if (repaymentSchedule) {
    repaymentSchedule.forEach((r) => {
      r.transaction = t(r.transaction)
    })
  }
  const data = repaymentSchedule || []
  if (data && data.length > 0) {
    const cellProp = { color: 'light', className: 'fw-semibold' }
    data[data.length - 1]._props = cellProp
  }

  const handleAutoPenaltyChange = (e) => {
    const isChecked = e.target.checked
    setEnableAutoPenalty(isChecked)
    loan.enableAutoPenalty = isChecked
    dispatch(loanActions.updateLoan(loan))
  }

  const handleSchedulePrint = () => {
    const heads = document.getElementsByTagName('head')
    const divProfile = document.getElementById('container-borrower-profile-id').innerHTML
    const divSchedule = document.getElementById('card-repayment-schedule-id').innerHTML
    const page = window.open('', '')
    page.document.write('<html>')
    page.document.write(heads[0].innerHTML)
    page.document.write(
      `<body> <h3 class="text-center">${t('view.Borrowers.RepaymentSchedule')}</h3>`,
    )
    page.document.write(divProfile)
    page.document.write(divSchedule)
    page.document.write('</body></html>')
    page.document.close()
    page.print()
  }

  return (
    <CRow className="mt-4">
      <CCol xl={12} className="flex-left-center mb-4">
        <div className="flex-left">
          <Toggle
            id="enable-auto-penalty"
            className="me-2"
            defaultChecked={enableAutoPenalty}
            onChange={handleAutoPenaltyChange}
          />
          <label htmlFor="enable-auto-penalty">{t('common.EnableAutoPenalty')}</label>
        </div>
        <div className="ms-4">
          <CButton onClick={handleSchedulePrint}>{t('common.PrintSchedule')}</CButton>
        </div>
      </CCol>
      <CCol id="card-repayment-schedule-id" xl={12} className="table-responsive">
        <CSmartTable
          items={data}
          columns={[
            {
              key: 'installment',
              label: '#',
            },
            { key: 'date', label: t('view.Borrowers.tabPaymentSchedule.Date') },
            { key: 'received', label: t('view.Borrowers.tabPaymentSchedule.Received') },
            { key: 'transaction', label: t('view.Borrowers.tabPaymentSchedule.Transaction') },
            { key: 'capital', label: t('view.Borrowers.tabPaymentSchedule.Capital') },
            { key: 'interest', label: t('view.Borrowers.tabPaymentSchedule.Interest') },
            { key: 'fees', label: t('view.Borrowers.tabPaymentSchedule.Fee') },
            { key: 'penalties', label: t('view.Borrowers.tabPaymentSchedule.Penalty') },
            { key: 'total', label: t('view.Borrowers.tabPaymentSchedule.Total') },
            { key: 'paid', label: t('view.Borrowers.tabPaymentSchedule.Paid') },
            { key: 'pending', label: t('view.Borrowers.tabPaymentSchedule.Pending') },
            { key: 'balance', label: t('view.Borrowers.tabPaymentSchedule.Balance') },
          ]}
          tableHeadProps={{
            color: 'primary',
          }}
          tableProps={{
            hover: true,
          }}
          scopedColumns={{
            capital: (item) => (
              <td className="text-right">{loanHelpers.currencyFormatter(item.capital)}</td>
            ),
            interest: (item) => (
              <td className="text-right">{loanHelpers.currencyFormatter(item.interest)}</td>
            ),
            fees: (item) => (
              <td className="text-right">{loanHelpers.currencyFormatter(item.fees)}</td>
            ),
            penalties: (item) => (
              <td className="text-right">{loanHelpers.currencyFormatter(item.penalties)}</td>
            ),
            total: (item) => (
              <td className="text-right">{loanHelpers.currencyFormatter(item.total)}</td>
            ),
            paid: (item) => (
              <td className="text-right">{loanHelpers.currencyFormatter(item.paid)}</td>
            ),
            pending: (item) => (
              <td className="text-right font-weight-bold">
                {loanHelpers.currencyFormatter(item.pending)}
              </td>
            ),
            balance: (item) => (
              <td className="text-right">{loanHelpers.currencyFormatter(item.balance)}</td>
            ),
          }}
        />
      </CCol>
    </CRow>
  )
}

export default TabRepaymentSchedule

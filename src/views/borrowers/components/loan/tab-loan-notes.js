import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import {
  CButton,
  CCol,
  CFormTextarea,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from '@coreui/react-pro'
import { FaEdit, FaTrash } from 'react-icons/fa'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loanActions } from '../../../loans/actions'
import moment from 'moment'
import { accountHelpers } from '../../../../utils/account-helper'

const TabLoanNotes = (props) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const loanNotes = useSelector((state) => state.loans.loanNotes)
  const [isAddNote, setIsAddNote] = useState(false)
  const [danger, setDanger] = useState(false)
  const [selectedNote, setSelectedNote] = useState({
    description: '',
  })
  const { paramId, loan, ...attributes } = props
  const account = accountHelpers.getAccount()

  const renderNoteItem = (item) => {
    return (
      <CRow key={item.id} className="note-item mb-3">
        <CCol xl={12}>
          <span className="me-2 font-weight-bold">{item.fullName}</span>
          <span>{moment(item.createdDate).format('DD-MM-YYYY HH:MM:SS')}</span>
        </CCol>
        <CCol xl={12}>
          <span>{item.description}</span>
        </CCol>
        <CCol xl={12} className="flex-left mt-3 mb-3">
          <div
            className="icon-btn me-3"
            onClick={(e) => {
              setSelectedNote(item)
              setIsAddNote(true)
            }}
          >
            <FaEdit />
          </div>
          <div
            className="icon-btn"
            onClick={() => {
              setSelectedNote(item)
              setDanger(true)
            }}
          >
            <FaTrash />
          </div>
        </CCol>
      </CRow>
    )
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    setSelectedNote((selectedNote) => ({ ...selectedNote, [name]: value }))
  }

  const handleToAddNote = () => {
    const payload = {
      createdById: account.id,
      loanId: loan.id,
      description: selectedNote.description,
    }
    if (selectedNote.id) {
      payload.id = selectedNote.id
      dispatch(loanActions.updateLoanNote(payload))
    } else {
      dispatch(loanActions.createLoanNote(payload))
    }
    setIsAddNote(false)
  }

  const handleDeleteLoanNote = () => {
    const payload = {
      loanId: selectedNote.loanId,
      loanNoteId: selectedNote.id,
    }
    dispatch(loanActions.deleteLoanNote(payload))
    setDanger(false)
  }

  const renderAddNoteModal = () => {
    return (
      <CModal
        visible={isAddNote}
        onClose={() => {
          setIsAddNote(false)
        }}
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.Notes')}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CRow>
            <CCol xl={12} className="mb-2">
              <b>{t('common.Description')}</b>
            </CCol>
            <CCol xl={12}>
              <CFormTextarea
                id="description"
                name="description"
                rows="3"
                value={selectedNote.description}
                onChange={handleChange}
              />
            </CCol>
          </CRow>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={handleToAddNote}>
            {t('common.Save')}
          </CButton>
          <CButton
            color="secondary"
            onClick={() => {
              setIsAddNote(false)
            }}
          >
            {t('common.Cancel')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const renderPopupDelete = () => {
    return (
      <CModal
        visible={danger}
        onClose={() => {
          setDanger(false)
        }}
        color="danger"
      >
        <CModalHeader closeButton>
          <CModalTitle>{t('common.ConfirmDelete')}</CModalTitle>
        </CModalHeader>
        <CModalBody>{t('messages.messageConfirmDelete')}</CModalBody>
        <CModalFooter>
          <CButton color="danger" onClick={handleDeleteLoanNote}>
            {t('common.Delete')}
          </CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {
              setDanger(false)
            }}
          >
            {t('common.Cancel')}
          </CButton>
        </CModalFooter>
      </CModal>
    )
  }

  const getLoanNotes = () => {
    if (!loan.id) return
    dispatch(loanActions.getLoanNotes({ loanId: loan.id }))
  }

  useEffect(() => {
    const loadData = async () => {
      await getLoanNotes()
    }
    loadData()
  }, [dispatch])

  return (
    <CRow className="mt-4">
      <CCol xl={10}>
        {loanNotes &&
          loanNotes.length > 0 &&
          loanNotes.map((note) => {
            return renderNoteItem(note)
          })}
      </CCol>
      <CCol>
        <CButton color="primary" onClick={() => setIsAddNote(true)}>
          {t('common.AddNote')}
        </CButton>
      </CCol>
      {renderAddNoteModal()}
      {renderPopupDelete()}
    </CRow>
  )
}

export default TabLoanNotes

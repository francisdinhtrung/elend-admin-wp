import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CSmartTable,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CModal,
} from '@coreui/react-pro'
import { borrowerActions } from '../actions'
import { colorHelpers } from '../../../utils/color-helper'

const Borrowers = () => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const itemsPerPage = useSelector((state) => state.borrowers.itemsPerPage)
  const usersData = useSelector((state) => state.borrowers.borrowers)
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)
  const [details, setDetails] = useState([])
  const [isBlacklist, setIsBlacklist] = useState(false)
  const [selectedBorrower, setSelectedBorrower] = useState(null)

  const getAllUsers = async () => {
    dispatch(
      borrowerActions.getAllBorrowers({
        page: currentPage > 1 ? currentPage - 1 : 0,
        size: itemsPerPage,
        status: 'BANNED',
      }),
    )
    setPage(currentPage)
  }

  const getBadge = (status) => {
    if (!status) return
    return colorHelpers.getColorByStatus(status.toLowerCase(), false)
  }

  useEffect(() => {
    const loadData = async () => {
      await getAllUsers()
    }
    loadData()
  }, [dispatch, currentPage, page])

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  const handleMoveOutBlackList = () => {
    dispatch(borrowerActions.moveBorrowerOutBlacklist(selectedBorrower))
  }

  return (
    <CRow>
      <CCol>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm="5">
                <h4 id="traffic" className="card-title mb-0">
                  {t('theHeader.BorrowersBlacklist')}
                </h4>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody className="table-responsive">
            <CSmartTable
              items={usersData}
              columns={[
                {
                  key: 'fullName',
                  _classes: 'font-weight-bold',
                  label: t('view.Borrowers.FullName'),
                },
                { key: 'gender', label: t('view.Borrowers.Gender') },
                { key: 'idNumber', label: t('view.Borrowers.IDNumber') },
                { key: 'phoneNumber', label: t('view.Borrowers.PhoneNumber') },
                { key: 'status', label: t('common.Status') },
                { key: 'action', label: t('common.Action') },
              ]}
              tableFilter
              itemsPerPageSelect
              itemsPerPage={itemsPerPage}
              activePage={currentPage - 1}
              tableProps={{
                striped: true,
                hover: true,
              }}
              scopedColumns={{
                action: (item) => {
                  return (
                    <td>
                      <CButton
                        color="warning"
                        shape="square"
                        size="sm"
                        onClick={() => {
                          setIsBlacklist(true)
                          setSelectedBorrower(item)
                        }}
                      >
                        Undo Blacklist
                      </CButton>
                    </td>
                  )
                },
                status: (item) => (
                  <td>
                    <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                  </td>
                ),
              }}
            />
            <CModal
              visible={isBlacklist}
              onClose={() => {
                setIsBlacklist(false)
                setSelectedBorrower(null)
              }}
              color="warning"
            >
              <CModalHeader closeButton>
                <CModalTitle>{t('common.UndoBlacklist')}</CModalTitle>
              </CModalHeader>
              <CModalBody>{t('messages.messageConfirmUndoBlacklist')}</CModalBody>
              <CModalFooter>
                <CButton color="warning" onClick={handleMoveOutBlackList}>
                  {t('common.UndoBlacklist')}
                </CButton>{' '}
                <CButton
                  color="secondary"
                  onClick={() => {
                    setIsBlacklist(false)
                    setSelectedBorrower(null)
                  }}
                >
                  {t('common.Cancel')}
                </CButton>
              </CModalFooter>
            </CModal>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Borrowers

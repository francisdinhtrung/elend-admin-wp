import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CCollapse,
  CSmartTable,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CModal,
  CSmartPagination,
  CLink,
} from '@coreui/react-pro'
import { borrowerActions } from '../actions'
import { FaUserPlus } from 'react-icons/fa'
import { colorHelpers } from '../../../utils/color-helper'

const Borrowers = ({ match }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const itemsPerPage = useSelector((state) => state.borrowers.itemsPerPage)
  const usersData = useSelector((state) => state.borrowers.borrowers)
  const totalPages = useSelector((state) => state.borrowers.totalPages)
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)
  const [details, setDetails] = useState([])
  const [isBlacklist, setIsBlacklist] = useState(false)
  const [danger, setDanger] = useState(false)
  const [selectedBorrower, setSelectedBorrower] = useState(null)

  let loanStatus = ''
  const lastIndexOf = match.url.lastIndexOf('/')
  if (lastIndexOf > 0) {
    loanStatus = match.url.substring(lastIndexOf + 1, match.url.length)
  }

  const pageChange = (newPage) => {
    let url = `/borrowers?page=${newPage === 0 ? 1 : newPage}`
    if (loanStatus && loanStatus.length > 0) {
      url = `/borrowers/${loanStatus.toLowerCase()}?page=${newPage === 0 ? 1 : newPage}`
    }
    currentPage !== newPage && history.push(url)
  }

  const getAllUsers = async () => {
    const payload = {
      page: currentPage > 1 ? currentPage - 1 : 0,
      size: itemsPerPage,
      status: 'ACTIVE',
    }
    if (loanStatus && loanStatus.indexOf('loans') > -1) {
      if (loanStatus === 'active-loans') {
        payload.loanStatus = 'active'
      } else if (loanStatus === 'completed-loans') {
        payload.loanStatus = 'completed'
      }
    }
    dispatch(borrowerActions.getAllBorrowers(payload))
    setPage(currentPage)
  }

  const getBadge = (status) => {
    if (!status) return
    return colorHelpers.getColorByStatus(status.toLowerCase(), false)
  }

  useEffect(() => {
    const loadData = async () => {
      await getAllUsers()
    }
    loadData()
  }, [dispatch, currentPage, page])

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  const handleToView = (item, index, e) => {
    if (e === 'action') return
    history.push(`/borrowers/${item.id}`)
  }

  const handleToEdit = (item, index, e) => {
    if (e === 'action') return
    history.push(`/borrowers/${item.id}/edit`)
  }

  const navigationToUserCreation = () => {
    history.push(`/borrowers/create`)
  }

  const handleMoveToBlackList = () => {
    dispatch(borrowerActions.moveBorrowerToBlacklist(selectedBorrower))
  }

  const handleDeleteBorrower = async () => {
    setDanger(!danger)
    dispatch(borrowerActions.deleteBorrower(selectedBorrower.id))
  }

  const renderBorrowerFullNameLink = (item) => {
    return item && item.id ? (
      <CLink className="text-decoration-none" href={`#/borrowers/${item.id}`}>
        {item.fullName}
      </CLink>
    ) : null
  }

  return (
    <CRow>
      <CCol>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm="5">
                <h4 id="traffic" className="card-title mb-0">
                  {t('theHeader.Borrowers')}
                </h4>
              </CCol>
              <CCol sm="7" className="d-none d-md-block">
                <CButton color="primary" className="float-end" onClick={navigationToUserCreation}>
                  <FaUserPlus />
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody className="table-responsive">
            <CSmartTable
              items={usersData}
              columns={[
                {
                  key: 'fullName',
                  _classes: 'font-weight-bold',
                  label: t('view.Borrowers.FullName'),
                },
                { key: 'gender', label: t('view.Borrowers.Gender') },
                { key: 'idNumber', label: t('view.Borrowers.IDNumber') },
                { key: 'phoneNumber', label: t('view.Borrowers.PhoneNumber') },
                { key: 'status', label: t('common.Status') },
                { key: 'action', label: t('common.Action') },
              ]}
              tableFilter
              itemsPerPageSelect
              itemsPerPage={itemsPerPage}
              activePage={currentPage - 1}
              pagination
              tableProps={{
                striped: true,
                hover: true,
              }}
              scopedColumns={{
                fullName: (item) => {
                  return <td>{renderBorrowerFullNameLink(item)}</td>
                },
                action: (item) => {
                  return (
                    <td>
                      <CButton
                        color="primary"
                        variant="outline"
                        shape="square"
                        size="sm"
                        onClick={() => {
                          toggleDetails(item.id)
                        }}
                      >
                        {details.includes(item.id) ? 'Hide' : 'Show'}
                      </CButton>
                    </td>
                  )
                },
                status: (item) => (
                  <td>
                    <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                  </td>
                ),
                details: (item) => {
                  return (
                    <CCollapse visible={details.includes(item.id)}>
                      <CCardBody>
                        <CButton
                          size="sm"
                          color="info"
                          className="me-4"
                          onClick={() => handleToView(item)}
                        >
                          {t('common.View')}
                        </CButton>
                        <CButton
                          size="sm"
                          color="info"
                          className="me-4"
                          onClick={() => handleToEdit(item)}
                        >
                          {t('common.Edit')}
                        </CButton>
                        <CButton
                          size="sm"
                          color="warning"
                          className="me-4"
                          onClick={() => {
                            setIsBlacklist(true)
                            setSelectedBorrower(item)
                          }}
                        >
                          {t('common.MoveToBlacklist')}
                        </CButton>
                        <CButton
                          size="sm"
                          color="danger"
                          className="me-4"
                          onClick={() => {
                            setDanger(true)
                            setSelectedBorrower(item)
                          }}
                        >
                          {t('common.Delete')}
                        </CButton>
                      </CCardBody>
                    </CCollapse>
                  )
                },
              }}
            />
            <CSmartPagination
              activePage={page}
              onActivePageChange={pageChange}
              pages={totalPages}
              doubleArrows={false}
              align="center"
            />
            <CModal
              visible={isBlacklist}
              onClose={() => {
                setIsBlacklist(false)
                setSelectedBorrower(null)
              }}
              color="warning"
            >
              <CModalHeader closeButton>
                <CModalTitle>{t('common.ConfirmBlacklist')}</CModalTitle>
              </CModalHeader>
              <CModalBody>{t('messages.messageConfirmBlacklist')}</CModalBody>
              <CModalFooter>
                <CButton color="warning" onClick={handleMoveToBlackList}>
                  {t('common.Blacklist')}
                </CButton>{' '}
                <CButton
                  color="secondary"
                  onClick={() => {
                    setIsBlacklist(false)
                    setSelectedBorrower(null)
                  }}
                >
                  {t('common.Cancel')}
                </CButton>
              </CModalFooter>
            </CModal>
            <CModal
              visible={danger}
              onClose={() => {
                setDanger(false)
                setSelectedBorrower(null)
              }}
              color="danger"
            >
              <CModalHeader closeButton>
                <CModalTitle>{t('common.ConfirmDelete')}</CModalTitle>
              </CModalHeader>
              <CModalBody>{t('messages.messageConfirmDelete')}</CModalBody>
              <CModalFooter>
                <CButton color="danger" onClick={handleDeleteBorrower}>
                  {t('common.Delete')}
                </CButton>{' '}
                <CButton
                  color="secondary"
                  onClick={() => {
                    setDanger(false)
                    setSelectedBorrower(null)
                  }}
                >
                  {t('common.Cancel')}
                </CButton>
              </CModalFooter>
            </CModal>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Borrowers

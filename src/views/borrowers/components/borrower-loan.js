import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  CCard,
  CCardBody,
  CCol,
  CRow,
  CNavItem,
  CNav,
  CNavLink,
  CTabContent,
  CTabPane,
} from '@coreui/react-pro'
import Avatar from 'react-avatar'
import { useDispatch, useSelector } from 'react-redux'
import TabLoanInformation from './loan/tab-loan-information'
import TabSummary from './loan/tab-summary'
import TabTransactions from './loan/tab-transactions'
import TabRepaymentSchedule from './loan/tab-repayment-schedule'
import TabGuarantors from './loan/tab-guarantors'
import TabLoanNotes from './loan/tab-loan-notes'
import { loanActions } from '../../loans/actions'

const BorrowerLoan = ({ match }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const loanData = useSelector((state) => state.loans.loan)
  const paramId = match.params.id

  const [activeKey, setActiveKey] = useState(1)
  const tabs = {
    INFORMATION: 1,
    SUMMARY: 2,
    TRANSACTIONS: 3,
    REPAYMENT_SCHEDULE: 4,
    GUARANTEE: 5,
    NOTES: 6,
  }
  const tabItems = [
    {
      index: tabs.INFORMATION,
      text: t('view.Borrowers.Information'),
      visible: true,
    },
    {
      index: tabs.SUMMARY,
      text: t('view.Borrowers.Summary'),
      visible: false,
    },
    {
      index: tabs.TRANSACTIONS,
      text: t('view.Borrowers.Transactions'),
      visible: false,
    },
    {
      index: tabs.REPAYMENT_SCHEDULE,
      text: t('view.Borrowers.RepaymentSchedule'),
      visible: false,
    },
    {
      index: tabs.GUARANTEE,
      text: t('view.Borrowers.Guarantee'),
      visible: false,
    },
    {
      index: tabs.NOTES,
      text: t('common.Notes'),
      visible: false,
    },
  ]

  const handleTabSelected = (tabIndex) => {
    setActiveKey(tabIndex)
  }

  const getId = () => {
    if (!loanData) return ''
    return `#${loanData.userDto.id.substr(0, 8)}`
  }

  const getFullName = () => {
    if (!loanData) return ''
    return `${loanData.userDto.lastName} ${loanData.userDto.firstName}`
  }

  const getGender = () => {
    if (!loanData) return ''
    return loanData.userProfileDto.gender
  }

  const getBirthDate = () => {
    if (!loanData) return ''
    return loanData.userProfileDto.birthDate
  }

  const getIdentityNumber = () => {
    if (!loanData) return ''
    return loanData.borrowerDto.identifyNumber
  }

  const getCompany = () => {
    if (!loanData) return ''
    return loanData.borrowerDto.company
  }

  const getMobilePhone = () => {
    if (!loanData) return ''
    return loanData.userProfileDto.phone
  }
  const getMobilePhoneLink = () => {
    if (!loanData) return '#'
    return 'tel:' + loanData.userProfileDto.phone
  }

  const getEmail = () => {
    if (!loanData) return ''
    return loanData.userDto.email
  }

  const getEmailLink = () => {
    if (!loanData) return '#'
    return 'mailto:' + loanData.userDto.email
  }

  const getAddress = () => {
    if (!loanData || !loanData.userAddressList || loanData.userAddressList.length === 0) return ''
    return loanData.userAddressList.filter((a) => a.isPrimary)[0].addressLine1
  }

  useEffect(() => {
    dispatch(loanActions.getLoanById(paramId))
  }, [dispatch])

  const renderProfile = (item) => {
    return (
      <CCard className="card-avatar">
        <CCardBody>
          <CRow>
            <CCol>
              <CRow>
                <CCol xl={2}>
                  <Avatar name={getFullName()} size="120px" />
                </CCol>
                <CCol xl={3}>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('view.Borrowers.Profile.ID')}</b>
                    </CCol>
                    <CCol>{getId()}</CCol>
                  </CRow>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('view.Borrowers.Profile.FullName')}</b>
                    </CCol>
                    <CCol>{getFullName()}</CCol>
                  </CRow>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('view.User.Gender')}</b>
                    </CCol>
                    <CCol>{getGender()}</CCol>
                  </CRow>
                </CCol>
                <CCol xl={3}>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('view.User.DateOfBirth')}</b>
                    </CCol>
                    <CCol>{getBirthDate()}</CCol>
                  </CRow>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('view.User.IdentificationNumber')}</b>
                    </CCol>
                    <CCol>{getIdentityNumber()}</CCol>
                  </CRow>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('view.User.Company')}</b>
                    </CCol>
                    <CCol>{getCompany()}</CCol>
                  </CRow>
                </CCol>
                <CCol xl={3}>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('view.User.MobileNumber')}</b>
                    </CCol>
                    <CCol>
                      <a href={getMobilePhoneLink()}>{getMobilePhone()}</a>
                    </CCol>
                  </CRow>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('view.User.Email')}</b>
                    </CCol>
                    <CCol>
                      <a href={getEmailLink()}>{getEmail()}</a>
                    </CCol>
                  </CRow>
                  <CRow className="mb-4">
                    <CCol>
                      <b>{t('common.Address')}</b>
                    </CCol>
                    <CCol>{getAddress()}</CCol>
                  </CRow>
                </CCol>
              </CRow>
            </CCol>
          </CRow>
        </CCardBody>
      </CCard>
    )
  }

  const renderLoanTabs = (data) => {
    const statusList = ['PENDING', 'SUBMITTED', 'APPROVED', 'REJECTED']
    return (
      <CNav variant="tabs">
        {tabItems.map((item) => {
          if (data && item.index > 1) {
            item.visible = statusList.indexOf(data.loansDTO.status) === -1
          }
          return item.visible ? (
            <CNavItem key={item.index}>
              <CNavLink
                active={activeKey === item.index}
                onClick={() => handleTabSelected(item.index)}
              >
                {item.text}
              </CNavLink>
            </CNavItem>
          ) : null
        })}
      </CNav>
    )
  }

  const renderSummaryTab = (data) => {
    if (!data.loanRepaymentSchedules || data.loanRepaymentSchedules.length === 0) return null
    return (
      <CTabPane visible={activeKey === tabs.SUMMARY}>
        <TabSummary
          loan={data.loansDTO}
          repaymentSchedule={data.loanRepaymentSchedules}
          transactions={data.loanTransactions}
        />
      </CTabPane>
    )
  }

  const renderTransactions = (data) => {
    if (!data.loanRepaymentSchedules || data.loanRepaymentSchedules.length === 0) return null
    return (
      <CTabPane visible={activeKey === tabs.TRANSACTIONS}>
        <TabTransactions
          loan={data.loansDTO}
          transactions={data.loanTransactions}
          repaymentSchedule={data.loanRepaymentSchedules}
        />
      </CTabPane>
    )
  }

  const renderRepaymentSchedules = (data) => {
    if (!data.loanRepaymentSchedules || data.loanRepaymentSchedules.length === 0) return null
    return (
      <CTabPane visible={activeKey === tabs.REPAYMENT_SCHEDULE}>
        <TabRepaymentSchedule
          loan={data.loansDTO}
          repaymentSchedule={data.loanRepaymentSchedules}
        />
      </CTabPane>
    )
  }

  const renderLoanContents = (data) => {
    return (
      <CTabContent>
        <CTabPane visible={activeKey === tabs.INFORMATION}>
          <TabLoanInformation loan={data.loansDTO} />
        </CTabPane>
        {renderSummaryTab(data)}
        {renderTransactions(data)}
        {renderRepaymentSchedules(data)}
        <CTabPane visible={activeKey === tabs.GUARANTEE}>
          <TabGuarantors guarantors={data.borrowerDto.guarantors} />
        </CTabPane>
        <CTabPane visible={activeKey === tabs.NOTES}>
          <TabLoanNotes loan={data.loansDTO} />
        </CTabPane>
      </CTabContent>
    )
  }

  return (
    <CRow>
      <CCol xl={12} className="mb-4" id="container-borrower-profile-id">
        {renderProfile()}
      </CCol>
      <CCol xl={12}>
        <CCard className="card-borrower-loan">
          <CCardBody>
            <CRow>
              <CCol>
                {renderLoanTabs(loanData)}
                {loanData ? renderLoanContents(loanData) : null}
              </CCol>
            </CRow>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default BorrowerLoan

import React, { useState, useEffect } from 'react'
import { Redirect, useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CButton,
  CFormLabel,
  CFormInput,
  CForm,
  CFormFeedback,
  CInputGroup,
  CInputGroupText,
  CFormSelect,
  CButtonGroup,
  CDatePicker,
} from '@coreui/react-pro'
import { FaAddressCard, FaCheckCircle, FaRegBuilding } from 'react-icons/fa'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import CIcon from '@coreui/icons-react'
import {
  cilPeople,
  cilPhone,
  cilUser,
  cilBuilding,
  cilLocationPin,
  cilLan,
  cilEnvelopeOpen,
  cilCalendar,
} from '@coreui/icons'
import api from '../../../utils/api'
import { toast } from 'react-toastify'
import { borrowerActions } from '../actions'
import { organizationActions } from '../../organization/actions'
import { accountHelpers } from '../../../utils/account-helper'

const BorrowerCreation = ({ match }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const isCreated = useSelector((state) => state.borrowers.isCreated)
  const organizations = useSelector((state) => state.organizations.organizations)
  const editUserData = useSelector((state) => state.borrowers.borrower)
  const [idFrontFaceURL, setIdFrontFaceURL] = useState(
    `${process.env.PUBLIC_URL}/images/id-front.png`,
  )
  const [fileIdFrontFace, setFileIdFrontFace] = useState([])
  const [idBackFaceURL, setIdBackFaceURL] = useState(`${process.env.PUBLIC_URL}/images/id-back.png`)
  const [fileIdBackFace, setFileIdBackFace] = useState([])
  const [errors, setErrors] = useState({})
  const myOrg = accountHelpers.getMyOrganization()
  let paramId = match.params.id

  const genders = ['Male', 'Female', 'Unknown']
  const constGenders = {
    MALE: 'Male',
    FEMALE: 'Female',
    UNKNOWN: 'Unknown',
  }
  const schema = Yup.object({
    gender: Yup.string()
      .required(t('messages.validations.genderRequired'))
      .oneOf(genders, t('messages.validations.genderRequired')),
    firstName: Yup.string().required(t('messages.validations.firstNameRequired')),
    lastName: Yup.string().required(t('messages.validations.lastNameRequired')),
    mobilePhone: Yup.string().required(t('messages.validations.mobilePhoneRequired')),
    permanentAddress: Yup.string().required(t('messages.validations.permanentAddressRequired')),
    currentAddress: Yup.string().required(t('messages.validations.currentAddressRequired')),
    identificationNumber: Yup.string().required(
      t('messages.validations.identificationNumberRequired'),
    ),
    idFrontFaceURL: Yup.string().required(t('messages.validations.attachedFileRequired')),
    idBackFaceURL: Yup.string().required(t('messages.validations.attachedFileRequired')),
    company: Yup.string().required(t('messages.validations.companyRequired')),
    yearOfService: Yup.string().required(t('messages.validations.yearsOfServiceRequired')),
    // guarantors: Yup.array().of(
    //   Yup.object().shape({
    //     fullName: Yup.string().required(t('messages.validations.fullNameRequired')),
    //     numberPhone: Yup.string().required(t('messages.validations.mobilePhoneRequired')),
    //   }),
    // ),
  })

  const getPermanentAddress = (data) => {
    if (!data || !data.userAddressList || data.userAddressList.length === 0) return ''
    const permanentAddressList = data.userAddressList.filter((address) => !address.isPrimary)
    return permanentAddressList && permanentAddressList.length
      ? permanentAddressList[0].addressLine1
      : ''
  }

  const getCurrentAddress = (data) => {
    if (!data || !data.userAddressList || data.userAddressList.length === 0) return ''
    const currentAddressList = data.userAddressList.filter((address) => address.isPrimary)
    return currentAddressList && currentAddressList.length ? currentAddressList[0].addressLine1 : ''
  }

  const getAttachedIdentity = (data, type) => {
    if (!data || !data.userMetadataList || data.userMetadataList.length === 0) return ''
    const item = data.userMetadataList.filter((item) => item.imageType === type)[0]
    if (item) {
      return item.thumbUrl
    }
    return ''
  }

  const formik = useFormik({
    initialValues: {
      selectedOrganizationId: editUserData ? editUserData.organizationId : '',
      firstName: editUserData ? editUserData.firstName : '',
      lastName: editUserData ? editUserData.lastName : '',
      gender: editUserData ? editUserData.gender : '',
      email: editUserData ? editUserData.email : '',
      mobilePhone: editUserData ? editUserData.phone : '',
      birthDate: editUserData ? editUserData.birthDate : '',
      permanentAddress: getPermanentAddress(editUserData),
      currentAddress: getCurrentAddress(editUserData),
      city: '',
      country: 'Vietnam', // default values
      zipCode: '',
      identificationNumber: editUserData ? editUserData.identifyNumber : '',
      idFrontFaceURL: getAttachedIdentity(editUserData, 'identity_front'),
      filename1: '',
      filename2: '',
      idBackFaceURL: getAttachedIdentity(editUserData, 'identity_back'),
      guarantors: [
        {
          title: 'GuarantorIDetail',
          fullName:
            editUserData && editUserData.guarantors.length > 0
              ? editUserData.guarantors[0].fullName
              : '',
          numberPhone:
            editUserData && editUserData.guarantors.length > 0
              ? editUserData.guarantors[0].numberPhone
              : '',
          address:
            editUserData && editUserData.guarantors.length > 0
              ? editUserData.guarantors[0].address
              : '',
          relationship: '',
          translateHeader: 'view.Borrowers.GuarantorIDetail',
          translateName: 'view.Borrowers.FullName',
        },
        {
          title: 'GuarantorIIDetail',
          fullName:
            editUserData && editUserData.guarantors.length > 1
              ? editUserData.guarantors[1].fullName
              : '',
          numberPhone:
            editUserData && editUserData.guarantors.length > 1
              ? editUserData.guarantors[1].numberPhone
              : '',
          address:
            editUserData && editUserData.guarantors.length > 1
              ? editUserData.guarantors[1].address
              : '',
          relationship:
            editUserData && editUserData.guarantors.length > 1
              ? editUserData.guarantors[1].relationship
              : '',
          translateHeader: 'view.Borrowers.GuarantorIIDetail',
          translateName: 'view.Borrowers.FullName',
        },
      ],
      company: editUserData ? editUserData.company : '',
      yearOfService: editUserData ? editUserData.yearOfService : '',
    },
    enableReinitialize: true,
    validationSchema: schema,
  })

  const getAllOrganizations = async () => {
    dispatch(
      organizationActions.getOrganizationList({
        all: true,
      }),
    )
  }

  useEffect(() => {
    const loadData = async () => {
      if (!myOrg || !myOrg.organizationId) {
        await getAllOrganizations()
      }
      if (paramId) {
        dispatch(borrowerActions.getBorrowerById(paramId))
      } else {
        const guarantors = [...formik.values.guarantors]
        guarantors.forEach((g) => {
          g.address = ''
          g.fullName = ''
          g.numberPhone = ''
          g.relationship = ''
          delete g.id
        })
        formik.setFieldValue('guarantors', guarantors)
        dispatch(borrowerActions.resetBorrower())
      }
    }
    loadData()
  }, [dispatch])

  const handleToUploadAttachedFiles = (typeOfUploadFile, e) => {
    const isFront = typeOfUploadFile === 'front'
    const file = e.target.files[0]

    api.cheveretoService
      .uploadImage(file)
      .then((response) => {
        return response.json()
      })
      .then((result) => {
        if (result.status_code === 200) {
          toast.success(`Upload the image successfully.`)
          if (isFront) {
            formik.values.filename1 = result.image.filename
            formik.values.idFrontFaceURL = result.image.url
          } else {
            formik.values.filename2 = result.image.filename
            formik.values.idBackFaceURL = result.image.url
          }
        } else {
          toast.error(result.error.message)
        }
      })
      .catch((error) => {
        console.error('error=', error)
      })
  }

  const onUploadFileIdFrontFaceChange = (event) => {
    const file = event.target.files[0]
    setFileIdFrontFace(event.target.files)
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = (e) => {
      setIdFrontFaceURL(reader.result)
      handleToUploadAttachedFiles('front', event)
    }
  }

  const onUploadFileIdBackFaceChange = (event) => {
    const file = event.target.files[0]
    setFileIdBackFace(event.target.files)
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = (e) => {
      setIdBackFaceURL(reader.result)
      handleToUploadAttachedFiles('back', event)
    }
  }

  const inputChangedHandler = (event, index, inputType) => {
    const valInput = event.target.value
    switch (inputType) {
      case 'fullName':
        formik.values.guarantors[index].fullName = valInput
        break
      case 'relationship':
        formik.values.guarantors[index].relationship = valInput
        break
      case 'phone':
        formik.values.guarantors[index].numberPhone = valInput
        break
      case 'address':
        formik.values.guarantors[index].address = valInput
        break
      default:
        break
    }
  }
  const generatePayLoadUserAddress = (data, organizationId) => {
    const userAddresses = []
    userAddresses.push({
      addressLine1: data.permanentAddress,
      organizationId: organizationId,
      isPrimary: false,
    })
    userAddresses.push({
      addressLine1: data.currentAddress,
      organizationId: organizationId,
      isPrimary: true,
    })
    return userAddresses
  }

  const generatePayLoadUserMetadata = (data) => {
    const userMetadata = []
    userMetadata.push({
      thumbUrl: data.idFrontFaceURL,
      fileName: data.filename1 || '',
      imageType: 'identity_front',
    })
    userMetadata.push({
      thumbUrl: data.idBackFaceURL,
      fileName: data.filename2 || '',
      imageType: 'identity_back',
    })
    return userMetadata
  }

  const isValidation = async () => {
    const validateForms = await formik.validateForm(formik.values)
    setErrors(validateForms)
    formik.setErrors(validateForms)
    formik.setTouched(validateForms)
    return Object.keys(validateForms).length === 0
  }

  const handleToSubmitBorrower = async () => {
    const isValid = await isValidation()
    if (!isValid) {
      return
    }
    const data = formik.values
    const organizationId = myOrg ? myOrg.organizationId : data.selectedOrganizationId
    const userAddresses = generatePayLoadUserAddress(data, organizationId)
    const guarantors = data.guarantors
    const userMetadata = generatePayLoadUserMetadata(data)

    if (!data.email || data.email.length === 0) {
      // generate dummy email for borrower
      const fakeEmail = `${data.firstName.replace(/\s/g, '')}.${data.lastName.replace(
        /\s/g,
        '',
      )}@mailinator.com`
      data.email = fakeEmail
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd')
        .replace(/Đ/g, 'D')
        .toLowerCase()
    }

    const payload = {
      login: data.email,
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      imageUrl: '',
      langKey: 'vi',
      birthDate: data.birthDate,
      gender: data.gender,
      mobilePhone: data.mobilePhone,
      homePhone: '',
      tempPassword: 'NewUser1!',
      isTempPassword: true,
      identifyNumber: data.identificationNumber,
      yearOfService: data.yearOfService,
      company: data.company,
      organizationId: organizationId,
      issueDate: '',
      guarantors: guarantors,
      userAddresses: userAddresses,
      userMetadata: userMetadata,
    }
    if (editUserData) {
      payload.id = editUserData.id
      payload.tempPassword = ''
      payload.isTempPassword = false
      dispatch(borrowerActions.updateBorrower(payload))
    } else {
      dispatch(borrowerActions.createBorrower(payload))
    }
  }

  const renderOrganizations = () => {
    return !myOrg || !myOrg.organizationId ? (
      <CCol xl={12}>
        <CFormLabel htmlFor="organization" className="col-form-label">
          {t('view.Organizations.title')}
        </CFormLabel>
        <CInputGroup>
          <CInputGroupText>
            <FaRegBuilding />
          </CInputGroupText>
          <CFormSelect {...formik.getFieldProps('selectedOrganizationId')}>
            <option>{t('view.Organizations.SelectOrganizations')}</option>
            {organizations.map((item, index) => {
              return (
                <option key={index} value={item.id}>
                  {item.name}
                </option>
              )
            })}
          </CFormSelect>
        </CInputGroup>
      </CCol>
    ) : null
  }

  const generateGuarantors = (item, index) => {
    return (
      <CCol key={index}>
        <CRow>
          <CCol xl={12}>
            <CFormLabel htmlFor={item.title} className="col-form-label">
              {t(item.translateHeader)}
            </CFormLabel>
          </CCol>
          <CCol xl={12}>
            <CFormLabel htmlFor={item.translateName} className="col-form-label">
              {t(item.translateName)}
            </CFormLabel>
            <CInputGroup>
              <CInputGroupText>
                <CIcon icon={cilUser} />
              </CInputGroupText>
              <CFormInput
                id={`${item.title}FullName-${index}`}
                name={`${item.title}FullName-${index}`}
                placeholder={t(item.translateName)}
                defaultValue={item.fullName}
                autoComplete="none"
                onChange={(event) => inputChangedHandler(event, index, 'fullName')}
              />
            </CInputGroup>
            <CFormFeedback invalid style={{ display: 'block' }}>
              {formik.errors.guarantors ? formik.errors.guarantors[index].fullName : ''}
            </CFormFeedback>
          </CCol>
          <CCol xl={12}>
            <CFormLabel htmlFor="relationship" className="col-form-label">
              {t('view.Borrowers.Relationship')}
            </CFormLabel>
            <CInputGroup>
              <CInputGroupText>
                <CIcon icon={cilLan} />
              </CInputGroupText>
              <CFormInput
                id={`${item.title}Relationship-${index}`}
                name={`${item.title}Relationship-${index}`}
                placeholder={t('view.Borrowers.Relationship')}
                defaultValue={item.relationship}
                autoComplete="none"
                onKeyUp={(event) => inputChangedHandler(event, index, 'relationship')}
              />
            </CInputGroup>
          </CCol>
          <CCol xl={12}>
            <CFormLabel htmlFor="MobileNumber" className="col-form-label">
              {t('view.User.MobileNumber')}
            </CFormLabel>
            <CInputGroup>
              <CInputGroupText>
                <CIcon icon={cilPhone} />
              </CInputGroupText>
              <CFormInput
                type="number"
                id={`${item.title}MobileNumber-${index}`}
                name={`${item.title}MobileNumber-${index}`}
                placeholder={t('common.MobileNumber')}
                defaultValue={item.numberPhone}
                autoComplete="none"
                onChange={(event) => inputChangedHandler(event, index, 'phone')}
              />
            </CInputGroup>
            <CFormFeedback invalid style={{ display: 'block' }}>
              {formik.errors.guarantors ? formik.errors.guarantors[index].numberPhone : ''}
            </CFormFeedback>
          </CCol>
          {/*<CCol xl={12}>*/}
          {/*  <CFormLabel htmlFor="address" className="col-form-label">*/}
          {/*    {t('common.Address')}*/}
          {/*  </CFormLabel>*/}
          {/*  <CInputGroup>*/}
          {/*    <CInputGroupText>*/}
          {/*      <CIcon icon={cilLocationPin} />*/}
          {/*    </CInputGroupText>*/}
          {/*    <CFormInput*/}
          {/*      id={`${item.title}Address-${index}`}*/}
          {/*      name={`${item.title}Address-${index}`}*/}
          {/*      placeholder={t('common.Address')}*/}
          {/*      defaultValue={item.address}*/}
          {/*      autoComplete="none"*/}
          {/*      onChange={(event) => inputChangedHandler(event, index, 'address')}*/}
          {/*    />*/}
          {/*  </CInputGroup>*/}
          {/*</CCol>*/}
        </CRow>
      </CCol>
    )
  }

  const getTitle = () => {
    return paramId ? t('view.Borrowers.EditBorrower') : t('view.Borrowers.AddNewBorrower')
  }

  return isCreated ? (
    <Redirect to={{ pathname: `/borrowers` }} />
  ) : (
    <CRow>
      <CCol lg={12}>
        <CCard>
          <CForm autoComplete="off">
            <CCardHeader style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div>{getTitle()}</div>
              <div>
                <CButton
                  type="button"
                  color="primary"
                  className="text-center"
                  onClick={handleToSubmitBorrower}
                >
                  {t('common.Save')}
                </CButton>
              </div>
            </CCardHeader>
            <CCardBody>
              <CRow>
                {renderOrganizations()}
                <CCol xl={12}>
                  <CFormLabel htmlFor="LastName" className="col-form-label">
                    {t('view.User.LastName')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      id="LastName"
                      name="LastName"
                      placeholder={t('view.User.LastName')}
                      invalid={
                        formik.errors.lastName &&
                        formik.errors.lastName.length > 0 &&
                        formik.touched.lastName &&
                        formik.touched.lastName.length > 0
                      }
                      value={formik.values.lastName}
                      autoComplete="none"
                      {...formik.getFieldProps('lastName')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display: formik.errors.lastName && formik.touched.lastName ? 'block' : 'none',
                    }}
                  >
                    {formik.errors.lastName}
                  </CFormFeedback>
                </CCol>
                <CCol xl={12}>
                  <CFormLabel htmlFor="FirstName" className="col-form-label">
                    {t('view.User.FirstName')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      id="FirstName"
                      name="FirstName"
                      placeholder={t('view.User.FirstName')}
                      invalid={
                        formik.errors.firstName &&
                        formik.errors.firstName.length > 0 &&
                        formik.touched.firstName &&
                        formik.touched.firstName.length > 0
                      }
                      value={formik.values.firstName}
                      autoComplete="none"
                      {...formik.getFieldProps('firstName')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.firstName && formik.touched.firstName ? 'block' : 'none',
                    }}
                  >
                    {formik.errors.firstName}
                  </CFormFeedback>
                </CCol>
                <CCol xl={12}>
                  <CFormLabel htmlFor="gender" className="col-form-label">
                    {t('view.User.Gender')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilPeople} />
                    </CInputGroupText>
                    <CFormSelect
                      custom="true"
                      name="Gender"
                      id="Gender"
                      invalid={
                        formik.errors.gender &&
                        formik.errors.gender.length > 0 &&
                        formik.touched.gender &&
                        formik.touched.gender.length > 0
                      }
                      {...formik.getFieldProps('gender')}
                    >
                      <option value="0">{t('messages.messagePleaseSelect')}</option>
                      <option value={constGenders.MALE}>{t('view.User.GenderType.Male')}</option>
                      <option value={constGenders.FEMALE}>
                        {t('view.User.GenderType.Female')}
                      </option>
                      <option value={constGenders.UNKNOWN}>
                        {t('view.User.GenderType.Unknown')}
                      </option>
                    </CFormSelect>
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display: formik.errors.gender && formik.touched.gender ? 'block' : 'none',
                    }}
                  >
                    {formik.errors.gender}
                  </CFormFeedback>
                </CCol>
                <CCol sm={12}>
                  <CFormLabel htmlFor="DateOfBirth" className="col-form-label">
                    {t('view.User.DateOfBirth')}
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilCalendar} />
                    </CInputGroupText>
                    <CDatePicker
                      id="DateOfBirth"
                      date={formik.values.birthDate}
                      style={{ flex: 1 }}
                      name="DateOfBirth"
                      onDateChange={(date) => {
                        if (date) {
                          formik.setFieldValue('birthDate', date)
                        }
                      }}
                    />
                  </CInputGroup>
                </CCol>
                <CCol sm={12}>
                  <CFormLabel htmlFor="MobileNumber" className="col-form-label">
                    {t('view.User.MobileNumber')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilPhone} />
                    </CInputGroupText>
                    <CFormInput
                      type="number"
                      id="MobileNumber"
                      name="MobileNumber"
                      placeholder={t('common.MobileNumber')}
                      invalid={
                        formik.errors.mobilePhone &&
                        formik.errors.mobilePhone.length > 0 &&
                        formik.touched.mobilePhone &&
                        formik.touched.mobilePhone.length > 0
                      }
                      value={formik.values.mobilePhone}
                      autoComplete="none"
                      {...formik.getFieldProps('mobilePhone')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.mobilePhone && formik.touched.mobilePhone ? 'block' : 'none',
                    }}
                  >
                    {formik.errors.mobilePhone}
                  </CFormFeedback>
                </CCol>
                <CCol sm={12} className="mb-4">
                  <CFormLabel htmlFor="email" className="col-form-label">
                    {t('view.User.Email')}
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilEnvelopeOpen} />
                    </CInputGroupText>
                    <CFormInput
                      type="text"
                      id="email"
                      name="email"
                      placeholder={t('view.User.Email')}
                      invalid={
                        formik.errors.email &&
                        formik.errors.email.length > 0 &&
                        formik.touched.email &&
                        formik.touched.email.length > 0
                      }
                      value={formik.values.email}
                      autoComplete="none"
                      {...formik.getFieldProps('email')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display: formik.errors.email && formik.touched.email ? 'block' : 'none',
                    }}
                  >
                    {formik.errors.email}
                  </CFormFeedback>
                </CCol>
                <CCol xl={12}>
                  <CFormLabel htmlFor="permanentAddress" className="col-form-label">
                    {t('view.User.PermanentAddress')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilLocationPin} />
                    </CInputGroupText>
                    <CFormInput
                      id="permanentAddress"
                      name="permanentAddress"
                      placeholder={t('view.User.PermanentAddress')}
                      invalid={
                        formik.errors.permanentAddress &&
                        formik.errors.permanentAddress.length > 0 &&
                        formik.touched.permanentAddress &&
                        formik.touched.permanentAddress.length > 0
                      }
                      value={formik.values.permanentAddress}
                      autoComplete="none"
                      {...formik.getFieldProps('permanentAddress')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.permanentAddress && formik.touched.permanentAddress
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {formik.errors.permanentAddress}
                  </CFormFeedback>
                </CCol>
                <CCol xl={12}>
                  <CFormLabel htmlFor="currentAddress" className="col-form-label">
                    {t('view.User.CurrentAddress')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilLocationPin} />
                    </CInputGroupText>
                    <CFormInput
                      id="currentAddress"
                      name="currentAddress"
                      placeholder={t('view.User.CurrentAddress')}
                      invalid={
                        formik.errors.currentAddress &&
                        formik.errors.currentAddress.length > 0 &&
                        formik.touched.currentAddress &&
                        formik.touched.currentAddress.length > 0
                      }
                      value={formik.values.currentAddress}
                      autoComplete="none"
                      {...formik.getFieldProps('currentAddress')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.currentAddress && formik.touched.currentAddress
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {formik.errors.currentAddress}
                  </CFormFeedback>
                </CCol>
                <CCol xl={12}>
                  <CFormLabel htmlFor="identificationNumber" className="col-form-label">
                    {t('view.User.IdentificationNumber')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <FaAddressCard />
                    </CInputGroupText>
                    <CFormInput
                      id="identificationNumber"
                      name="identificationNumber"
                      placeholder={t('view.User.IdentificationNumber')}
                      invalid={
                        formik.errors.identificationNumber &&
                        formik.errors.identificationNumber.length > 0 &&
                        formik.touched.identificationNumber &&
                        formik.touched.identificationNumber.length > 0
                      }
                      value={formik.values.identificationNumber}
                      autoComplete="none"
                      {...formik.getFieldProps('identificationNumber')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.identificationNumber && formik.touched.identificationNumber
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {formik.errors.identificationNumber}
                  </CFormFeedback>
                </CCol>
                <CCol sm={6} className="mb-4">
                  <CFormLabel htmlFor="idAttached" className="col-form-label">
                    {t('view.User.IdAttached')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <div className="box-identification  mb-4">
                    <img
                      src={
                        editUserData
                          ? getAttachedIdentity(editUserData, 'identity_front')
                          : idFrontFaceURL
                      }
                      height="80px"
                    />
                  </div>
                  <CInputGroup>
                    <CButtonGroup role="group" className="group-button-identification">
                      <CFormInput
                        type="file"
                        onChange={(e) => onUploadFileIdFrontFaceChange(e)}
                      ></CFormInput>
                    </CButtonGroup>
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.idFrontFaceURL && formik.touched.idFrontFaceURL
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {formik.errors.idFrontFaceURL}
                  </CFormFeedback>
                </CCol>
                <CCol sm={6} className="mb-4">
                  <CFormLabel htmlFor="id2Attached" className="col-form-label">
                    {t('view.User.Id2Attached')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <div className="box-identification mb-4">
                    <img
                      src={
                        editUserData
                          ? getAttachedIdentity(editUserData, 'identity_back')
                          : idBackFaceURL
                      }
                      height="80px"
                    />
                  </div>
                  <CInputGroup>
                    <CButtonGroup role="group" className="group-button-identification">
                      <CFormInput
                        type="file"
                        onChange={(e) => onUploadFileIdBackFaceChange(e)}
                      ></CFormInput>
                    </CButtonGroup>
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.idBackFaceURL && formik.touched.idBackFaceURL
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {formik.errors.idBackFaceURL}
                  </CFormFeedback>
                </CCol>
                <CCol xl={12}>
                  <CFormLabel htmlFor="company" className="col-form-label">
                    {t('view.User.Company')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <CIcon icon={cilBuilding} />
                    </CInputGroupText>
                    <CFormInput
                      id="company"
                      name="company"
                      placeholder={t('view.User.Company')}
                      value={formik.values.company}
                      autoComplete="none"
                      {...formik.getFieldProps('company')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display: formik.errors.company && formik.touched.company ? 'block' : 'none',
                    }}
                  >
                    {formik.errors.company}
                  </CFormFeedback>
                </CCol>
                <CCol xl={12}>
                  <CFormLabel htmlFor="yearOfService" className="col-form-label">
                    {t('view.User.YearsOfService')} <span className="form-required"> *</span>
                  </CFormLabel>
                  <CInputGroup>
                    <CInputGroupText>
                      <FaAddressCard />
                    </CInputGroupText>
                    <CFormInput
                      id="yearOfService"
                      name="yearOfService"
                      placeholder={t('view.User.YearsOfService')}
                      value={formik.values.yearOfService}
                      autoComplete="none"
                      {...formik.getFieldProps('yearOfService')}
                    />
                  </CInputGroup>
                  <CFormFeedback
                    invalid
                    style={{
                      display:
                        formik.errors.yearOfService && formik.touched.yearOfService
                          ? 'block'
                          : 'none',
                    }}
                  >
                    {formik.errors.yearOfService}
                  </CFormFeedback>
                </CCol>
              </CRow>
            </CCardBody>
          </CForm>
        </CCard>
        <CCard className="mt-3">
          <CCardBody>
            <CRow>
              {formik.values.guarantors.map((item, index) => {
                return generateGuarantors(item, index)
              })}
            </CRow>
            <CRow>
              <CCol xl={12}>
                <div className="flex-right mt-4">
                  <CButton
                    type="button"
                    color="primary"
                    className="text-center"
                    onClick={handleToSubmitBorrower}
                  >
                    {t('common.Save')}
                  </CButton>
                </div>
              </CCol>
            </CRow>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default BorrowerCreation

import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CSidebar,
  CSidebarNav,
  CNavItem,
  CListGroup,
  CListGroupItem,
  CTabContent,
  CTabPane,
  CLink,
  CBadge,
  CButton,
  CCollapse,
  CSmartTable,
} from '@coreui/react-pro'
import Avatar from 'react-avatar'
import { useDispatch, useSelector } from 'react-redux'
import { borrowerActions } from '../actions'
import CIcon from '@coreui/icons-react'
import { cilMobile, cilPencil, cilNewspaper, cilBalanceScale, cilDollar } from '@coreui/icons'
import { FaCheck, FaMotorcycle, FaTimes } from 'react-icons/fa'
import { loanActions } from '../../loans/actions'
import { colorHelpers } from '../../../utils/color-helper'
import moment from 'moment'
import { loanHelpers } from '../../../utils/loan-helper'

const Borrower = ({ match }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const dispatch = useDispatch()
  const usersData = useSelector((state) => state.borrowers.borrower)
  const loans = useSelector((state) => state.loans.loans)
  const [activeKey, setActiveKey] = useState(1)
  const itemsPerPage = useSelector((state) => state.loans.itemsPerPage)
  const totalPages = useSelector((state) => state.loans.totalPages)

  const tabs = {
    PROFILE: 1,
    LOAN: 2,
  }

  let paramId = match.params.id

  const getAllLoans = async () => {
    const payload = {
      borrowerId: paramId,
      page: 0,
      size: itemsPerPage,
    }
    dispatch(loanActions.getAllLoans(payload))
  }

  useEffect(() => {
    if (paramId) {
      dispatch(borrowerActions.getBorrowerById(paramId))
    }
    const loadData = async () => {
      await getAllLoans()
    }
    loadData()
  }, [dispatch])

  const getUserData = (data, type) => {
    if (!data) return ''
    switch (type) {
      case 'fullName':
        return `${data.firstName} ${data.lastName}`
      case 'gender':
        return data.gender
      case 'identifyNumber':
        return data.identifyNumber
      case 'birthDate':
        return data.birthDate
      case 'permanentAddress':
        const permanentAddressList = data.userAddressList.filter((address) => !address.isPrimary)
        return permanentAddressList && permanentAddressList.length
          ? permanentAddressList[0].addressLine1
          : ''
      case 'currentAddress':
        const currentAddressList = data.userAddressList.filter((address) => address.isPrimary)
        return currentAddressList && currentAddressList.length
          ? currentAddressList[0].addressLine1
          : ''
      case 'company':
        return data.company
      case 'yearOfService':
        return data.yearOfService
      case 'phone':
        return data.phone
      case 'email':
        return data.email
      default:
        return ''
    }
  }

  const navigationToUserEdit = () => {
    history.push(`/borrowers/${paramId}/edit`)
  }

  const getActiveTab = (index) => {
    let active = index === activeKey ? 'active' : ''
    return `card-navigation-item ${active}`
  }

  const getShortLoanId = (id) => {
    return id.substring(0, 8).toUpperCase()
  }

  const getPledgeIcons = (item) => {
    return item.pledge ? (
      <FaCheck size="1.2em" color="#1e9ff2" />
    ) : (
      <FaTimes size="1.2em" color="#e55353" />
    )
  }

  const getBadge = (status) => {
    if (!status) return
    return colorHelpers.getColorByStatus(status.toLowerCase(), false)
  }

  const getInstallment = (item) => {
    if (item.status !== 'ACTIVE') return null
    const text = 'common.' + item.repaymentFrequencyType
    return item.totalInstallment ? `${item.nextInstallment}/${item.totalInstallment} ${t(text)}` : 0
  }

  const getNextInstallment = (item) => {
    if (item.status !== 'ACTIVE') return null
    return moment(item.repaymentDueDate).format('DD-MM-yyyy')
  }

  const getDayInArrearsColor = (item) => {
    if (item.status !== 'ACTIVE') return ''
    return colorHelpers.getDayInArrearsColor(item)
  }

  const getDayInArrears = (item) => {
    if (item.status !== 'ACTIVE') return ''
    const today = moment()
    const dueDate = moment(item.repaymentDueDate)
    const duration = dueDate.diff(today, 'days')
    let dayInArrearText = t('common.Today')
    if (duration > 0) {
      dayInArrearText = `${t('common.Still')} ${duration} ${t('common.Days')}`
    } else if (duration < 0) {
      dayInArrearText = `${t('common.Late')} ${duration} ${t('common.Days')}`
    }
    return dayInArrearText
  }

  const renderProfile = () => {
    return (
      <CCard className="card-profile">
        <CCardBody>
          <h3 className="text-muted">{t('common.Profile')}</h3>
          <CRow>
            <CCol>
              <h5>{t('common.BasicDetail')}</h5>
              <CListGroup>
                <CListGroupItem>
                  <CRow>
                    <CCol xl={4}>
                      <b>{t('view.Borrowers.Profile.FullName')}</b>
                    </CCol>
                    <CCol>{getUserData(usersData, 'fullName')}</CCol>
                  </CRow>
                </CListGroupItem>
                <CListGroupItem>
                  <CRow>
                    <CCol xl={4}>
                      <b>{t('view.User.Gender')}</b>
                    </CCol>
                    <CCol>{getUserData(usersData, 'gender')}</CCol>
                  </CRow>
                </CListGroupItem>
                <CListGroupItem>
                  <CRow>
                    <CCol xl={4}>
                      <b>{t('view.User.IdentificationNumber')}</b>
                    </CCol>
                    <CCol>{getUserData(usersData, 'identifyNumber')}</CCol>
                  </CRow>
                </CListGroupItem>
                <CListGroupItem>
                  <CRow>
                    <CCol xl={4}>
                      <b>{t('view.User.DateOfBirth')}</b>
                    </CCol>
                    <CCol>{getUserData(usersData, 'birthDate')}</CCol>
                  </CRow>
                </CListGroupItem>
                <CListGroupItem>
                  <CRow>
                    <CCol xl={4}>
                      <b>{t('view.User.PermanentAddress')}</b>
                    </CCol>
                    <CCol>{getUserData(usersData, 'permanentAddress')}</CCol>
                  </CRow>
                </CListGroupItem>
                <CListGroupItem>
                  <CRow>
                    <CCol xl={4}>
                      <b>{t('view.User.CurrentAddress')}</b>
                    </CCol>
                    <CCol>{getUserData(usersData, 'currentAddress')}</CCol>
                  </CRow>
                </CListGroupItem>
                <CListGroupItem>
                  <CRow>
                    <CCol xl={4}>
                      <b>{t('view.User.Company')}</b>
                    </CCol>
                    <CCol>{getUserData(usersData, 'company')}</CCol>
                  </CRow>
                </CListGroupItem>
                <CListGroupItem>
                  <CRow>
                    <CCol xl={4}>
                      <b>{t('view.User.YearsOfService')}</b>
                    </CCol>
                    <CCol>{getUserData(usersData, 'yearOfService')}</CCol>
                  </CRow>
                </CListGroupItem>
              </CListGroup>
            </CCol>
            <CCol>
              <CRow className="mb-4">
                <h5 className="ps-0">{t('common.ContactDetail')}</h5>
                <CListGroup>
                  <CListGroupItem>
                    <CRow>
                      <CCol xl={4}>
                        <b>{t('common.MobileNumber')}</b>
                      </CCol>
                      <CCol>{getUserData(usersData, 'phone')}</CCol>
                    </CRow>
                  </CListGroupItem>
                  <CListGroupItem>
                    <CRow>
                      <CCol xl={4}>
                        <b>{t('view.User.Email')}</b>
                      </CCol>
                      <CCol>{getUserData(usersData, 'email')}</CCol>
                    </CRow>
                  </CListGroupItem>
                </CListGroup>
              </CRow>
              <CRow>
                <h5 className="ps-0">{t('view.Borrowers.Profile.UploadFiles')}</h5>
                {usersData &&
                  usersData.userMetadataList &&
                  usersData.userMetadataList.map((data, index) => {
                    return (
                      <CCol key={index}>
                        <a key={index} href={data.thumbUrl} target="_blank" rel="noreferrer">
                          <img key={index} src={data.thumbUrl} style={{ width: '150px' }} />
                        </a>
                      </CCol>
                    )
                  })}
              </CRow>
            </CCol>
          </CRow>
        </CCardBody>
      </CCard>
    )
  }

  const renderLoans = () => {
    return (
      <CCard className="card-profile">
        <CCardBody>
          <h3 className="text-muted">{t('view.Borrowers.Loan')}</h3>
          <CRow>
            <CCol className="table-responsive">
              <CSmartTable
                items={loans}
                columns={[
                  {
                    key: 'id',
                    _classes: 'font-weight-bold',
                    label: t('view.Loan.LoanId'),
                  },
                  { key: 'amountPrincipal', label: t('view.Loan.Amount') },
                  { key: 'repaymentDueAmount', label: t('view.Loan.InstallmentAmount') },
                  { key: 'installment', label: t('view.Loan.Installment') },
                  { key: 'repaymentFromDate', label: t('view.Loan.NextInstallment') },
                  { key: 'pledge', label: t('common.Pledge') },
                  { key: 'status', label: t('view.Loan.Status') },
                ]}
                tableFilter
                itemsPerPageSelect
                itemsPerPage={itemsPerPage}
                pagination
                tableProps={{
                  striped: true,
                  hover: true,
                }}
                scopedColumns={{
                  id: (item) => (
                    <td>
                      <CLink
                        className="text-decoration-none font-weight-bold"
                        href={`#/loans/${item.id}`}
                      >
                        {getShortLoanId(item.id)}
                      </CLink>
                    </td>
                  ),
                  amountPrincipal: (item) => (
                    <td className="text-right">
                      {loanHelpers.currencyFormatter(item.amountPrincipal)}
                    </td>
                  ),
                  repaymentDueAmount: (item) => (
                    <td className="text-right">
                      {item.status === 'ACTIVE'
                        ? loanHelpers.currencyFormatter(item.repaymentDueAmount)
                        : ''}
                    </td>
                  ),
                  installment: (item) => (
                    <td>
                      <CRow>
                        <span>{getInstallment(item)}</span>
                      </CRow>
                    </td>
                  ),
                  repaymentFromDate: (item) => (
                    <td>
                      <CRow className="mb-2">
                        <span>{getNextInstallment(item)}</span>
                      </CRow>
                      <CRow>
                        <span style={{ color: getDayInArrearsColor(item) }}>
                          <b>{getDayInArrears(item)}</b>
                        </span>
                      </CRow>
                    </td>
                  ),
                  pledge: (item) => (
                    <td>
                      <FaMotorcycle size="1.2em" className="me-2" />
                      {getPledgeIcons(item)}
                    </td>
                  ),
                  status: (item) => (
                    <td>
                      <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                    </td>
                  ),
                }}
              />
            </CCol>
          </CRow>
        </CCardBody>
      </CCard>
    )
  }

  return (
    <CRow className="borrower">
      <CCol xl={3}>
        <CCard className="card-avatar">
          <CCardBody>
            <CRow>
              <CCol xl={12} className="flex-center mb-3">
                <div>{getUserData(usersData, 'fullName')}</div>
              </CCol>
              <CCol xl={12} className="flex-center">
                <Avatar name={getUserData(usersData, 'fullName')} size="100px" />
              </CCol>
              <CCol xl={12} className="flex-center">
                <div className="icon-circle me-1 mt-3">
                  <CIcon icon={cilMobile} size="xl" />
                </div>
                <div className="icon-circle ms-1 mt-3" onClick={navigationToUserEdit}>
                  <CIcon icon={cilPencil} size="xl" />
                </div>
              </CCol>
            </CRow>
          </CCardBody>
        </CCard>
        <CCard className="card-navigation">
          <CCardHeader className="card-navigation-header">Navigation</CCardHeader>
          <CCardBody style={{ padding: 0 }}>
            <CSidebar className="card-navigation-bar">
              <CSidebarNav>
                <CNavItem className={getActiveTab(tabs.PROFILE)}>
                  <div onClick={() => setActiveKey(tabs.PROFILE)}>
                    <CIcon icon={cilNewspaper} className="me-3" />
                    {t('common.Profile')}
                  </div>
                </CNavItem>
                <CNavItem className={getActiveTab(tabs.LOAN)}>
                  <div onClick={() => setActiveKey(tabs.LOAN)}>
                    <CIcon icon={cilBalanceScale} className="me-3" />
                    {t('view.Borrowers.Loan')}
                  </div>
                </CNavItem>
                {/*<CNavItem className="card-navigation-item">*/}
                {/*  <CIcon icon={cilDollar} className="me-3" />*/}
                {/*  {t('view.Borrowers.Payment')}*/}
                {/*</CNavItem>*/}
              </CSidebarNav>
            </CSidebar>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol xl={9}>
        <CTabContent>
          <CTabPane data-tab="profile" visible={activeKey === tabs.PROFILE}>
            {renderProfile()}
          </CTabPane>
          <CTabPane data-tab="loan" visible={activeKey === tabs.LOAN}>
            {renderLoans()}
          </CTabPane>
        </CTabContent>
      </CCol>
    </CRow>
  )
}
export default Borrower

import * as t from './actionTypes'

const initialState = {
  isFetched: false,
  isFetching: false,
  borrowers: [],
  borrower: null,
  itemsPerPage: 5,
  totalPages: 0,
  isRedirect: false,
  isCreated: false,
}

const convertDataToBorrowerProfile = (data) => {
  return {
    id: data.borrowerDto.id,
    organizationId: data.borrowerDto.organizationId,
    fullName: `${data.userDto.lastName} ${data.userDto.firstName}`,
    gender: data.userProfileDto.gender,
    idNumber: data.borrowerDto.identifyNumber,
    phoneNumber: data.userProfileDto.phone,
    status: data.borrowerDto.status,
  }
}

const borrowerReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.CREATE_BORROWER_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
        isRedirect: false,
        isCreated: false,
      })
    case t.CREATE_BORROWER_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        isFetched: true,
        isRedirect: true,
        isCreated: true,
        borrower: action.borrower,
      })
    case t.CREATE_BORROWER_FAILURE:
      return Object.assign({}, state, {
        errorFetch: action.error,
      })
    case t.UPDATE_BORROWER_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
        isRedirect: false,
      })
    case t.UPDATE_BORROWER_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        isFetched: true,
        isRedirect: true,
        borrower: action.borrower,
      })
    case t.UPDATE_BORROWER_FAILURE:
      return Object.assign({}, state, {
        errorFetch: action.error,
      })
    case t.GET_ALL_BORROWERS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
        isRedirect: false,
      })
    case t.GET_ALL_BORROWERS_SUCCESS:
      const borrowers = []
      action.response.data.forEach((item) => {
        const borrower = convertDataToBorrowerProfile(item)
        borrowers.push(borrower)
      })
      return Object.assign({}, state, {
        isFetching: false,
        isFetched: true,
        borrowers: borrowers,
        totalPages: Math.ceil(action.response.headers['x-total-count'] / state.itemsPerPage),
      })
    case t.GET_ALL_BORROWERS_FAILURE:
      return Object.assign({}, state, {
        errorFetch: action.error,
      })
    case t.GET_BORROWER_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
        isRedirect: false,
      })
    case t.GET_BORROWER_SUCCESS:
      const convertedObject = Object.assign(
        {},
        action.borrower.borrowerDto,
        action.borrower.userDto,
        action.borrower.userProfileDto,
      )
      convertedObject.userAddressList = action.borrower.userAddressList
      convertedObject.userMetadataList = action.borrower.userMetadataList
      return Object.assign({}, state, {
        isFetching: false,
        isFetched: true,
        borrower: convertedObject,
      })
    case t.GET_BORROWER_FAILURE:
      return Object.assign({}, state, {
        errorFetch: action.error,
      })
    case t.RESET_BORROWER_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isFetched: false,
        isRedirect: false,
        isCreated: false,
      })
    case t.RESET_BORROWER_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        isFetched: true,
        isRedirect: false,
        isCreated: false,
        borrower: null,
      })
    default:
      return state
  }
}

export default borrowerReducer

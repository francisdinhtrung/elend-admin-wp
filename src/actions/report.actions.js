import { toast } from 'react-toastify'
import { reportConstants } from '../constants'
import api from '../utils/api'

function getReportBorrowers(organizationId) {
  return (dispatch) => {
    dispatch(request())
    return api.reportService
      .getReportBorrowers(organizationId)
      .then((response) => {
        dispatch(success(response))
      })
      .catch((error) => {
        dispatch(failure(error))
      })
  }

  function request() {
    return { type: reportConstants.GET_REPORT_BORROWERS_REQUEST }
  }

  function success(response) {
    return { type: reportConstants.GET_REPORT_BORROWERS_SUCCESS, response }
  }

  function failure(error) {
    return { type: reportConstants.GET_REPORT_BORROWERS_FAILURE, error }
  }
}

function getReportLoans(organizationId) {
  return (dispatch) => {
    dispatch(request())
    return api.reportService
      .getReportLoans(organizationId)
      .then((response) => {
        dispatch(success(response))
      })
      .catch((error) => {
        dispatch(failure(error))
      })
  }

  function request() {
    return { type: reportConstants.GET_REPORT_LOANS_REQUEST }
  }

  function success(response) {
    return { type: reportConstants.GET_REPORT_LOANS_SUCCESS, response }
  }

  function failure(error) {
    return { type: reportConstants.GET_REPORT_LOANS_FAILURE, error }
  }
}

function getReportLoanAnalysis(organizationId) {
  return (dispatch) => {
    dispatch(request())
    return api.reportService
      .getReportLoanAnalysis(organizationId)
      .then((response) => {
        dispatch(success(response))
      })
      .catch((error) => {
        dispatch(failure(error))
      })
  }

  function request() {
    return { type: reportConstants.GET_REPORT_LOANS_ANALYSIS_REQUEST }
  }

  function success(response) {
    return { type: reportConstants.GET_REPORT_LOANS_ANALYSIS_SUCCESS, response }
  }

  function failure(error) {
    return { type: reportConstants.GET_REPORT_LOANS_ANALYSIS_FAILURE, error }
  }
}

export const reportActions = {
  getReportBorrowers,
  getReportLoans,
  getReportLoanAnalysis,
}

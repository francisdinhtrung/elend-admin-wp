import React from 'react'
import { NavLink } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import {
  CButtonGroup,
  CFormCheck,
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderDivider,
  CHeaderNav,
  CHeaderToggler,
  CNavLink,
  CNavItem,
} from '@coreui/react-pro'
import CIcon from '@coreui/icons-react'
import {
  cilApplicationsSettings,
  cilMoon,
  cilSun,
  cilBell,
  cilEnvelopeOpen,
  cilList,
  cilMenu,
} from '@coreui/icons'

import { AppBreadcrumb } from './index'

import {
  AppHeaderDropdown,
  AppHeaderDropdownMssg,
  AppHeaderDropdownNotif,
  AppHeaderDropdownTasks,
  TheHeaderDropdownLanguage,
} from './header/index'

import { logo } from 'src/assets/brand/logo'

const AppHeader = () => {
  const dispatch = useDispatch()

  const sidebarShow = useSelector((state) => state.changeState.sidebarShow)
  const asideShow = useSelector((state) => state.changeState.asideShow)

  return (
    <CHeader position="sticky" className="mb-4">
      <CContainer fluid>
        <CHeaderToggler
          className="ps-1"
          onClick={() => dispatch({ type: 'set', sidebarShow: !sidebarShow })}
        >
          <CIcon icon={cilMenu} size="lg" />
        </CHeaderToggler>
        <CHeaderBrand className="mx-auto d-md-none" to="/">
          <CIcon icon={logo} height={48} alt="Logo" />
        </CHeaderBrand>
        <CHeaderNav className="d-none d-md-flex me-auto">
          <CNavItem>
            <CNavLink to="/dashboard" component={NavLink} activeClassName="active">
              Dashboard
            </CNavLink>
          </CNavItem>
          <CNavItem>
            <CNavLink to="/users" component={NavLink}>
              Users
            </CNavLink>
          </CNavItem>
          {/*<CNavItem>*/}
          {/*  <CNavLink href="#">Settings</CNavLink>*/}
          {/*</CNavItem>*/}
        </CHeaderNav>
        <CHeaderNav className="px-3">
          <TheHeaderDropdownLanguage />
        </CHeaderNav>
        <CHeaderNav>
          {/*<CNavItem>*/}
          {/*  <CNavLink href="#">*/}
          {/*    <CIcon icon={cilBell} size="lg" />*/}
          {/*  </CNavLink>*/}
          {/*</CNavItem>*/}
          {/*<CNavItem>*/}
          {/*  <CNavLink href="#">*/}
          {/*    <CIcon icon={cilList} size="lg" />*/}
          {/*  </CNavLink>*/}
          {/*</CNavItem>*/}
          {/*<CNavItem>*/}
          {/*  <CNavLink href="#">*/}
          {/*    <CIcon icon={cilEnvelopeOpen} size="lg" />*/}
          {/*  </CNavLink>*/}
          {/*</CNavItem>*/}
        </CHeaderNav>
        <CHeaderNav className="ms-3 me-4">
          <AppHeaderDropdown />
        </CHeaderNav>
        {/*<CHeaderToggler*/}
        {/*  className="px-md-0 me-md-3"*/}
        {/*  onClick={() => dispatch({ type: 'set', asideShow: !asideShow })}*/}
        {/*>*/}
        {/*  <CIcon icon={cilApplicationsSettings} size="lg" />*/}
        {/*</CHeaderToggler>*/}
      </CContainer>
      <CHeaderDivider />
      <CContainer fluid>
        <AppBreadcrumb />
      </CContainer>
    </CHeader>
  )
}

export default AppHeader

import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { CSidebar, CSidebarBrand, CSidebarNav, CSidebarToggler } from '@coreui/react-pro'

import CIcon from '@coreui/icons-react'

import { AppSidebarNav } from './AppSidebarNav'

import { logoNegative } from 'src/assets/brand/logo-negative'
import { sygnet } from 'src/assets/brand/sygnet'
import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'

// sidebar nav config
import navigation from '../_nav'
import { authHelpers } from '../utils/auth-helper'

const AppSidebar = () => {
  const { t } = useTranslation()

  const dispatch = useDispatch()
  const unfoldable = useSelector((state) => state.changeState.sidebarUnfoldable)
  const sidebarShow = useSelector((state) => state.changeState.sidebarShow)
  const account = useSelector((state) => state.auths.account)
  const userAccount = account
  const myOrganization =
    account && account.organizations && account.organizations.length > 0
      ? account.organizations[0]
      : undefined
  const userPermissions =
    userAccount && userAccount.authorities && userAccount.authorities.length > 0
      ? userAccount.authorities.map((a) => a.name)
      : []

  navigation.forEach((item) => {
    if (
      userPermissions &&
      userPermissions.length > 0 &&
      item.permissions &&
      item.permissions.length > 0
    ) {
      const foundPermissions = authHelpers.findMatchRolePermissions(
        userPermissions,
        item.permissions,
      )
      item.className = foundPermissions.length === 0 ? 'hidden' : ''
    }

    if (item.translatename) {
      item.name = t(item.translatename)
    }

    if (item.to && item.to.indexOf('/organizations') > -1 && myOrganization) {
      item.to = `/organizations/${myOrganization.organizationId}`
      item.name = t(item.translatemyorgname)
    }

    if (item.items && item.items.length > 0) {
      item.items.forEach((childItem) => {
        if (
          userPermissions &&
          userPermissions.length > 0 &&
          childItem.permissions &&
          childItem.permissions.length > 0
        ) {
          const foundPermissions = authHelpers.findMatchRolePermissions(
            userPermissions,
            childItem.permissions,
          )
          childItem.className = foundPermissions.length === 0 ? 'hidden' : ''
        }
        if (childItem.translatename) {
          childItem.name = t(childItem.translatename)
        }
      })
    }
  })

  const renderLogoHeader = () => {
    return myOrganization && myOrganization.logoUrl ? (
      <img src={myOrganization.logoUrl} height={35} />
    ) : (
      <div>
        <CIcon className="sidebar-brand-full" icon={logoNegative} height={35} />
        <CIcon className="sidebar-brand-narrow" icon={sygnet} height={35} />
      </div>
    )
  }

  return (
    <CSidebar
      position="fixed"
      unfoldable={unfoldable}
      visible={sidebarShow}
      // colorScheme={'light'}
      onVisibleChange={(visible) => {
        dispatch({ type: 'set', sidebarShow: visible })
      }}
    >
      <CSidebarBrand className="d-none d-md-flex" to="/">
        {renderLogoHeader()}
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <AppSidebarNav items={navigation} />
        </SimpleBar>
      </CSidebarNav>
      <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() => dispatch({ type: 'set', sidebarUnfoldable: !unfoldable })}
      />
    </CSidebar>
  )
}

export default React.memo(AppSidebar)
